fake_players_data = [
    {
        "user_uuid": "uuid1",
        "user_name": "Player1",
        "gender": "male",
        "sexual_orientation": "heterosexual",
        "couple_state": "single",
        "alcohol": "regularly",
        "total_sips": 10,
        "game_alcohol": 5,
        "profile_picture": "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png"
    },
    {
        "user_uuid": "uuid2",
        "user_name": "Player2",
        "gender": "female",
        "sexual_orientation": "homosexual",
        "couple_state": "couple",
        "alcohol": "occasionally",
        "total_sips": 5,
        "game_alcohol": 2,
        "profile_picture": "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png"
    },
    {
        "user_uuid": "uuid3",
        "user_name": "Player3",
        "gender": "female",
        "sexual_orientation": "homosexual",
        "couple_state": "couple",
        "alcohol": "occasionally",
        "total_sips": 5,
        "game_alcohol": 4,
        "profile_picture": "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png"
    },
    {
        "user_uuid": "uuid4",
        "user_name": "Player5",
        "gender": "female",
        "sexual_orientation": "homosexual",
        "couple_state": "couple",
        "alcohol": "occasionally",
        "total_sips": 5,
        "game_alcohol": 5,
        "profile_picture": "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png"
    },
    {
        "user_uuid": "uuid5",
        "user_name": "Player5",
        "gender": "female",
        "sexual_orientation": "homosexual",
        "couple_state": "couple",
        "alcohol": "occasionally",
        "total_sips": 5,
        "game_alcohol": 0,
        "profile_picture": "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png"
    },
    {
        "user_uuid": "uuid6",
        "user_name": "Player6",
        "gender": "female",
        "sexual_orientation": "homosexual",
        "couple_state": "couple",
        "alcohol": "occasionally",
        "total_sips": 5,
        "game_alcohol": 1,
        "profile_picture": "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png"
    },
    {
        "user_uuid": "uuid7",
        "user_name": "Player7",
        "gender": "female",
        "sexual_orientation": "homosexual",
        "couple_state": "couple",
        "alcohol": "occasionally",
        "total_sips": 4,
        "profile_picture": "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png"
    },
    {
        "user_uuid": "uuid8",
        "user_name": "Player8",
        "gender": "female",
        "sexual_orientation": "homosexual",
        "couple_state": "couple",
        "alcohol": "occasionally",
        "total_sips": 5,
        "game_alcohol": 2,
        "profile_picture": "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png"
    },
    {
        "user_uuid": "uuid9",
        "user_name": "Player9",
        "gender": "female",
        "sexual_orientation": "homosexual",
        "couple_state": "couple",
        "alcohol": "occasionally",
        "total_sips": 5,
        "game_alcohol": 3,
        "profile_picture": "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png"
    },
    {
        "user_uuid": "uuid10",
        "user_name": "Player10",
        "gender": "other",
        "sexual_orientation": "other",
        "couple_state": "complicated",
        "alcohol": "frequently",
        "total_sips": 15,
        "game_alcohol": 5,
        "profile_picture": "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png"
    }
]

players = [Player(**player_data) for player_data in fake_players_data]
print(players)