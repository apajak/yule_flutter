from time import sleep

import socketio
import eventlet
import os
from players import Player
from game import BarbuGame


# Fetch the port number from environment variable or use default
PORT = int(os.environ.get('PORT', 3000))

# create a Socket.IO server
sio = socketio.Server(cors_allowed_origins='*')  # Allow all origins for simplicity

# create a WSGI application
app = socketio.WSGIApp(sio)

# Global variables
master: Player
players = []
game = BarbuGame(players)

@sio.event
def connect(sid, environ):
    print(f"INFO: New client connected: {sid}")
    sio.emit('request_player_data', room=sid)  # Ask the newly connected client for player data


@sio.event
def disconnect(sid):
    global master, players
    print(f"INFO: Client disconnected: {sid}")

    disconnected_player = next((player for player in players if player.wsId == sid), None)
    if disconnected_player:
        print(f'Player {disconnected_player.user_name} disconnected')
        players.remove(disconnected_player)
        broadcast_player_list()  # Broadcast the updated player list to all clients
        if master and master.wsId == sid:
            assign_new_master()
        print(f"INFO: Player {disconnected_player.user_name} removed from player list")
        sio.emit('player_list_update', [player.to_simple_player().to_dict() for player in players])
    else:
        print(f'ERROR: Player not found in player list: {sid}')


@sio.event
def kick_player(sid, data):
    print(f'INFO: Kick player request from {sid}, data: {data}')
    global players
    player_uuid = data['playerUuid']
    kicked_player = next((player for player in players if player.user_uuid == player_uuid), None)
    if kicked_player:
        players.remove(kicked_player)
        print(f"INFO: Player {kicked_player.user_name} removed from player list")
        sio.emit('you_are_kicked', room=kicked_player.wsId)
        broadcast_player_list()


@sio.event
def get_player_list(sid):
    print(f'INFO: Player list request from {sid}')
    global players
    simple_players = [player.to_simple_player().to_dict() for player in players]
    print(f'INFO: Sending player list {simple_players} to {sid}')
    sio.emit('player_list_response', simple_players, room=sid)  # Envoyer uniquement au client demandeur
    # update the game state
    broadcast_game_state()


@sio.event
def player_data(sid, data):
    """Receive player data from a client."""
    print(f"INFO: Player data received: {data} from {sid}")
    global master, players
    if len(players) >= 54:
        # return error
        print('ERROR: Too many players')
        return
    player = Player(**data)  # Create a Player object from the received data
    print(f'INFO: Player created: {player}')
    player.set_wsId(sid)
    if len(players) == 0:
        player.set_isMaster(True)
        master = player
    players.append(player)
    print(f'INFO: Player added to player list: {players}')
    broadcast_player_list()  # Broadcast the updated player list to all clients


def broadcast_player_list():
    """Broadcast the player list to all clients."""
    global players
    print(f"INFO: Broadcasting player list: {players}")
    simple_players = [player.to_simple_player().to_dict() for player in players]
    sio.emit('player_list_update', simple_players)

@sio.event
def get_first_turn(sid):
    global players
    first_player = players[0]
    data = {
        'current_player_name': first_player.user_name,
        'current_player_uuid': first_player.user_uuid,
        'current_player_index': 0,
    }
    print(f"INFO: Broadcasting first turn: {data}")
    sio.emit('broadcast_first_turn', data, room=sid)

@sio.event
def start_game(sid):
    global master
    print(f'INFO: Game start request from {sid}')
    if master and master.wsId == sid:
        game.start_game()
        print(f"INFO: emitting game_started")
        sio.emit('game_started', {'message': 'Game has started'})
        sleep(0.5)
        print(f"info: Broadcasting game state: {game.get_game_state()}")
        broadcast_game_state()
    else:
        print(f"ERROR: Game start attempt by non-master player")


def broadcast_game_state():
    global game
    print(f"INFO: Broadcasting game state: {game.get_game_state()}")
    sio.emit('game_state_update', game.get_game_state())


# player broadcast functions
def drink_broadcast(player_uuid, sips):
    """Send the number of sips to drink to all players except the specified player."""
    global game
    print('INFO: Broadcasting drink')
    data = {
        'playerUuid': player_uuid,
        'sips': sips
    }
    # Emit message to each player except the one specified by player_uuid
    print(f'INFO: Broadcasting drink to all players except {player_uuid}')
    print(f"INFO: Data: {data}")
    for player in game.players:
        if player.user_uuid != player_uuid:
            sio.emit('drink_broadcast', data, room=player.wsId)


def team_drink_broadcast(players_uuid, sips):
    """Send the number of sips to drink to all players except those specified in players_uuid."""
    global game
    print('INFO: Broadcasting team drink')
    data = {
        'playersUuid': players_uuid,
        'sips': sips
    }
    print(f'INFO: Broadcasting team drink to all players except {players_uuid}')
    print(f"INFO: Data: {data}")
    # Emit message to each player not in players_uuid
    for player in game.players:
        if player.user_uuid not in players_uuid:
            sio.emit('team_drink_broadcast', data, room=player.wsId)


def drink(player_uuid, sips):
    """Send the number of sips to drink to the player who has to drink"""
    global game
    sid = next((p.wsId for p in game.players if p.user_uuid == player_uuid), None)
    print(f'INFO: Broadcasting send sips to drink to {player_uuid}')
    data = {
        'playerUuid': player_uuid,
        'sips': sips
    }
    print(f'INFO: Data: {data}')
    sio.emit('drink', data, room=sid)

def drink_servant(player_uuid, sips, master_uuid):
    """Send the number of sips to drink to the player who has to drink"""
    global game
    sid = next((p.wsId for p in game.players if p.user_uuid == player_uuid), None)
    print(f'INFO: Broadcasting send sips to drink to {player_uuid}')
    data = {
        'playerUuid': player_uuid,
        'sips': sips,
        'masterUuid': master_uuid
    }
    print(f'INFO: Data: {data}')
    sio.emit('drink_servant', data, room=sid)

def queen_broadcast(player_uuid):
    """Broadcast the queens info to all players """
    global game
    print(f'INFO: Broadcasting queens to all players except {player_uuid}')
    sio.emit('queen_broadcast', {'queenUuid': player_uuid})

def king_broadcast(player_uuid):
    """Broadcast the kings info to all players """
    global game
    print(f'INFO: Broadcasting kings to all players except {player_uuid}')
    sio.emit('king_broadcast', {'kingUuid': player_uuid})

## Thumb master
def thumb_master(player_uuid):
    """Broadcast the thumb master notification to the new thumb master players """
    global game
    sid = next((p.wsId for p in game.players if p.user_uuid == player_uuid), None)
    print(f'INFO: Broadcasting thumb master to players {player_uuid}')
    sio.emit('thumb_master', {'thumbMasterUuid': player_uuid}, room=sid)

def thumb_master_broadcast(player_uuid):
    """Broadcast the thumb master notification to all players """
    global game
    print(f'INFO: Broadcasting thumb master to all players except {player_uuid}')
    data = {
        'thumbMasterUuid': player_uuid
    }
    for player in game.players:
        if player.user_uuid not in player_uuid:
            sio.emit('thumb_master_broadcast', data, room=player.wsId)

def thumb_master_drink_broadcast(player_uuid):
    """Broadcast the who loose the thumb master event to all players except the looser """
    global game
    print(f'INFO: Broadcasting thumb master to all players except {player_uuid}')
    data = {
        'looserUuid': player_uuid
    }
    for player in game.players:
        if player.user_uuid not in player_uuid:
            sio.emit('thumb_master_drink_broadcast', data, room=player.wsId)

def thumb_master_drink(player_uuid):
    """Broadcast to the looser the drink notification"""
    global game
    sid = next((p.wsId for p in game.players if p.user_uuid == player_uuid), None)
    player = next((p for p in game.players if p.user_uuid == player_uuid), None)
    print(f'INFO: Broadcasting thumb drink to player  {player.user_name}')
    sio.emit('thumb_master_drink', {'thumbMasterUuid': player_uuid}, room=sid)

@sio.event
def thumb_master_validate(sid):
    """ Get the thumb master validation"""
    global game
    player = next((p for p in game.players if p.wsId == sid), None)
    print(f"INFO: Player {player.user_name} validated his thumb master")
    if player.user_uuid == game.thumb_master:
        game.next_turn()
        broadcast_new_turn()
    else:
        print(f'ERROR: Invalid action by {sid}')

@sio.event
def thumb_master_event(sid, data):
    """ Get the thumb master event"""
    global game
    player = next((p for p in game.players if p.wsId == sid), None)
    print(f"INFO: Player {player.user_name} select a thumb master looser")
    thumbed_player_uuid = data['thumbedPlayerUuid']
    if player.user_uuid == game.thumb_master:
        thumb_master_drink(thumbed_player_uuid)
    else:
        print(f'ERROR: Invalid action by {sid}')

@sio.event
def thumb_master_drink_validate(sid):
    global game
    player = next((p for p in game.players if p.wsId == sid), None)
    print(f"INFO: Player {player.user_name} validated his thumb master drink")
    player.game_sips += 1

## Shifumi
def shifumi_broadcast(player_uuid):
    """Broadcast the shifumi info to all players """
    global game
    print(f'INFO: Broadcasting shifumi to all players except {player_uuid}')
    for player in game.players:
        if player.user_uuid != player_uuid:
            sio.emit('shifumi_broadcast', {'shifumiMasterUuid': player_uuid}, room=player.wsId)

def shifumi(player_uuid):
    """Broadcast the shifumi info to master players """
    global game
    print(f'INFO: Broadcasting shifumi to master players {player_uuid}')
    sid = next((p.wsId for p in game.players if p.user_uuid == player_uuid), None)
    sio.emit('shifumi', {'shifumiMasterUuid': player_uuid}, room=sid)

def shifumi_turn_event_master(master_uuid, opponent_uuid):
    """ broadcast the shifumi turn event"""
    global game
    sid = next((p.wsId for p in game.players if p.user_uuid == master_uuid), None)
    data = {
        'shifumiMasterUuid': master_uuid,
        'shifumiOpponentUuid': opponent_uuid,
        'shifumiTurns': game.shifumi_turns,
        'shifumiCurrentTurn': game.shifumi_current_turn,
        'shifumiSips': game.shifumi_sips
    }
    sio.emit('shifumi_turn_event_master', data, room=sid)

def shifumi_preturn_event_master(master_uuid, opponent_uuid):
    """ broadcast the shifumi turn event"""
    global game
    sid = next((p.wsId for p in game.players if p.user_uuid == master_uuid), None)
    data = {
        'shifumiMasterUuid': master_uuid,
        'shifumiOpponentUuid': opponent_uuid,
        'shifumiTurns': game.shifumi_turns,
        'shifumiCurrentTurn': game.shifumi_current_turn,
        'shifumiSips': game.shifumi_sips
    }
    sio.emit('shifumi_preturn_event_master', data, room=sid)

def shifumi_turn_event_player(player_uuid, master_uuid):
    """ broadcast the shifumi turn event"""
    global game
    sid = next((p.wsId for p in game.players if p.user_uuid == player_uuid), None)
    data = {
        'shifumiMasterUuid': master_uuid,
        'shifumiOpponentUuid': player_uuid,
        'shifumiTurns': game.shifumi_turns,
        'shifumiCurrentTurn': game.shifumi_current_turn,
        'shifumiSips': game.shifumi_sips
    }
    sio.emit('shifumi_turn_event_player', data, room=sid)

def shifumi_turn_event_broadcast(player_uuid, master_uuid):
    """ send new duel info to all players except the two players"""
    global game
    print(f'INFO: Broadcasting shifumi turn event to all players except {player_uuid}')
    data = {
        'shifumiMasterUuid': master_uuid,
        'shifumiOpponentUuid': player_uuid,
        'shifumiTurns': game.shifumi_turns,
        'shifumiCurrentTurn': game.shifumi_current_turn,
        'shifumiSips': game.shifumi_sips
    }
    for player in game.players:
        if player.user_uuid != player_uuid and player.user_uuid != master_uuid:
            sio.emit('shifumi_turn_event_broadcast', data, room=player.wsId)

def shifumi_preturn_event_player(player_uuid, master_uuid):
    """ broadcast the shifumi turn event"""
    global game
    sid = next((p.wsId for p in game.players if p.user_uuid == player_uuid), None)
    data = {
        'shifumiMasterUuid': master_uuid,
        'shifumiOpponentUuid': player_uuid,
        'shifumiTurns': game.shifumi_turns,
        'shifumiCurrentTurn': game.shifumi_current_turn,
        'shifumiSips': game.shifumi_sips
    }
    sio.emit('shifumi_preturn_event_player', data, room=sid)

def shifumi_result_broadcast(player_uuid):
    """Broadcast the shifumi result info to all players """
    global game
    print(f'INFO: Broadcasting shifumi result to all players except {player_uuid}')
    data = {
        'shifumiMasterUuid': player_uuid,
        'shifumiSips': game.shifumi_sips
    }
    for player in game.players:
        if player.user_uuid != player_uuid:
            sio.emit('shifumi_result_broadcast', data, room=player.wsId)

def shifumi_result_inter_broadcast(player_uuid):
    """Broadcast the shifumi result info to all players """
    global game
    print(f'INFO: Broadcasting shifumi intermediate result to all players except {player_uuid}')
    data = {
        'shifumiMasterUuid': player_uuid,
        'shifumiSips': game.shifumi_sips
    }
    for player in game.players:
        if player.user_uuid != player_uuid:
            sio.emit('shifumi_result_inter_broadcast', data, room=player.wsId)
def shifumi_drink(player_uuid):
    """Broadcast to the looser the drink notification"""
    global game
    sid = next((p.wsId for p in game.players if p.user_uuid == player_uuid), None)
    player = next((p for p in game.players if p.user_uuid == player_uuid), None)
    print(f'INFO: Broadcasting shifumi drink to player  {player.user_name}')
    data = {
        'shifumiMasterUuid': player_uuid,
        'shifumiSips': game.shifumi_sips,
        'shifumiTurns': game.shifumi_turns
    }
    sio.emit('shifumi_drink', data, room=sid)

@sio.event
def shifumi_preturn_validate(sid):
    """ Get the shifumi master validation"""
    global game
    player = next((p for p in game.players if p.wsId == sid), None)
    print(f"INFO: Player {player.user_name} validated his shifumi preturn")
    if player.user_uuid == game.shifumi_master:
        print(f"INFO: Master {player.user_name} validated his shifumi preturn")
        game.shifumi_validation.append(player.user_uuid)
    if player.user_uuid == game.shifumi_current_opponent:
        print(f"INFO: Opponent {player.user_name} validated his shifumi preturn")
        game.shifumi_validation.append(player.user_uuid)
    if len(game.shifumi_validation) == 2:
        print(f"INFO: Both players validated their shifumi preturn")
        print(f"INFO: Broadcasting shifumi turn start")
        game.shifumi_validation = []
        shifumi_turn_event_master(game.shifumi_master, game.shifumi_current_opponent)
        shifumi_turn_event_player(game.shifumi_current_opponent, game.shifumi_master)
    else:
        print(f'ERROR: Invalid action by {sid}')


@sio.event
def shifumi_validate(sid):
    """ Get the shifumi master validation"""
    global game
    player = next((p for p in game.players if p.wsId == sid), None)
    print(f"INFO: Player {player.user_name} validated his shifumi rule")
    if player.user_uuid == game.shifumi_master:
        # go for first shifumi turn
        if (len(game.shifumi_opponent) > 0):
            game.shifumi_current_turn = 0
            opponent = game.shifumi_opponent.pop(0)
            game.shifumi_current_opponent = opponent
            game.shifumi_current_turn += 1
            shifumi_preturn_event_master(game.shifumi_master, opponent)
            shifumi_preturn_event_player(opponent, game.shifumi_master)
        else :
            print(f"INFO: Not enough players for shifumi")
    else:
        print(f'ERROR: Invalid action by {sid}')

@sio.event
def shifumi_turn_result(sid, data):
    """ Get shifumi turn result"""
    global game
    player = next((p for p in game.players if p.wsId == sid), None)
    print(f"INFO: Player {player.user_name} validated his shifumi turn")
    sips = data['sips']
    if player.user_uuid == game.shifumi_master:
        # go for next shifumi turn
        if len(game.shifumi_opponent > 0):
            game.shifumi_sips += sips
            game.shifumi_current_turn += 1
            opponent = game.shifumi_opponent.pop(0)
            shifumi_preturn_event_master(game.shifumi_master, opponent.user_uuid)
            shifumi_preturn_event_player(opponent.user_uuid, game.shifumi_master)
        else :
            print(f"INFO: End of shifumi")
            shifumi_result_broadcast(game.shifumi_master)
            shifumi_drink(game.shifumi_master)
            # reset shifumi variables
            game.shifumi_master = None
            game.shifumi_opponent = None
            game.shifumi_turns = None
            game.shifumi_current_turn = 0
            game.shifumi_sips = 0
            game.shifumi_current_opponent = None
            game.shifumi_validation = []
    else:
        print(f'ERROR: Invalid action by {sid}')



def never_have_i_ever_broadcast(player_uuid):
    """Broadcast the never have i ever info to all players """
    global game
    print(f'INFO: Broadcasting never have i ever to all players except {player_uuid}')
    sio.emit('never_have_i_ever_broadcast', {'neverHaveIEverUuid': player_uuid})


def i_have_broadcast(player_uuid):
    """Broadcast the i have info to all players """
    global game
    print(f'INFO: Broadcasting i have to all players except {player_uuid}')
    sio.emit('i_have_broadcast', {'iHaveUuid': player_uuid})

def i_packed_my_bag_broadcast(player_uuid):
    """Broadcast the i packed my bag info to all players """
    global game
    print(f'INFO: Broadcasting i packed my bag to all players except {player_uuid}')
    sio.emit('i_packed_my_bag_broadcast', {'iPackedMyBagUuid': player_uuid})

def back_to_back_broadcast(player_uuid):
    """Broadcast the back to back info to all players """
    global game
    print(f'INFO: Broadcasting back to back to all players except {player_uuid}')
    sio.emit('back_to_back_broadcast', {'backToBackUuid': player_uuid})

def truth_or_dare_broadcast(player_uuid):
    """Broadcast the truth or dare info to all players """
    global game
    print(f'INFO: Broadcasting truth or dare to all players except {player_uuid}')
    sio.emit('truth_or_dare_broadcast', {'truthOrDareUuid': player_uuid})

def story_time_broadcast(player_uuid):
    """Broadcast the story time info to all players """
    global game
    print(f'INFO: Broadcasting story time to all players except {player_uuid}')
    sio.emit('story_time_broadcast', {'storyTimeUuid': player_uuid})

def tongue_twister_broadcast(player_uuid):
    """Broadcast the tongue twister info to all players """
    global game
    print(f'INFO: Broadcasting tongue twister to all players except {player_uuid}')
    sio.emit('tongue_twister_broadcast', {'tongueTwisterUuid': player_uuid})

def charades_broadcast(player_uuid):
    """Broadcast the charades info to all players """
    global game
    print(f'INFO: Broadcasting charades to all players except {player_uuid}')
    sio.emit('charades_broadcast', {'charadesUuid': player_uuid})

def dance_move_broadcast(player_uuid):
    """Broadcast the dance move info to all players """
    global game
    print(f'INFO: Broadcasting dance move to all players except {player_uuid}')
    sio.emit('dance_move_broadcast', {'danceMoveUuid': player_uuid})

def impersonation_broadcast(player_uuid):
    """Broadcast the impersonation info to all players """
    global game
    print(f'INFO: Broadcasting impersonation to all players except {player_uuid}')
    sio.emit('impersonation_broadcast', {'impersonationUuid': player_uuid})

def two_truths_and_a_lie_broadcast(player_uuid):
    """Broadcast the two truths and a lie info to all players """
    global game
    print(f'INFO: Broadcasting two truths and a lie to all players except {player_uuid}')
    sio.emit('two_truths_and_a_lie_broadcast', {'twoTruthsAndALieUuid': player_uuid})

def singing_challenge_broadcast(player_uuid):
    """Broadcast the singing challenge info to all players """
    global game
    print(f'INFO: Broadcasting singing challenge to all players except {player_uuid}')
    sio.emit('singing_challenge_broadcast', {'singingChallengeUuid': player_uuid})

def drawing_challenge_broadcast(player_uuid):
    """Broadcast the drawing challenge info to all players """
    global game
    print(f'INFO: Broadcasting drawing challenge to all players except {player_uuid}')
    sio.emit('drawing_challenge_broadcast', {'drawingChallengeUuid': player_uuid})

def pictionary_broadcast(player_uuid):
    """Broadcast the pictionary info to all players """
    global game
    print(f'INFO: Broadcasting pictionary to all players except {player_uuid}')
    sio.emit('pictionary_broadcast', {'pictionaryUuid': player_uuid})

def memory_game_broadcast(player_uuid):
    """Broadcast the memory game info to all players """
    global game
    print(f'INFO: Broadcasting memory game to all players except {player_uuid}')
    sio.emit('memory_game_broadcast', {'memoryGameUuid': player_uuid})

def funny_faces_broadcast(player_uuid):
    """Broadcast the funny faces info to all players """
    global game
    print(f'INFO: Broadcasting funny faces to all players except {player_uuid}')
    sio.emit('funny_faces_broadcast', {'funnyFacesUuid': player_uuid})

def pose_master_broadcast(player_uuid):
    """Broadcast the pose master info to all players """
    global game
    print(f'INFO: Broadcasting pose master to all players except {player_uuid}')
    sio.emit('pose_master_broadcast', {'poseMasterUuid': player_uuid})

def riddle_broadcast(player_uuid):
    """Broadcast the riddle info to all players """
    global game
    print(f'INFO: Broadcasting riddle to all players except {player_uuid}')
    sio.emit('riddle_broadcast', {'riddleUuid': player_uuid})

def compliment_time_broadcast(player_uuid):
    """Broadcast the compliment time info to all players """
    global game
    print(f'INFO: Broadcasting compliment time to all players except {player_uuid}')
    sio.emit('compliment_time_broadcast', {'complimentTimeUuid': player_uuid})

def select_player_broadcast(player_uuid, sips):
    """Broadcast a message to select a player with the given number of sips."""
    global game
    print('INFO: Broadcasting select player')
    data = {
        'playerUuid': player_uuid,
        'sips': sips,
    }
    print(f'INFO: Broadcasting select player to all players except {player_uuid}')
    print(f"INFO: Data: {data}")
    for player in game.players:
        if player.user_uuid != player_uuid:
            sio.emit('select_player_broadcast', data, room=player.wsId)


def select_player(player_uuid, sips):
    """Send the number of sips to give to the player who has to give"""
    global game
    print('INFO: Broadcasting select player')
    sid = next((p.wsId for p in game.players if p.user_uuid == player_uuid), None)
    suggested_players, other_players = game.get_suggested_players()
    # Convertir les objets Player en dictionnaires
    suggested_players_uuid = [p.user_uuid for p in suggested_players]
    other_players_uuid = [p.user_uuid for p in other_players]

    data = {
        'playerUuid': player_uuid,
        'sips': sips,
        'suggestedPlayers': suggested_players_uuid,
        'otherPlayers': other_players_uuid
    }
    print(f'INFO: Broadcasting select player to {player_uuid}')
    print(f"INFO: Data: {data}")
    sio.emit('select_player', data, room=sid)


def count_sips_broadcast(player_uuid):
    """Broadcast the count of sips for a specific player."""
    global game
    print(f'INFO: Broadcasting count sips to all players except {player_uuid}')
    for player in game.players:
        if player.user_uuid != player_uuid:
            sio.emit('count_sips_broadcast', {'playerUuid': player_uuid}, room=player.wsId)


def count_sips(counter_uuid, player_uuid):
    """Send the number of sips to drink to the player who has to drink"""
    global game
    sid = next((p.wsId for p in game.players if p.user_uuid == counter_uuid), None)
    print(f"INFO: Broadcasting count sips to {counter_uuid}")
    data = {
        'playerUuid': player_uuid,
        'counterUuid': counter_uuid
    }
    print(f"INFO: Data: {data}")
    sio.emit('count_sips', data, room=sid)


def servants_broadcast(master_uuid):
    """Broadcast the servants info to all players except the master."""
    global game
    print(f'INFO: Broadcasting servants to all players except {master_uuid}')
    for player in game.players:
        if player.user_uuid != master_uuid:
            sio.emit('servants_broadcast', {'masterUuid': master_uuid}, room=player.wsId)


def validate_servants_broadcast(master_uuid, servant_uuid):
    """Broadcast validation of servant assignment."""
    global game
    print(f"INFO: Broadcasting validate servants to all players except {master_uuid} and {servant_uuid}")
    data = {
        'masterUuid': master_uuid,
        'servantUuid': servant_uuid
    }
    print(f"INFO: Data: {data}")
    for player in game.players:
        if player.user_uuid != game.current_player:
            sio.emit('validate_servants_broadcast', data, room=player.wsId)


def servants(master_uuid):
    """Send the number of sips to drink to the player who has to drink"""
    global game
    sid = next((p.wsId for p in game.players if p.user_uuid == master_uuid), None)
    suggested_players, other_players = game.get_suggested_players()
    # Convertir les objets Player en dictionnaires
    suggested_players_uuid = [p.user_uuid for p in suggested_players]
    other_players_uuid = [p.user_uuid for p in other_players]
    print(f"INFO: Broadcasting servants to {master_uuid}")
    data = {
        'masterUuid': master_uuid,
        'suggestedPlayers': suggested_players_uuid,
        'otherPlayers': other_players_uuid
    }
    sio.emit('servants', data, room=sid)


def custom_rule_broadcast(player_uuid, rule):
    """Broadcast a custom rule to all players except the specified player."""
    global game
    print(f'INFO: Broadcasting custom rule to all players except {player_uuid}')
    data = {
        'playerUuid': player_uuid,
        'rule': rule
    }
    print(f'INFO: data: {data}')
    for player in game.players:
        if player.user_uuid != player_uuid:
            sio.emit('custom_rule_broadcast', data, room=player.wsId)


def custom_rule(player_uuid, rule):
    """Send the number of sips to drink to the player who has to play"""
    global game
    sid = next((p.wsId for p in game.players if p.user_uuid == player_uuid), None)
    print(f'INFO: Broadcasting custom rule to {player_uuid}')
    data = {
        'rule': rule,
        'playerUuid': player_uuid
    }
    print(f'INFO: data: {data}')
    sio.emit('custom_rule', data, room=sid)


# Game broadcast functions
def broadcast_draw_card():
    global game
    print(f"INFO: Broadcasting draw card: {game.current_card}")
    data = {
        'current_card': game.current_card,
        'remaining_cards': game.card_manager.remaining_cards()
    }
    print(f"INFO: Data: {data}")
    sio.emit('draw_card', data)


def broadcast_new_turn():
    global game
    print(f"INFO: Broadcasting new turn: {game.current_player.user_name}")
    data = {
        'current_player_name': game.current_player.user_name,
        'current_player_uuid': game.current_player.user_uuid,
        'current_player_index': game.current_player_index,
    }
    print(f"INFO: Data: {data}")
    sio.emit('new_turn', data)


def broadcast_end_game():
    global game
    print(f"INFO: Broadcasting end game")
    sio.emit('end_game', {"game_state": start_game.game_state})


@sio.event
def get_game_state(sid):
    global game
    print(f"INFO: Game state request from {sid}")
    sio.emit('game_state_update', game.get_game_state(), room=sid)


def assign_new_master():
    global master, players
    if players:
        master = players[0]  # Assign the first player in the list as the new master
        master.set_isMaster(True)
        print(f"INFO: New master assigned: {master.user_name}")
        broadcast_player_list()  # Broadcast the updated player list to all clients

@sio.event
def send_game_alcohol(sid, data):
    """get the game alcohol from the player"""
    print(f"INFO: Game alcohol send from {sid}")
    global game
    player = next((p for p in game.players if p.wsId == sid), None)
    if player:
        player.game_alcohol = data['gameAlcohol']
        print(f"INFO: Game alcohol set to {player.game_alcohol} for player {player.user_name}")
        broadcast_player_list()
    else:
        print(f'ERROR: Player not found in player list: {sid}')

@sio.event
def validate_sips(sid, data):
    global game
    sips = data['sips']
    player = next((p for p in game.players if p.wsId == sid), None)
    print(f"INFO: Player {player.user_name} validated his {sips} sips")
    if sips == -1:
        sips = 10
    player.game_sips += sips
    game.next_turn()
    broadcast_new_turn()


@sio.event
def validate_servants(sid, data):
    global game
    player = next((p for p in game.players if p.wsId == sid), None)

    # Vérifiez si le joueur existe avant d'accéder à ses attributs
    if player:
        print(f"INFO: Player {player.user_name} validated his servants")
        servant_uuid = data['selectedServant']
        print(f"INFO: Servant {servant_uuid} validated")

        if game.current_player_index == game.players.index(player):
            validate_servants_broadcast(player.user_uuid, servant_uuid)
            game.add_servant(player.user_uuid, servant_uuid)
            game.next_turn()
            broadcast_new_turn()
        else:
            print(f'ERROR: Invalid action by {sid}')
    else:
        print(f'ERROR: Player not found for SID: {sid}')


@sio.event
def send_selected_players(sid, data):
    """get the selected players and send the number of sips to drink to each of them"""
    global game
    player = next((p for p in game.players if p.wsId == sid), None)
    print(f"INFO: Selected players send from {player.user_name}")
    if player and game.current_player_index == game.players.index(player):
        # get selected players list
        selected_players = data['selectedPlayers']
        # get sips counts
        sips = data['sips']

        if len(selected_players) == 0:
            print('ERROR: No player selected')
            return

        if len(selected_players) == 1:
            print(f'INFO: one Selected player: {selected_players[0]}')
            drink(selected_players[0], sips[0])
            drink_broadcast(selected_players[0], sips[0])
        else:
            print(f'INFO: multiple Selected players: {selected_players}')
            for player_uuid, sips_count in zip(selected_players, sips):
                drink(player_uuid, sips_count)
                drink_broadcast(player_uuid, sips_count)

    else:
        print(f'ERROR: Invalid action by {sid}')

@sio.event
def draw_card(sid):
    global game
    print(f"INFO: Draw card request from {sid}")
    # check card remaining
    if game.card_manager.remaining_cards() == 0:
        print('ERROR: No more cards')
        return
    else :
        player = next((p for p in game.players if p.wsId == sid), None)
        if player and game.current_player_index == game.players.index(player):
            # Handle the draw card action
            game.handle_player_action(player.user_uuid, 'draw_card')
            broadcast_draw_card()
            if game.current_category == 'drink':
                print(f"INFO: Broadcasting drink")
                # if player.game_alcohol == 0: # If the player has no alcohol, he can't drink
                #     non_alcoholic_rule = game.rules.get_non_drinking_challenge()
                #     custom_rule(player.user_uuid, non_alcoholic_rule)
                #     custom_rule_broadcast(player.user_uuid, non_alcoholic_rule)
                # else: # If the player has alcohol, he can drink
                sips_count = game.rules.get_sips_count(game.current_rule)
                drink(player.user_uuid, sips_count)
                # check if the player have a servant
                servant_list = game.get_servants_of_master(player.user_uuid)
                if len(servant_list) > 0:
                    print(f"INFO: Broadcasting drink servant")
                    for servant in servant_list:
                        drink_servant(servant, sips_count, player.user_uuid)
                    drinkers = [servant for servant in servant_list]
                    drinkers.append(player.user_uuid)
                    team_drink_broadcast(drinkers, sips_count)
                else:
                    drink_broadcast(player.user_uuid, sips_count)

            elif game.current_category == 'distribute':
                print(f"INFO: Broadcasting select player")
                sips_count = game.rules.get_sips_count(game.current_rule)
                select_player(player.user_uuid, sips_count)
                select_player_broadcast(player.user_uuid, sips_count)

            elif game.current_category == 'special':
                if game.current_rule == 'servant':
                    print(f"INFO: Broadcasting servants")
                    servants(player.user_uuid)
                    servants_broadcast(player.user_uuid)
                elif game.current_rule == 'shifumi':
                    print(f"INFO: Broadcasting shifumi")
                    shifumi_players_uuid = [p.user_uuid for p in game.players if p.user_uuid != player.user_uuid]
                    print(f"INFO: Shifumi players: {player.user_uuid} vs {shifumi_players_uuid}")
                    # broadcast shifumi event to master and shifumi players
                    # informe player
                    game.shifumi_master = player.user_uuid
                    game.shifumi_opponent = shifumi_players_uuid
                    game.shifumi_turns = len(shifumi_players_uuid)
                    game.shifumi_current_turn = 0
                    shifumi(player.user_uuid)
                    shifumi_broadcast(player.user_uuid)
                    # wait for player response
                    # sleep 1 second to let the client receive the broadcast
                    # for all players create a turn & broadcast the shifumi round turn
                        # wait for bothe player are ready
                        # broadcast the shifumi round event
                        # wait for the result
                        # add round result to the round result
                        # broadcast the shifumi result event
                    # broadcast the drink event to the master


                elif game.current_rule == 'queens':
                    print(f"INFO: Broadcasting queens")
                    # get all players in the game where .gender == female
                    queens = [p for p in game.players if p.gender != 'male']
                    queen_broadcast(player.user_uuid)
                    team_drink_broadcast([queen.user_uuid for queen in queens], 1)
                    for queen in queens:
                        drink(queen.user_uuid, 1)
                elif game.current_rule == 'kings':
                    print(f"INFO: Broadcasting kings")
                    kings = [p for p in game.players if p.gender != 'female']
                    king_broadcast(player.user_uuid)
                    team_drink_broadcast([king.user_uuid for king in kings], 1)
                    for queen in kings:
                        drink(queen.user_uuid, 1)
                elif game.current_rule == 'thumb_master':
                    print(f"INFO: Broadcasting thumb master")
                    game.thumb_master = player.user_uuid
                    thumb_master_broadcast(player.user_uuid)
                    thumb_master(player.user_uuid)
                else:
                    custom_rule(player.user_uuid, game.current_rule)
                    custom_rule_broadcast(player.user_uuid, game.current_rule)
        else:
            print(f'ERROR: Invalid action by {sid}')

@sio.event
def send_count_sips(sid, data):
    """get the number of sips to drink from the player"""
    print(f"INFO: Count sips send from {sid}")
    global game
    sips = data['sips']
    drink_broadcast(game.current_player.user_uuid, sips)
    drink(game.current_player.user_uuid, sips)



# run the application
if __name__ == '__main__':
    eventlet.wsgi.server(eventlet.listen(('', PORT)), app)

