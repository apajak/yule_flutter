import random


class CardManager:
    def __init__(self):
        self.deck = self.create_deck()

    def create_deck(self):
        suits = ['H', 'D', 'C', 'S']
        ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
        return [f'{rank}{suit}' for suit in suits for rank in ranks]

    def shuffle_deck(self):
        random.shuffle(self.deck)

    def deal_card(self):
        return self.deck.pop() if self.deck else None

    def deal_hand(self, hand_size):
        return [self.deal_card() for _ in range(hand_size)] if len(self.deck) >= hand_size else []

    def remaining_cards(self):
        return len(self.deck)


# Example of Usage:
# if __name__ == '__main__':
#
#     # Utilisation de la classe
#     card_manager = CardManager()
#     card_manager.shuffle_deck()
#
#     # Distribuer une carte
#     print(card_manager.deal_card())
#
#     # Distribuer une main de 5 cartes
#     print(card_manager.deal_hand(5))
#
#     # Afficher le nombre de cartes restantes dans le jeu
#     print(card_manager.remaining_cards())
