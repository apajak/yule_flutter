import random


class BarbuRules:
    def __init__(self):
        self.drink_rules = {
            '2': "get_2",
            '3': "get_3",
            '4': "get_4",
            'A': "get_shot"
        }

        self.distribute_rules = {
            '2': "give_2",
            '3': "give_3",
            '4': "give_4"
        }

        self.special_rules = {
            '5': "servant",
            '6': "shifumi",
            '7': "never_have_i_ever",
            '8': "thumb_master",
            '9': "i_have",
            '10': "i_packed_my_bag",
            'J': "back_to_back",
            'Q': "queens",
            'K': "kings"
        }

        self.non_alcohol_challenges = [
            "truth_or_dare",
            "story_time",
            "tongue_twister",
            "charades",
            "dance_move",
            "impersonation",
            "two_truths_and_a_lie",
            "singing_challenge",
            "drawing_challenge",
            "pictionary",
            "memory_game",
            "funny_faces",
            "pose_master",
            "riddle",
            "compliment_time"
        ]


    def apply_rule(self, card):
        rank, suit = card[:-1], card[-1]
        if rank in self.distribute_rules and suit in ['H', 'D']:
            rule_description = f"{self.distribute_rules[rank]}"
            rule_category = "distribute"
        elif rank in self.drink_rules:
            rule_description = f"{self.drink_rules[rank]}"
            rule_category = "drink"
        elif rank in self.special_rules:
            rule_description = self.special_rules[rank]
            rule_category = "special"
        else:
            rule_description = "default_action"
            rule_category = "default"
        return rule_description, rule_category

    def get_non_drinking_challenge(self):
        return random.choice(self.non_alcohol_challenges)

    @staticmethod
    def get_sips_count(rule_description):
        # Return the number of sips to drink based on the rule description ("get_2", "give_3", etc.)
        # if rule_description is get_shot, return 10
        # else return the split "get_4" -> 4
        if rule_description == "get_shot":
            return -1
        else:
            return int(rule_description.split("_")[1])


# Example of Usage
# if __name__ == '__main__':
#     barbu_rules = BarbuRules()
#     print(barbu_rules.apply_rule('4D'))
#     print(barbu_rules.get_non_drinking_challenge())
#     print(barbu_rules.get_sips_count("get_4"))
#     print(barbu_rules.get_sips_count("get_shot"))
#     print(barbu_rules.get_sips_count("give_3"))
