from cards import CardManager
from rules import BarbuRules
from suggestion import SipSuggestions

class BarbuGame:
    def __init__(self, players):
        self.players = players  # List of players in the game
        self.removed_players = []  # List of players who left the game
        self.card_manager = CardManager()  # Instance of the CardManager for card operations
        self.current_player_index = 0  # Index of the current player's turn
        self.game_state = "waiting"  # Could be "waiting", "started", "finished"
        self.current_rule = None  # Current rule applied to the game
        self.current_card = None  # Current card played
        self.current_category = None  # Current category of the rule
        self.suggested_players = []  # List of players suggested by the current rule
        self.rules = BarbuRules()  # Instance of the BarbuRules class
        self.suggestions = SipSuggestions()  # Instance of the SipSuggestion class
        self.current_player = None  # Current user
        self.selected_player = None  # Selected user
        self.servants = {}  # Dictionary of servants
        self.thumb_master = None  # Thumb master
        self.shifumi_master = None  # Shifumi master
        self.shifumi_opponent = None  # Shifumi opponent
        self.shifumi_turns = None  # Shifumi turns
        self.shifumi_current_turn = 0  # Shifumi current turn
        self.shifumi_sips = 0  # Shifumi result
        self.shifumi_current_opponent = None  # Shifumi current opponent
        self.shifumi_validation = []  # Shifumi validation

    def add_player(self, player):
        # Logic to add a player to the game
        if player.user_uuid in [p.user_uuid for p in self.players]:
            raise Exception("Player already in game")
        if player.user_uuid in [p.user_uuid for p in self.removed_players]:
            # Re-add a player who left the game
            old_player = next(p for p in self.removed_players if p.user_uuid == player.user_uuid)
            self.removed_players.remove(old_player)
            self.players.append(player)
        else:
            self.players.append(player)

    def remove_player(self, player_id):
        # Logic to remove a player from the game
        player = next(p for p in self.players if p.user_uuid == player_id)
        self.players = [p for p in self.players if p.user_uuid != player_id]
        self.removed_players.append(player)


    def start_game(self):
        # Logic to start the game
        self.game_state = "started"
        self.card_manager.shuffle_deck()
        # Deal initial cards, set up game state, etc.

    def next_turn(self):
        # Logic to pass the turn to the next player
        self.current_player_index = (self.current_player_index + 1) % len(self.players)
        self.current_player = self.players[self.current_player_index]

    def handle_player_action(self, player_id, action):
        if action == 'draw_card':
            # self.current_card = self.card_manager.deal_card()
            self.current_card = "6C"
            player = next(p for p in self.players if p.user_uuid == player_id)
            self.current_rule, self.current_category = self.rules.apply_rule(self.current_card)  # Capture both values
            self.current_player = player
            # You can now use rule_description and rule_category in your game logic
            print(f"Card drawn: {self.current_card}, Rule: {self.current_rule}, Category: {self.current_category}")

    def get_current_player_uuid(self):
        # Return the current player
        return self.players[self.current_player_index].to_simple_player().user_uuid

    def get_current_rule(self):
        # Return the current rule
        return {
            "current_card": self.current_card,
            "rule_description": self.current_rule,
            "rule_category": self.current_category,
            "current_player": self.current_player.to_simple_player().user_uuid,
            "remaining_cards": self.card_manager.remaining_cards()
        }

    def get_suggested_players(self):
        # Return the suggested players
        suggested_players, other_players = self.suggestions.suggest_players(self.current_player, self.players)
        return suggested_players, other_players

    def add_servant(self, master_uuid, servant_uuid):
        """Add a servant to a master."""
        self.servants[servant_uuid] = master_uuid

    def remove_servant(self, servant_uuid):
        """Remove a servant from the master."""
        if servant_uuid in self.servants:
            del self.servants[servant_uuid]

    def is_servant(self, player_uuid):
        """Check if a player is a servant."""
        return player_uuid in self.servants

    def get_master_of_servant(self, servant_uuid):
        """Get the master of a specific servant."""
        return self.servants.get(servant_uuid)

    def get_servants_of_master(self, master_uuid):
        """Get all servants of a specific master."""
        return [servant_uuid for servant_uuid, master_id in self.servants.items() if master_id == master_uuid]

    def get_game_state(self):
        # Return current state of the game
        print('get_game_state')
        print(self.players)
        return {
            "players": [player.to_simple_player().to_dict() for player in self.players],
            "current_turn": self.current_player_index,
            "game_state": self.game_state,
            "current_rule": self.current_rule,
            "current_category": self.current_category,
            "current_card": self.current_card,
            "remaining_cards": self.card_manager.remaining_cards()
        }
