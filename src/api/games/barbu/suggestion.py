class SipSuggestions:
    def suggest_players(self, current_player, players):
        # Separate players based on eligibility to drink
        eligible_players = [
            p for p in players if p.user_uuid != current_player.user_uuid
            and p.game_alcohol != 0
            and not self.has_reached_sip_limit(p)
        ]
        ineligible_players = [
            p for p in players if p.user_uuid != current_player.user_uuid
            and (p.game_alcohol == 0 or self.has_reached_sip_limit(p))
        ]

        # Order eligible players who are the farthest from their sip limit
        eligible_players.sort(key=lambda p: self.calculate_sip_limit(p.game_alcohol) - p.game_sips, reverse=True)

        # Separate eligible players into compatible and other players
        compatible_players = [p for p in eligible_players if self.is_compatible(current_player, p)]
        other_eligible_players = [p for p in eligible_players if p not in compatible_players]

        # Return two lists (suggestion in order of priority, other players)
        return compatible_players + other_eligible_players, ineligible_players

    @staticmethod
    def is_compatible(player1, player2):
        # Check if two players are compatible based on couple state and sexual orientation
        if player1.couple_state == 'single' and player2.couple_state == 'single':
            if player1.sexual_orientation == 'heterosexual' and player1.gender != player2.gender:
                return True
            elif player1.sexual_orientation == 'homosexual' and player1.gender == player2.gender:
                return True
        return False

    def has_reached_sip_limit(self, player):
        # Check if a player has reached their sip limit
        sip_limit = self.calculate_sip_limit(player.game_alcohol)
        return player.game_sips >= sip_limit

    @staticmethod
    def calculate_sip_limit(game_alcohol_level):
        # Define sip limit based on the game_alcohol level (0 to 5)
        # Adjust these values based on your game's design and balance
        sip_limits = {
            0: 0,  # Players who don't drink
            1: 5,  # Light drinkers
            2: 10,
            3: 15,
            4: 20,
            5: 25  # Heavy drinkers
        }
        return sip_limits.get(game_alcohol_level, 0)