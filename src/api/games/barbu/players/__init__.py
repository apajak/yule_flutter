class Player:
    def __init__(self, user_uuid, user_name, gender, sexual_orientation, couple_state, alcohol, total_sips,
                 profile_picture, game_alcohol=-1):
        self.wsId = None
        self.user_uuid = user_uuid
        self.user_name = user_name
        self.gender = gender # ["female", "male", "ND"]
        self.sexual_orientation = sexual_orientation # ["heterosexual", "homosexual", "ND"]
        self.couple_state = couple_state # ["single", "couple", "open_relationship" "ND"]
        self.alcohol = alcohol # ["never", "occasionally", "regularly", "ND"]
        self.total_sips = total_sips
        self.game_alcohol = game_alcohol # drink factor for the game [0, 5]
        self.game_sips = 0  # track the number of sips the player has to drink
        self.profile_picture = profile_picture
        self.is_ready = False
        self.isMaster = False

        if self.game_alcohol == -1:
            self.set_game_alcohol()

    def set_game_alcohol(self):
        if self.alcohol == "never":
            self.game_alcohol = 0
        elif self.alcohol == "occasionally":
            self.game_alcohol = 2
        elif self.alcohol == "regularly":
            self.game_alcohol = 4
        else:
            self.game_alcohol = 3

    def to_dict(self):
        return {
            "user_uuid": self.user_uuid,
            "user_name": self.user_name,
            "gender": self.gender,
            "sexual_orientation": self.sexual_orientation,
            "couple_state": self.couple_state,
            "alcohol": self.alcohol,
            "total_sips": self.total_sips,
            "game_alcohol": self.game_alcohol,
            "game_sips": self.game_sips,
            "profile_picture": self.profile_picture,
        }
    def set_wsId(self, wsId):
        self.wsId = wsId

    def set_is_ready(self, is_ready):
        self.is_ready = is_ready

    def set_isMaster(self, isMaster):
        self.isMaster = isMaster

    def to_simple_player(self):
        return SimplePlayer(self.user_uuid, self.user_name, self.profile_picture, self.game_alcohol, self.gender)

    def __repr__(self):
        return (f"Player({self.user_uuid}, {self.user_name}, {self.gender}, {self.sexual_orientation},"
                f" {self.couple_state}, {self.alcohol}, {self.total_sips}, {self.game_alcohol}, {self.game_sips} "
                f"{self.profile_picture})")


class SimplePlayer:
    def __init__(self, user_uuid, user_name, profile_picture, game_alcohol=3, gender="ND"):
        self.wsId = None
        self.user_uuid = user_uuid
        self.user_name = user_name
        self.profile_picture = profile_picture
        self.game_alcohol = game_alcohol
        self.gender = gender

    def to_dict(self):
        return {
            "user_uuid": self.user_uuid,
            "user_name": self.user_name,
            "profile_picture": self.profile_picture,
            "game_alcohol": self.game_alcohol,
            "gender": self.gender
        }

    def set_wsId(self, wsId):
        self.wsId = wsId

    def __repr__(self):
        return f"SimplePlayer({self.user_uuid}, {self.user_name}, {self.profile_picture})"