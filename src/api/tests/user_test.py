from yule_modules.user.user_manager import User
from datetime import date

# Create a User object
user = User(user_name="test", first_name="test", last_name="test", email="test",
            birth_date=date(1998, 10, 11), gender="test", sexual_orientation="test", couple_state="test",
            alcohol="test", total_sips="test", is_connect="test")

# Print the User object
print(user)

# Set and retrieve the user's password
user.set_password("test")
print(user.get_password())

# Set and retrieve the user's ID
user.user_id = 1
print(user.get_user_id())

# Convert the User object to a tuple and back to an object
user_tuple = user.to_tuple()
user_from_tuple = User.from_tuple(user_tuple)
print(user_from_tuple)

# Convert the User object to a dictionary and back to an object
user_dict = user.to_dict()
user_from_dict = User.from_dict(user_dict)
print(user_from_dict)

# Print end of user test
print("End of user test")
