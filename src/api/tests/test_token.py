from yule_modules.user.token_manager import IdentificationToken, MailToken
from yule_modules.user.user_manager import User
from yule_modules.database.token import TokenDatabase
from yule_modules.database.user import UserDatabase
from dotenv import load_dotenv
import os
from yule_modules.database.manager import Database

load_dotenv("../.env")
db_config = {
    "host": os.getenv("DATABASE_HOST"),
    "user": os.getenv("DATABASE_USER"),
    "password": os.getenv("DATABASE_PASSWORD"),
    "database": os.getenv("DATABASE_NAME"),
}
print(db_config)
db = Database(**db_config)
user_db = UserDatabase(**db_config)
token_db = TokenDatabase(**db_config)

# Create a IdentificationToken object
identification_token = IdentificationToken(token_id=1, user_uuid="test", timestamp="test")
# Print the IdentificationToken object
print(identification_token)
# Set and retrieve the token
identification_token.set_token()
print(identification_token.get_token())
# Convert the IdentificationToken object to a tuple and back to an object
identification_token_tuple = identification_token.to_tuple()
identification_token_from_tuple = IdentificationToken.from_tuple(identification_token_tuple)
print(identification_token_from_tuple)
# Convert the IdentificationToken object to a dictionary and back to an object
identification_token_dict = identification_token.to_dict()
identification_token_from_dict = IdentificationToken.from_dict(identification_token_dict)
print(identification_token_from_dict)
# Print end of identification token test

# Create a MailToken object from existing IdentificationToken object
mail_token = MailToken(token_id=identification_token.token_id, user_uuid=identification_token.user_uuid,
                       timestamp=identification_token.timestamp, token=identification_token.token)
# Print the MailToken object
print(mail_token)
# Set and retrieve the token
mail_token.set_token()
print(mail_token.get_token())

# add token to database
user = user_db.get_user_by_id(1)
print(user)
user.set_identification_token()
print(user.identification_token)
try :
    token_db.insert_identification_token(user.identification_token)
except Exception as e:
    print(e)
    token_db.update_identification_token(user.identification_token)
print(token_db.get_identification_token(user.user_uuid))
token_db.delete_identification_token(user.user_uuid)
print(token_db.check_identification_token(user.identification_token.token))

token_db.insert_identification_token(user.identification_token)
print(token_db.check_identification_token(user.identification_token.token))



print("End of identification token test")


if __name__ == "__main__":
    pass
