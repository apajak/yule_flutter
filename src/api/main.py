from typing import Optional

from fastapi import FastAPI, File, UploadFile, Query,HTTPException,Header,Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import JSONResponse
from pathlib import Path
from dotenv import load_dotenv
import mysql.connector
import os
from yule_modules.database.manager import Database
from yule_modules.database.token import TokenDatabase
from yule_modules.database.user import UserDatabase
from yule_modules.database.docker import DockerDatabase
from yule_modules.database.FCM_token import FCMTokenDatabase
from yule_modules.docker import DockerManager
from yule_modules.security import UpdatePasswordVerifier
from yule_modules.user.token_manager import FCMToken
from yule_modules.user.user_manager import User, UserCreate, UserCredentials, UserUpdate, UserUpdatePassword, \
    FriendRequest, UserToken, RemoveGame, FcmTokenModel, GetGame, InviteGame
from yule_modules.security.register import RegisterFormVerifier
from yule_modules.security.updateProfile import ProfileUpdateVerifier
from yule_modules.security.login import LoginFormVerifier
from yule_modules.firebase import Firebase
import uvicorn
import bcrypt
import secrets
from PIL import Image
import io

# Load the environment variables
load_dotenv(".env")
db_config = {
    "host": os.getenv("DATABASE_HOST"),
    "user": os.getenv("DATABASE_USER"),
    "password": os.getenv("DATABASE_PASSWORD"),
    "database": os.getenv("DATABASE_NAME"),
}
## Global variables
# Create the FastAPI app
app = FastAPI()
# Create the database object
db = Database(**db_config)
user_db = UserDatabase(**db_config)
token_db = TokenDatabase(**db_config)
docker_db = DockerDatabase(**db_config)
FCMToken_db = FCMTokenDatabase(**db_config)
docker_manager = DockerManager(docker_db)
PP_DIRECTORY = Path("profile_pics")
PP_DIRECTORY.mkdir(parents=True, exist_ok=True)
host = "http://localhost:8000"
app.mount("/profile_pics", StaticFiles(directory=PP_DIRECTORY), name="profile_pics")
firebase_admin = Firebase(os.getenv("FIREBASE_SERVICE_ACCOUNT_KEY_PATH"))

@app.get("/")
async def root():
    """Returns a welcome message"""
    return {"message": "Welcome to Yule API!"}


@app.get("/ping")
async def ping():
    """Checks if the API is working fine"""
    print("INFO:ping")
    return {"message": "pong"}


@app.get("/database_ping")
async def database_ping():
    """Checks if the database is working fine"""
    print("INFO:database_ping")
    try:
        # Connect to the database
        connection = mysql.connector.connect(**db_config)
        cursor = connection.cursor()
        cursor.execute("SELECT 1")
        # Fetch the result
        result = cursor.fetchone()
        # Close the cursor and connection
        cursor.close()
        connection.close()
        print(f"INFO:Database is working fine: {result}")
        return {"message": "Database is working fine", "result": result}
    except Exception as e:
        print(f"ERROR:Failed to connect to database: {str(e)}")
        return {"message": f"Database error: {str(e)}"}


@app.post("/register")
async def register(user_data: UserCreate):
    """Registers a new user"""
    print("INFO:register")
    # check entry
    register_form_verifier = RegisterFormVerifier(user_data)
    valid, failed_tests = register_form_verifier.check_form()

    if not valid:
        return {"message": "Submitted form not satisfy requirements", "value": False, "failed_tests": failed_tests}
    try:
        user = User(
            user_name=user_data.user_name,
            first_name=user_data.first_name,
            last_name=user_data.last_name,
            email=user_data.email,
            birth_date=user_data.birth_date,
            gender=user_data.gender,
            sexual_orientation=user_data.sexual_orientation,
            couple_state=user_data.couple_state,
            alcohol=user_data.alcohol,
            total_sips=0,
            is_connect=0,
        )
        # check if user exists
        if user_db.user_exist(user.email, user.user_name):
            return {"message": "User already exists", "value": False}

        # Hash the password
        user.set_password(bcrypt.hashpw(user_data.password.encode("utf-8"), bcrypt.gensalt()))
        user_id = user_db.insert_user(user)
        print(f"INFO:User created successfully: {user_id}")
        return {"message": "User created successfully", "value": True, "user_id": user_id}
    except Exception as e:
        print(f"ERROR:Failed to create user: {str(e)}")
        return {"message": f"Failed to create user: {str(e)}", "value": False}

@app.post("/update_profile")
async def update_profile(user_data: UserUpdate):
    """Update a user profile"""
    # check token validity
    print("INFO:update_profile")
    user = token_db.get_user_by_identification_token(user_data.identification_token)
    if not user:
        return {"message": "Invalid token", "value": False}
    # check entry
    update_form_verifier = ProfileUpdateVerifier(user_data, user)
    valid, failed_tests = update_form_verifier.check_profile_update()
    if not valid:
        return {"message": "Submitted form not satisfy requirements", "value": False, "failed_tests": failed_tests}

    try:
        user_db.update_user_profile(user.user_id, user_data.user_name, user_data.email, user_data.sexual_orientation,
                                    user_data.couple_state, user_data.alcohol)
        new_user = user_db.get_user_by_id(user.user_id)
        print(f"INFO:new_user: {new_user}")
        return {"message": "User updated successfully", "value": True, "user": new_user.to_dict()}
    except Exception as e:
        print(f"ERROR:Failed to update user: {str(e)}")
        return {"message": f"Failed to update user: {str(e)}", "value": False}

@app.post("/update_password")
async def update_password(password_form: UserUpdatePassword):
    """Update a user password"""
    print("INFO:update_password")
    # check token validity
    user = token_db.get_user_by_identification_token(password_form.identification_token)
    if not user:
        return {"message": "Invalid token", "value": False}

    # check if old password is correct
    if not user_db.check_password(user.user_name, password_form.password):
        return {"message": "Old password is incorrect", "value": False}

    # check entry
    update_password_verifier = UpdatePasswordVerifier(password_form)
    valid, failed_tests = update_password_verifier.check_password()
    if not valid:
        print(f"ERROR:Failed to update user: {failed_tests}")
        return {"message": "Submitted form not satisfy requirements", "value": False, "failed_tests": failed_tests}

    try:
        # Hash the password
        user.set_password(bcrypt.hashpw(password_form.new_password.encode("utf-8"), bcrypt.gensalt()))
        user_db.update_user_password(user.user_id, user.password)
        print(f"INFO:User password updated successfully: {user.user_id}")
        return {"message": "User updated successfully", "value": True}
    except Exception as e:
        print(f"ERROR:Failed to update user: {str(e)}")
        return {"message": f"Failed to update user: {str(e)}", "value": False}


@app.post("/upload/profile-picture")
async def upload_profile_picture(file: UploadFile, user_id: str):
    print("INFO:upload_profile_picture")
    user_id = int(user_id)
    filename = f"profile_picture_{secrets.token_hex(8)}.png"
    file_path = os.path.join(PP_DIRECTORY, filename)

    # Process and save the image
    try:
        # Read the image from the uploaded file
        image_data = await file.read()
        image = Image.open(io.BytesIO(image_data))

        # Resize the image
        image = image.resize((150, 150))

        # check if have already a profile picture
        if user_db.get_user_profile_picture(user_id):
            # delete the old profile picture
            old_url = user_db.get_user_profile_picture(user_id)
            old_profile_picture = old_url.split("/")[-1]
            os.remove(os.path.join(PP_DIRECTORY, old_profile_picture))

        # Save the resized image
        image.save(file_path, format='PNG')

        # Update the user's profile picture
        ## for dev only ##
        user_db.update_user_profile_picture(user_id, f"http://10.0.2.2:8000/profile_pics/{filename}")
        print(f"INFO:Profile picture uploaded successfully: {filename}")
        return JSONResponse(content={"message": "Profile picture uploaded successfully",
                                     "filename": filename,
                                     "value": True,
                                     "url": f"{host}/profile_pics/{filename}"
                                     })
    except Exception as e:
        print(f"ERROR:Failed to upload profile picture: {str(e)}")
        return JSONResponse(content={"message": f"Failed to upload profile picture: {str(e)}",
                                     "filename": filename,
                                     "value": False,
                                     "url": None},
                            status_code=500)


@app.post("/login")
async def login(user_data: UserCredentials):
        """ Login a user """
        print("INFO:login")
        # check input data
        login_form_verifier = LoginFormVerifier(user_data)
        valid, failed_tests = login_form_verifier.check_form()
        if not valid:
            return {"message": "Submitted form not satisfy requirements", "value": False, "failed_tests": failed_tests}
        try:
            # check if user exists
            if not user_db.user_exist(user_data.identifier, user_data.identifier):
                return {"message": "User does not exist", "value": False}
            # check if password is correct
            if not user_db.check_password(user_data.identifier, user_data.password):
                return {"message": "Password is incorrect", "value": False}
            else:
                user = user_db.get_user_by_identifier(user_data.identifier)
                if user:
                    user.is_connect = 1
                    user.set_identification_token()
                    user_db.update_user(user.user_id, user)
                    token_db.insert_identification_token(user.identification_token)
            print(f"INFO:User logged in successfully: {user.user_id}")
            print(f"INFO:User token: {user.get_identification_token()}")
            return {"message": "User logged in successfully", "value": True, "token": user.get_identification_token(),
                    "user": user.to_dict()}
        except Exception as e:
            print(f"ERROR:Failed to login user: {str(e)}")
            return {"message": f"Failed to login user: {str(e)}", "value": False}
@app.get("/get_users_not_friends")
async def get_users_not_friends(user_token: str):
    """ Get all possible future friends (uuid, user_name, profile_picture)"""
    print(f"INFO:get_users_not_friends")
    # check token validity
    user = token_db.get_user_by_identification_token(user_token)
    if not user:
        return {"message": "Invalid token", "value": False}
    try:
        users = user_db.get_users_not_friends(user.user_uuid)
        print(f"INFO:users: {users}")
        return {"message": "Users retrieved successfully", "value": True, "users": users}
    except Exception as e:
        print(f"ERROR:Failed to retrieve users: {str(e)}")
        return {"message": f"Failed to retrieve users: {str(e)}", "value": False}

@app.get("/get_user_friends")
async def get_friends(user_token: str):
    """ Get all friends (uuid, user_name, profile_picture)"""
    # check token validity
    print(f"INFO:get_friends")
    user = token_db.get_user_by_identification_token(user_token)
    if not user:
        return {"message": "Invalid token", "value": False}
    try:
        users = user_db.get_users_friends(user.user_uuid)
        print(f"INFO:friends: {users}")
        return {"message": "Users retrieved successfully", "value": True, "users": users}
    except Exception as e:
        print(f"ERROR:Failed to retrieve users: {str(e)}")
        return {"message": f"Failed to retrieve users: {str(e)}", "value": False}


@app.get("/search_users")
async def search_users(user_token:str, username_slice:str):
    """ Get all possible future friends (uuid, user_name, profile_picture) with username containing username slice"""
    # check token validity
    print(f"INFO:search_users")
    user = token_db.get_user_by_identification_token(user_token)
    if not user:
        return {"message": "Invalid token", "value": False}
    try:
        users = user_db.search_users(user.user_uuid, username_slice)
        print(f"INFO:users: {users}")
        return {"message": "Users retrieved successfully", "value": True, "users": users}
    except Exception as e:
        print(f"ERROR:Failed to retrieve users: {str(e)}")
        return {"message": f"Failed to retrieve users: {str(e)}", "value": False}

@app.post("/send_friend_request")
async def send_friend_request(request: FriendRequest):
    """ Send a friend request """
    print("INFO:send_friend_request")
    # check token validity
    sender = token_db.get_user_by_identification_token(request.user_token)
    if not sender:
        return {"message": "Invalid token", "value": False}
    try:
        # Check if already friends or friend request already sent
        if user_db.check_friendship(sender.user_uuid, request.user_uuid):
            return {"message": "Already friends", "value": False}
        if user_db.check_if_pending_friend_request(sender.user_uuid, request.user_uuid):
            return {"message": "Friend request already sent", "value": False}
        # add friend request
        user_db.add_friend_request(sender.user_uuid, request.user_uuid)
        # send notification
        receiver = user_db.get_user_by_uuid(request.user_uuid)
        if receiver:
            receiver_token = FCMToken_db.get_token_by_user_id(receiver.user_uuid)
            if receiver_token:
                firebase_admin.send_fcm_friend_request_notification(receiver_token.fcm_token, sender.user_name)
        print(f"INFO:Friend request sent successfully: {sender.user_uuid} -> {request.user_uuid}")
        return {"message": "Friend request sent successfully", "value": True}
    except Exception as e:
        print(f"ERROR:Failed to send friend request: {str(e)}")
        return {"message": f"Failed to send friend request: {str(e)}", "value": False}

@app.post("/send_fcm_token")
async def send_fcm_token(request: FcmTokenModel):
    """ Send a FCM token to server database"""
    # Check token validity
    print("INFO:send_fcm_token")
    user = token_db.get_user_by_identification_token(request.user_token)
    if not user:
        return {"message": "Invalid token", "value": False}
    # FCM token object
    fcm_token = FCMToken(user.user_uuid, request.fcm_token)
    try:
        # Check if user already has a FCM token
        existing_fcm_token = FCMToken_db.get_token_by_user_id(user.user_uuid)
        # if exist check if it's the same token or not. Update if not the same token else do nothing
        if existing_fcm_token:
            if existing_fcm_token.fcm_token == request.fcm_token:
                print("INFO:FCM token already exist")
                return {"message": "FCM token already exist", "value": False}
            else:
                FCMToken_db.update_token(fcm_token)
        else:
            # insert new token
            FCMToken_db.insert_token(fcm_token)
        print(f"INFO:FCM token processed successfully: {fcm_token}")
        return {"message": "FCM token processed successfully", "value": True}
    except Exception as e:
        print(f"ERROR:Failed to process FCM token: {str(e)}")
        return {"message": f"Failed to process FCM token: {str(e)}", "value": False}



@app.post("/accept_friend_request")
async def accept_friend_request(request: FriendRequest):
    """ Accept a friend request """
    print("INFO:accept_friend_request")
    # check token validity
    user = token_db.get_user_by_identification_token(request.user_token)
    if not user:
        print("ERROR:Invalid token")
        return {"message": "Invalid token", "value": False}
    try:
        user_db.add_friendship(user.user_uuid, request.user_uuid)
        user_db.delete_friend_request(user.user_uuid, request.user_uuid)
        print(f"INFO:Friend request accepted successfully: {user.user_uuid} -> {request.user_uuid}")
        return {"message": "Friend request accepted successfully", "value": True}
    except Exception as e:
        print(f"ERROR:Failed to accept friend request: {str(e)}")
        return {"message": f"Failed to accept friend request: {str(e)}", "value": False}

@app.post("/cancel_friend_request")
async def cancel_friend_request(request: FriendRequest):
    """ Cancel a friend request """
    print("INFO:cancel_friend_request")
    # check token validity
    user = token_db.get_user_by_identification_token(request.user_token)
    if not user:
        print("ERROR:Invalid token")
        return {"message": "Invalid token", "value": False}
    try:
        user_db.delete_friend_request(user.user_uuid, request.user_uuid)
        print(f"INFO:Friend request canceled successfully: {user.user_uuid} -> {request.user_uuid}")
        return {"message": "Friend request canceled successfully", "value": True}
    except Exception as e:
        print(f"ERROR:Failed to cancel friend request: {str(e)}")
        return {"message": f"Failed to cancel friend request: {str(e)}", "value": False}

@app.get("/get_friend_requests")
async def get_friend_requests(user_token: str = Query(None)):
    """ Get all friend requests """
    print("INFO:get_friend_requests")
    # check token validity
    user = token_db.get_user_by_identification_token(user_token)
    if not user:
        print("ERROR:Invalid token")
        return {"message": "Invalid token", "value": False}
    try:
        friend_requests = user_db.get_friend_requests(user.user_uuid)
        print(f"INFO:friend_requests: {friend_requests}")
        return {"message": "Friend requests retrieved successfully", "value": True, "friend_requests": friend_requests}
    except Exception as e:
        print(f"ERROR:Failed to retrieve friend requests: {str(e)}")
        return {"message": f"Failed to retrieve friend requests: {str(e)}", "value": False}


@app.post("/decline_friend_request")
async def decline_friend_request(request: FriendRequest):
    """ Decline a friend request """
    print("INFO:decline_friend_request")
    # check token validity
    user = token_db.get_user_by_identification_token(request.user_token)
    if not user:
        print("ERROR:Invalid token")
        return {"message": "Invalid token", "value": False}
    try:
        user_db.delete_friend_request(user.user_uuid, request.user_uuid)
        print(f"INFO:Friend request declined successfully: {user.user_uuid} -> {request.user_uuid}")
        return {"message": "Friend request declined successfully", "value": True}
    except Exception as e:
        print(f"ERROR:Failed to decline friend request: {str(e)}")
        return {"message": f"Failed to decline friend request: {str(e)}", "value": False}

@app.post("/remove_friend")
async def remove_friend(request: FriendRequest):
    """ Remove a friend """
    print("INFO:remove_friend")
    # check token validity
    user = token_db.get_user_by_identification_token(request.user_token)
    if not user:
        print("ERROR:Invalid token")
        return {"message": "Invalid token", "value": False}
    try:
        user_db.delete_friendship(user.user_uuid, request.user_uuid)
        print(f"INFO:Friend removed successfully: {user.user_uuid} -> {request.user_uuid}")
        return {"message": "Friend removed successfully", "value": True}
    except Exception as e:
        print(f"ERROR:Failed to remove friend: {str(e)}")
        return {"message": f"Failed to remove friend: {str(e)}", "value": False}


@app.post("/create-game")
async def create_game(request: UserToken):
    """ Create a game """
    print("INFO:create_game")
    # check token validity
    user = token_db.get_user_by_identification_token(request.user_token)
    if not user:
        print("ERROR:Invalid token")
        return {"message": "Invalid token", "value": False}
    try:
        # check if user has already a game
        if docker_db.user_has_running_server(user.user_uuid):
            print("ERROR:User already has a game")
            return {"message": "User already has a game", "value": False}
        # Create a Socket.IO Container
        url, identifier = docker_manager.create_socketio_container("socketio-server", user.user_uuid)
        print(f"INFO:Game created successfully: {user.user_uuid}")
        return {"message": "Game created successfully", "value": True, "url": url, "identifier": identifier}
    except Exception as e:
        print(f"ERROR:Failed to create game: {str(e)}")
        return {"message": "Failed to create game", "value": False}

@app.post("/delete-game")
async def delete_game(request: RemoveGame):
    """ Delete a game """
    # Check token validity
    print("INFO:delete_game")
    user = token_db.get_user_by_identification_token(request.user_token)
    if not user:
        print("ERROR:Invalid token")
        raise HTTPException(status_code=401, detail="Invalid token")

    # Fetch game information
    game_info = docker_db.get_container_by_identifier(request.identifier)
    if not game_info:
        print("ERROR:Game not found")
        raise HTTPException(status_code=404, detail="Game not found")
    # check if user is the owner of the game
    if game_info.user_uuid != user.user_uuid:
        print("ERROR:You are not the owner of this game")
        raise HTTPException(status_code=401, detail="You are not the owner of this game")

    # Stop and remove the container
    try:
        docker_manager.remove_container(game_info.container_id)
        print(f"INFO:Game deleted successfully: {game_info.container_id}")
    except Exception as e:
        print(f"ERROR:Failed to stop the game: {str(e)}")
        raise HTTPException(status_code=500, detail="Failed to stop the game")
    return {"message": "Game deleted successfully", "value": True}

@app.get("/get-game")
async def get_game(user_token: str, identifier: str):
    """ Get a game """
    print("INFO:get_game")
    # Check token validity
    user = token_db.get_user_by_identification_token(user_token)
    if not user:
        print("ERROR:Invalid token")
        raise HTTPException(status_code=401, detail="Invalid token")
    # Fetch game information
    game_info = docker_db.get_container_by_identifier(identifier)
    if not game_info:
        print("ERROR:Game not found")
        raise HTTPException(status_code=404, detail="Game not found")
    print(f"INFO:Game retrieved successfully: {game_info.container_id}, {game_info.url}, {game_info.identifier}")
    return {"message": "Game retrieved successfully", "value": True, "url": game_info.url, "identifier": game_info.identifier}


@app.post("/invite-friend")
async def invite_friend(request: InviteGame):
    """ Invite a friend to a game """
    print("INFO:invite_friend")
    # check token validity
    sender = token_db.get_user_by_identification_token(request.user_token)
    if not sender:
        print("ERROR:Invalid token")
        return {"message": "Invalid token", "value": False}
    try:
        # Check users friendship
        if not user_db.check_friendship(sender.user_uuid, request.friend_uuid):
            print("ERROR:Not friends")
            return {"message": "Not friends", "value": False}
        # send notification
        receiver = user_db.get_user_by_uuid(request.friend_uuid)
        if receiver:
            receiver_token = FCMToken_db.get_token_by_user_id(receiver.user_uuid)
            if receiver_token:
                firebase_admin.send_fcm_game_invite_notification(receiver_token.fcm_token, sender.user_name,
                                                                 request.identifier)
        print(f"INFO:Friend invited successfully: {sender.user_uuid} -> {request.friend_uuid}")
        return {"message": "Friend invited successfully", "value": True}
    except Exception as e:
        print(f"ERROR:Failed to invite friend: {str(e)}")
        return {"message": f"Failed to invite friend: {str(e)}", "value": False}

if __name__ == "__main__":
    uvicorn.run(app="main:app", host="localhost", port=8000, reload=True)