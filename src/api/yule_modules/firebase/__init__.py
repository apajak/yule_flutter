
import firebase_admin
from firebase_admin import credentials, messaging
from typing import Optional, Dict
class Firebase:
    def __init__(self, service_account_path: str):
        self.service_account_path = service_account_path
        self.initialize_firebase()

    def initialize_firebase(self):
        """Initialize the Firebase application if not already initialized."""
        if not firebase_admin._apps:
            cred = credentials.Certificate(self.service_account_path)
            firebase_admin.initialize_app(cred)
        else:
            print("Firebase app already initialized.")

    def send_fcm_notification(self, token: str, title: str, body: str, data: Dict[str, str] = None) -> Optional[str]:
        """Send a FCM notification with optional data payload."""
        message = messaging.Message(
            notification=messaging.Notification(
                title=title,
                body=body,
            ),
            data=data,  # Add the data payload
            token=token,
        )

        try:
            response = messaging.send(message)
            return response
        except Exception as e:
            print(f"Error sending FCM message: {e}")
            return None


    def send_fcm_friend_request_notification(self, token: str, sender_name: str) -> Optional[str]:
        """Send an FCM notification for a friend request."""
        title = "Friend request"
        body = f"{sender_name} sent you a friend request."
        data = {
            'route': '/friendScreen',  # Custom data for redirecting to specific screen
        }
        return self.send_fcm_notification(token, title, body, data)

    def send_fcm_game_invite_notification(self, token: str, sender_name: str, game_id: str) -> Optional[str]:
        """Send an FCM notification for a game invite."""
        title = "Game invite"
        body = f"{sender_name} invited you to a game."
        data = {
            'route': f'/joinGame?game_id={game_id}',  # Include game_id as a query parameter
        }
        return self.send_fcm_notification(token, title, body, data)
