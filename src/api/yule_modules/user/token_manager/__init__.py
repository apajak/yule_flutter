import secrets
import datetime


class TokenBase:
    def __init__(self, user_uuid: str, timestamp: datetime= None, token: str = None, token_id: int = None):
        self.token_id = token_id
        self.user_uuid = user_uuid
        if token is None:
            self.set_token()
        else:
            self.token = token
        if timestamp is None:
            self.timestamp = datetime.datetime.now()
        else:
            self.timestamp = timestamp


    def __str__(self) -> str:
        """Returns a string representation of the token"""
        return (f"{self.__class__.__name__}(token_id={self.token_id}, user_uuid={self.user_uuid}, "
                f"token={self.token}, timestamp={self.timestamp})")

    def get_token(self):
        """Getter method to retrieve the token"""
        return self.token

    def set_token(self):
        """Setter method to set a new token"""
        self.token = secrets.token_urlsafe(32)
        self.timestamp = datetime.datetime.now()
        return self.token

    def to_tuple(self) -> tuple:
        """Converts the token to a tuple"""
        return self.token_id, self.user_uuid, self.token, self.timestamp

    def to_dict(self) -> dict:
        """Converts the token to a dictionary"""
        token_dict = {
            "token_id": self.token_id,
            "user_uuid": self.user_uuid,
            "token": self.token,
            "timestamp": self.timestamp
        }
        return token_dict

    @staticmethod
    def from_tuple(token_tuple: tuple):
        if token_tuple is None:
            return None
        token_object = TokenBase(token_id=token_tuple[0], user_uuid=token_tuple[1], token=token_tuple[2],
                                 timestamp=token_tuple[3])
        return token_object

    @staticmethod
    def from_dict(token_dict: dict):
        return TokenBase(
            token_id=token_dict["token_id"],
            user_uuid=token_dict["user_uuid"],
            token=token_dict["token"],
            timestamp=token_dict["timestamp"]
        )


class IdentificationToken(TokenBase):
    pass


class MailToken(TokenBase):
    pass


class FCMToken:
    def __init__(self, user_uuid: str, fcm_token: str = None, last_updated: datetime = None):
        self.user_uuid = user_uuid
        self.fcm_token = fcm_token
        self.last_updated = last_updated

    def __str__(self) -> str:
        return (f"FCMToken(user_uuid={self.user_uuid}, "
                f"fcm_token={self.fcm_token}, last_updated={self.last_updated})")

    def to_dict(self) -> dict:
        """Converts the FCM token to a dictionary"""
        return {
            "user_uuid": self.user_uuid,
            "fcm_token": self.fcm_token,
            "last_updated": self.last_updated
        }

    @staticmethod
    def from_dict(token_dict: dict):
        """Creates an FCMToken object from a dictionary"""
        return FCMToken(
            user_uuid=token_dict["user_uuid"],
            fcm_token=token_dict["fcm_token"],
            last_updated=token_dict["last_updated"]
        )
