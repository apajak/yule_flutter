from datetime import date
import uuid
from pydantic import BaseModel
from yule_modules.user.token_manager import IdentificationToken


class User:
    """Represents a user in the database and contains all the information about a user and methods to manipulate it"""

    def __init__(self, user_name: str, first_name: str, last_name: str, email: str, birth_date:date, gender: str,
                 sexual_orientation: str, couple_state: str, alcohol: str, total_sips: int, is_connect: int,
                 user_id: int = None, user_uuid: str = uuid.uuid4(), profile_picture: str = "default_profile_picture.png"):
        """Creates a new user object"""
        self.user_id = user_id
        self.user_uuid = user_uuid
        self.user_name = user_name
        self.first_name = first_name
        self.last_name = last_name
        self.password = None
        self.email = email
        self.birth_date = birth_date
        self.gender = gender
        self.sexual_orientation = sexual_orientation
        self.couple_state = couple_state
        self.alcohol = alcohol
        self.total_sips = total_sips
        self.is_connect = is_connect
        self.identification_token = None
        self.mail_token = None
        self.profile_picture = profile_picture

    def __str__(self) -> str:
        """Returns a string representation of the user"""
        return f"User(uuid={self.user_uuid}, user_name={self.user_name}, first_name={self.first_name}, " \
               f"last_name={self.last_name}, email={self.email}, birth_date={self.birth_date}, " \
               f"gender={self.gender}, sexual_orientation={self.sexual_orientation}, " \
               f"couple_state={self.couple_state}, alcohol={self.alcohol}, " \
               f"total_sips={self.total_sips}, is_connect={self.is_connect}, profile_picture={self.profile_picture})"

    # password
    def set_password(self, new_password):
        """Setter method to set the user's password"""
        self.password = new_password

    def set_identification_token(self):
        """Setter method to set the user's identification token"""
        self.identification_token = IdentificationToken(self.user_uuid)
        return self.identification_token, self.user_uuid

    def get_identification_token(self):
        """Getter method to retrieve the user's identification token"""
        return self.identification_token.token

    def get_password(self):
        """Getter method to retrieve the user's password"""
        return self.password

    def get_user_id(self):
        """Getter method to retrieve the user's id"""
        return self.user_id

    def to_tuple(self) -> tuple:
        """Converts the user to a tuple"""
        return (self.user_id, str(self.user_uuid), self.user_name, self.first_name, self.last_name, self.email,
                self.birth_date, self.gender, self.sexual_orientation, self.couple_state, self.alcohol, self.total_sips,
                self.is_connect)

    def to_dict(self) -> dict:
        """Converts the user to a dictionary"""
        user_dict = {
            "user_id": self.user_id,
            "user_uuid": self.user_uuid,
            "user_name": self.user_name,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "email": self.email,
            "birth_date": self.birth_date.strftime("%Y-%m-%d") if self.birth_date else None,
            "gender": self.gender,
            "sexual_orientation": self.sexual_orientation,
            "couple_state": self.couple_state,
            "alcohol": self.alcohol,
            "total_sips": self.total_sips,
            "is_connect": self.is_connect,
            "profile_picture": self.profile_picture if self.profile_picture else "default_profile_picture.png"
        }
        return user_dict


    @staticmethod
    def from_tuple(user_tuple: tuple):
        user_object = User(user_id=user_tuple[0], user_uuid=user_tuple[1], user_name=user_tuple[2],
                           first_name=user_tuple[3], last_name=user_tuple[4], email=user_tuple[6],
                           birth_date=user_tuple[7], gender=user_tuple[8], sexual_orientation=user_tuple[9],
                           couple_state=user_tuple[10], alcohol=user_tuple[11], total_sips=user_tuple[12],
                           is_connect=user_tuple[13], profile_picture=user_tuple[14])
        return user_object
    @staticmethod
    def from_dict(user_dict: dict):
        birth_date_str = user_dict["birth_date"]
        birth_date = date.fromisoformat(birth_date_str) if birth_date_str else None

        return User(
            user_id=user_dict["user_id"],
            user_uuid=user_dict["user_uuid"],
            user_name=user_dict["user_name"],
            first_name=user_dict["first_name"],
            last_name=user_dict["last_name"],
            email=user_dict["email"],
            birth_date=birth_date,
            gender=user_dict["gender"],
            sexual_orientation=user_dict["sexual_orientation"],
            couple_state=user_dict["couple_state"],
            alcohol=user_dict["alcohol"],
            total_sips=user_dict["total_sips"],
            is_connect=user_dict["is_connect"],
            profile_picture=user_dict["profile_picture"]
        )


class UserCreate(BaseModel):
    user_name: str
    first_name: str
    last_name: str
    email: str
    birth_date: date
    gender: str
    sexual_orientation: str
    couple_state: str
    alcohol: str
    password: str


class UserCredentials(BaseModel):
    identifier: str
    password: str


class FriendRequest(BaseModel):
    user_token: str
    user_uuid: str


class FcmTokenModel(BaseModel):
    user_token: str
    fcm_token: str


class UserToken(BaseModel):
    user_token: str


class GetGame(BaseModel):
    user_token: str
    identifier: str


class InviteGame(BaseModel):
    user_token: str
    identifier: str
    friend_uuid: str


class RemoveGame(BaseModel):
    user_token: str
    identifier: str


class UserUpdate(BaseModel):
    user_name: str
    email: str
    sexual_orientation: str
    couple_state: str
    alcohol: str
    identification_token: str


class UserUpdatePassword(BaseModel):
    password: str
    new_password: str
    identification_token: str


def set_game_alcohol(alcohol: str):
    if alcohol == "never":
        return 0
    elif alcohol == "occasionally":
        return 2
    elif alcohol == "regularly":
        return 4
    else:
        return 3


class SimpleUser(BaseModel):
    user_uuid: str
    user_name: str
    profile_picture: str
    game_alcohol: int
    gender: str = "ND"

    @staticmethod
    def from_tuple(user_tuple: tuple):
        game_alcohol = set_game_alcohol(user_tuple[3])
        return SimpleUser(user_uuid=user_tuple[0], user_name=user_tuple[1], profile_picture=user_tuple[2],
                          game_alcohol=game_alcohol, gender=user_tuple[4])


    @staticmethod
    def from_dict(user_dict: dict):
        return SimpleUser(user_uuid=user_dict["user_uuid"], user_name=user_dict["user_name"],
                          profile_picture=user_dict["profile_picture"], game_alcohol=user_dict["game_alcohol"],
                          gender=user_dict["gender"])

    def to_dict(self) -> dict:
        """Converts the user to a dictionary"""
        user_dict = {
            "user_uuid": self.user_uuid,
            "user_name": self.user_name,
            "profile_picture": self.profile_picture if self.profile_picture else "default_profile_picture.png",
            "game_alcohol": self.game_alcohol,
            "gender": self.gender
        }
        return user_dict

    def to_tuple(self) -> tuple:
        """Converts the user to a tuple"""
        return self.user_uuid, self.user_name, self.profile_picture, self.game_alcohol, self.gender
