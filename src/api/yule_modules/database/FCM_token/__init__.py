from typing import Optional

import mysql.connector
from yule_modules.database.manager import Database
from yule_modules.user.token_manager import FCMToken

class FCMTokenDatabaseError(Exception):
    """Custom exception for FCM token database errors"""
    def __init__(self, message="FCM token database operation failed"):
        self.message = message
        super().__init__(self.message)


class FCMTokenDatabase(Database):
    def __init__(self, host, user, password, database):
        super().__init__(host, user, password, database)

    def insert_token(self, fcm_token: FCMToken) -> int:
        """Insert a new FCM token"""
        self.connect()
        query = """
                INSERT INTO fcm_tokens (user_uuid, fcm_token, last_updated)
                VALUES (%s, %s, %s)
                """
        try:
            self.execute(query, (fcm_token.user_uuid, fcm_token.fcm_token, fcm_token.last_updated))
            token_id = self.get_last_row_id()
            self.close()
            return token_id
        except mysql.connector.Error as err:
            print(f"Error inserting FCM token: {err}")
            self.close()
            raise FCMTokenDatabaseError(f"Failed to insert FCM token: {err}")

    def update_token(self, fcm_token: FCMToken) -> bool:
        """Update an existing FCM token"""
        self.connect()
        query = """
                UPDATE fcm_tokens
                SET fcm_token = %s, last_updated = CURRENT_TIMESTAMP
                WHERE user_uuid = %s
                """
        try:
            self.execute(query, (fcm_token.fcm_token, fcm_token.user_uuid))
            self.close()
            return True
        except mysql.connector.Error as err:
            print(f"Error updating FCM token: {err}")
            self.close()
            raise FCMTokenDatabaseError(f"Failed to update FCM token: {err}")

    def get_token_by_user_id(self, user_uuid: str) -> Optional[FCMToken]:
        """Retrieve an FCM token for a specific user"""
        self.connect()
        query = "SELECT user_uuid, fcm_token, last_updated FROM fcm_tokens WHERE user_uuid = %s"
        try:
            self.execute(query, (user_uuid,))
            row = self.fetchone()
            self.close()
            if row:
                return FCMToken(
                    user_uuid=row[0],
                    fcm_token=row[1],
                    last_updated=row[2]
                )
            else:
                return None
        except mysql.connector.Error as err:
            print(f"Error retrieving FCM token: {err}")
            self.close()
            return None

    def delete_token(self, user_uuid: str) -> bool:
        """Delete an FCM token for a specific user"""
        self.connect()
        query = "DELETE FROM fcm_tokens WHERE user_uuid = %s"
        try:
            self.execute(query, (user_uuid,))
            self.close()
            return True
        except mysql.connector.Error as err:
            print(f"Error deleting FCM token: {err}")
            self.close()
            raise FCMTokenDatabaseError(f"Failed to delete FCM token: {err}")


    def list_all_tokens(self) -> list[FCMToken]:
        """List all FCM tokens in the database"""
        self.connect()
        query = "SELECT * FROM fcm_tokens"
        try:
            result = self.execute(query)
            rows = result.fetchall()
            self.close()
            return [FCMToken.from_dict({'user_uuid': row[1], 'fcm_token': row[2], 'last_updated': row[3]}) for row in rows]
        except mysql.connector.Error as err:
            print(f"Error listing FCM tokens: {err}")
            self.close()
            raise FCMTokenDatabaseError(f"Failed to list FCM tokens: {err}")
