from yule_modules.database.manager import Database
from yule_modules.docker import DockerData
import mysql.connector

class DatabaseInsertionError(Exception):
    """Custom exception for database insertion errors"""
    def __init__(self, message="Database insertion failed"):
        self.message = message
        super().__init__(self.message)

class DockerDatabase(Database):
    def __init__(self, host, user, password, database):
        super().__init__(host, user, password, database)

    def insert_container(self, container_id, port, url, user_uuid, status, identifier):
        query = """
        INSERT INTO DockerContainers (ContainerID, Port, URL, UserUUID, Status, Identifier)
        VALUES (%s, %s, %s, %s, %s, %s)
        """
        args = (container_id, port, url, user_uuid, status, identifier)
        try:
            self.connect()
            self.execute(query, args)
            self.close()
        except mysql.connector.Error as e:
            raise DatabaseInsertionError(f"Error inserting container record: {str(e)}")

    def user_has_running_server(self, user_uuid):
        """Check if the user already has a server running."""
        query = "SELECT COUNT(*) FROM DockerContainers WHERE UserUUID = %s AND Status = 'running'"
        try:
            self.connect()
            self.execute(query, (user_uuid,))
            result = self.fetchone()
            self.close()
            return result[0] > 0
        except mysql.connector.Error as e:
            print(f"Error checking if user has running server: {str(e)}")

    def update_container_status(self, container_id, new_status):
        query = """
        UPDATE DockerContainers SET Status = %s WHERE ContainerID = %s
        """
        args = (new_status, container_id)
        try:
            self.connect()
            self.execute(query, args)
            self.close()
        except mysql.connector.Error as e:
            print(f"Error updating container status: {str(e)}")

    def get_container(self, container_id):
        query = """
        SELECT * FROM DockerContainers WHERE ContainerID = %s
        """
        args = (container_id,)
        try:
            self.connect()
            self.execute(query, args)
            result = self.fetchone()
            self.close()
            return DockerData.fromTuple(result)
        except mysql.connector.Error as e:
            print(f"Error fetching container: {str(e)}")

    def get_container_status(self, container_id):
        query = """
        SELECT Status FROM DockerContainers WHERE ContainerID = %s
        """
        args = (container_id,)
        try:
            self.connect()
            self.execute(query, args)
            result = self.fetchone()
            self.close()
            return result[0]
        except mysql.connector.Error as e:
            print(f"Error fetching container status: {str(e)}")

    def get_container_by_identifier(self, identifier) -> DockerData:
        """Retrieve a container record using its identifier."""
        query = "SELECT * FROM DockerContainers WHERE Identifier = %s"
        try:
            self.connect()
            self.execute(query, (identifier,))
            result = self.fetchone()
            self.close()
            return DockerData.fromTuple(result)
        except mysql.connector.Error as e:
            print(f"Error fetching container by identifier: {str(e)}")

    def delete_container(self, container_id):
        query = """
        DELETE FROM DockerContainers WHERE ContainerID = %s
        """
        args = (container_id,)
        try:
            self.connect()
            self.execute(query, args)
            self.close()
            return True
        except mysql.connector.Error as e:
            print(f"Error deleting container: {str(e)}")
            return False
