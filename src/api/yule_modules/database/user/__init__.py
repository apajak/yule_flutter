from yule_modules.database.manager import Database
from yule_modules.user.user_manager import User, SimpleUser
import mysql.connector
import bcrypt


class DatabaseInsertionError(Exception):
    """Custom exception for database insertion errors"""
    def __init__(self, message="Database insertion failed"):
        self.message = message
        super().__init__(self.message)


class UserDatabase(Database):
    def __init__(self, host, user, password, database):
        super().__init__(host, user, password, database)

    def insert_user(self, user_object) -> User:
        """Inserts a new user into the database"""
        self.connect()
        query = """
            INSERT INTO users (uuid, userName, firstName, lastName, email, birthDate, 
                               gender, sexualOrientation, coupleState, alcohol, totalSips, isConnect, password)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s,%s)
        """
        user_password = user_object.get_password()
        user_values = user_object.to_tuple()[1:] + (user_password,)
        print(user_values)
        try:
            self.execute(query, user_values)
            user_id = self.get_last_row_id()
            self.close()
            return user_id
        except mysql.connector.Error as err:
            print(f"Error inserting user: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to insert user: {err}")

    def update_user_profile_picture(self, user_id, image_path) -> bool:
        """Updates a user's profile picture in the database"""
        self.connect()
        query = """
            UPDATE users
            SET profilePicture = %s
            WHERE id = %s
        """
        user_values = (image_path, user_id)

        try:
            self.execute(query, user_values)
            self.close()
            return True
        except mysql.connector.Error as err:
            print(f"Error updating user profile picture: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to update user profile picture: {err}")

    def update_user_profile(self, user_id, user_name, email, sexual_orientation,couple_state,alcohol) -> bool:
        """Updates a user's data in the database"""
        self.connect()
        query = """
            UPDATE users
            SET userName = %s, email = %s, sexualOrientation = %s, coupleState = %s, alcohol = %s
            WHERE id = %s
        """
        user_values = (user_name, email, sexual_orientation,couple_state,alcohol, user_id)

        try:
            self.execute(query, user_values)
            self.close()
            return True
        except mysql.connector.Error as err:
            print(f"Error updating user: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to update user: {err}")
    def update_user(self, user_id, new_data) -> User:
        """Updates a user's data in the database"""
        self.connect()
        query = """
            UPDATE users
            SET uuid = %s, userName = %s, firstName = %s, lastName = %s,
                email = %s, birthDate = %s, gender = %s, sexualOrientation = %s,
                coupleState = %s, alcohol = %s, totalSips = %s, isConnect = %s
            WHERE id = %s
        """
        user_values = new_data.to_tuple()[1:] + (user_id,)

        try:
            self.execute(query, user_values)
            self.close()
            return self.get_user_by_id(user_id)
        except mysql.connector.Error as err:
            print(f"Error updating user: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to update user: {err}")

    def update_user_password(self, user_id, new_password) -> User:
        """Updates a user's password in the database"""
        self.connect()
        query = """
            UPDATE users
            SET password = %s
            WHERE id = %s
        """
        user_values = (new_password, user_id)

        try:
            self.execute(query, user_values)
            self.close()
            return self.get_user_by_id(user_id)
        except mysql.connector.Error as err:
            print(f"Error updating user password: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to update user password: {err}")

    def get_user_password(self, user_id) -> str:
        """Gets a user's password from the database"""
        self.connect()
        query = """
            SELECT password FROM users
            WHERE id = %s
        """
        user_values = (user_id,)
        try:
            self.execute(query, user_values)
            result = self.fetchone()
            self.close()
            return result[0]
        except mysql.connector.Error as err:
            print(f"Error getting user password: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to get user password: {err}")

    def get_user_profile_picture(self, user_id) -> str:
        """Gets a user's profile picture from the database"""
        self.connect()
        query = """
            SELECT profilePicture FROM users
            WHERE id = %s
        """
        user_values = (user_id,)
        try:
            self.execute(query, user_values)
            result = self.fetchone()
            self.close()
            return result[0]
        except mysql.connector.Error as err:
            print(f"Error getting user profile picture: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to get user profile picture: {err}")
    def delete_user(self, user_id) -> bool:
        """Deletes a user from the database"""
        self.connect()
        query = """
            DELETE FROM users
            WHERE id = %s
        """
        user_values = (user_id,)
        try:
            self.execute(query, user_values)
            self.close()
            return True
        except mysql.connector.Error as err:
            print(f"Error deleting user: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to delete user: {err}")

    def get_user_by_id(self, user_id) -> User:
        """Gets a user by their id"""
        self.connect()
        query = """
            SELECT * FROM users
            WHERE id = %s
        """
        user_values = (user_id,)
        try:
            self.execute(query, user_values)
            result = self.fetchone()
            self.close()
            return User.from_tuple(result)

        except mysql.connector.Error as err:
            print(f"Error getting user by id: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to get user by id: {err}")

    def get_user_by_uuid(self, user_uuid) -> User:
        """Gets a user by their uuid"""
        self.connect()
        query = """
            SELECT * FROM users
            WHERE uuid = %s
        """
        user_values = (user_uuid,)
        try:
            self.execute(query, user_values)
            result = self.fetchone()
            self.close()
            return User.from_tuple(result)

        except mysql.connector.Error as err:
            print(f"Error getting user by uuid: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to get user by uuid: {err}")

    def get_user_by_username(self, username) -> User:
        """Gets a user by their username"""
        self.connect()
        query = """
            SELECT * FROM users
            WHERE userName = %s
        """
        user_values = (username,)
        try:
            self.execute(query, user_values)
            result = self.fetchone()
            self.close()
            return User.from_tuple(result)

        except mysql.connector.Error as err:
            print(f"Error getting user by username: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to get user by username: {err}")

    def get_user_by_email(self, email) -> User:
        """Gets a user by their email"""
        self.connect()
        query = """
            SELECT * FROM users
            WHERE email = %s
        """
        user_values = (email,)
        try:
            self.execute(query, user_values)
            result = self.fetchone()
            self.close()
            return User.from_tuple(result)

        except mysql.connector.Error as err:
            print(f"Error getting user by email: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to get user by email: {err}")

    def get_user_by_identifier(self, identifier) -> User:
        """Gets a user by their username or email"""
        self.connect()
        query = """
            SELECT * FROM users
            WHERE userName = %s OR email = %s
        """
        user_values = (identifier, identifier)
        try:
            self.execute(query, user_values)
            result = self.fetchone()
            self.close()
            print(result)
            return User.from_tuple(result)

        except mysql.connector.Error as err:
            print(f"Error getting user by identifier: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to get user by identifier: {err}")

    def user_name_exist(self, user_name) -> bool:
        """ check if user exists """
        self.connect()
        query = """
            SELECT * FROM users
            WHERE userName = %s
        """
        user_values = (user_name,)
        try:
            self.execute(query, user_values)
            result = self.fetchone()
            self.close()
            return result is not None
        except mysql.connector.Error as err:
            print(f"Error checking if user exists: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to check if user exists: {err}")

    def user_mail_exist(self, user_mail) -> bool:
        """ check if user exists """
        self.connect()
        query = """
            SELECT * FROM users
            WHERE email = %s
        """
        user_values = (user_mail,)
        try:
            self.execute(query, user_values)
            result = self.fetchone()
            self.close()
            return result is not None
        except mysql.connector.Error as err:
            print(f"Error checking if user exists: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to check if user exists: {err}")

    def user_exist(self, user_mail, user_name):
        """ check if user exists """
        self.connect()
        query = """
            SELECT * FROM users
            WHERE userName = %s OR email = %s
        """
        user_values = (user_name, user_mail)
        try:
            self.execute(query, user_values)
            result = self.fetchone()
            self.close()
            return result
        except mysql.connector.Error as err:
            print(f"Error checking if user exists: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to check if user exists: {err}")

    def check_password(self, input_identifier, input_password) -> bool:
        """Checks if the user's password is correct"""
        self.connect()
        query = """
            SELECT password FROM users
            WHERE userName = %s OR email = %s
        """
        user_values = (input_identifier, input_identifier)
        try:
            self.execute(query, user_values)
            result = self.fetchone()
            self.close()
            return bcrypt.checkpw(input_password.encode("utf-8"), result[0].encode("utf-8"))
        except mysql.connector.Error as err:
            print(f"Error logging in: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to login: {err}")

    def get_users_not_friends(self, user_uuid):
        """Get users who are not friends with the specified user or are the same as the user_uuid."""
        self.connect()
        query = """
            SELECT uuid, userName, profilePicture, alcohol, gender
            FROM users
            WHERE uuid != %s
            AND uuid NOT IN (
                SELECT user_id_2 FROM friends WHERE user_id_1 = %s
                UNION
                SELECT user_id_1 FROM friends WHERE user_id_2 = %s
            )
        """
        try:
            self.execute(query, (user_uuid, user_uuid, user_uuid))
            result = self.fetchall()
            self.close()
            return [SimpleUser.from_tuple(user) for user in result]
        except mysql.connector.Error as err:
            print(f"Error getting users not friends: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to get users not friends: {err}")

    def get_users_friends(self, user_uuid):
        """Get users who are friends with the specified user."""
        self.connect()
        query = """
            SELECT uuid, userName, profilePicture, alcohol, gender
            FROM users
            WHERE uuid IN (
                SELECT user_id_2 FROM friends WHERE user_id_1 = %s
                UNION
                SELECT user_id_1 FROM friends WHERE user_id_2 = %s
            )
        """
        try:
            self.execute(query, (user_uuid, user_uuid))
            result = self.fetchall()
            self.close()
            if result is None:
                return []
            else :
                return [SimpleUser.from_tuple(user) for user in result]
        except mysql.connector.Error as err:
            print(f"Error getting users friends: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to get users friends: {err}")

    def search_users(self, user_uuid, username_slice):
        """Get users where name contains the specified slice and not the user_uuid."""
        self.connect()
        query = """
        SELECT uuid, userName, profilePicture, alcohol, gender
        FROM users
        WHERE userName LIKE %s AND uuid != %s

        """
        try:
            self.execute(query, (username_slice+'%', user_uuid,))
            result = self.fetchall()
            print(result)
            self.close()
            if result is None:
                return []
            else:
                return [SimpleUser.from_tuple(user) for user in result]
        except mysql.connector.Error as err:
            print(f"Error getting users friends: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to get users friends: {err}")

    def add_friendship(self,user_uuid, new_friend_uuid):
        """Add a friendship between two users"""
        self.connect()
        query = """
            INSERT INTO friends (user_id_1, user_id_2)
            VALUES (%s, %s)
        """
        try:
            self.execute(query, (user_uuid, new_friend_uuid))
            self.close()
            return True
        except mysql.connector.Error as err:
            print(f"Error adding friendship: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to add friendship: {err}")

    def delete_friendship(self,user_uuid, friend_uuid):
        """Delete a friendship between two users"""
        self.connect()
        query = """
            DELETE FROM friends
            WHERE (user_id_1 = %s AND user_id_2 = %s)
            OR (user_id_1 = %s AND user_id_2 = %s)
        """
        try:
            self.execute(query, (user_uuid, friend_uuid, friend_uuid, user_uuid))
            self.close()
            return True
        except mysql.connector.Error as err:
            print(f"Error deleting friendship: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to delete friendship: {err}")

    def add_friend_request(self, user_uuid, new_friend_uuid):
        """Add a friend request between two users, if not already present."""
        self.connect()

        # Check if a friend request already exists or is pending
        check_query = """
            SELECT * FROM friend_requests
            WHERE (sender_id = %s AND receiver_id = %s)
               OR (sender_id = %s AND receiver_id = %s)
        """
        self.execute(check_query, (user_uuid, new_friend_uuid, new_friend_uuid, user_uuid))
        result = self.cursor.fetchone()

        if result:
            self.close()
            print("Friend request already exists or is pending.")
            return False  # or raise an appropriate exception

        # Insert new friend request
        insert_query = """
            INSERT INTO friend_requests (sender_id, receiver_id)
            VALUES (%s, %s)
        """
        try:
            self.execute(insert_query, (user_uuid, new_friend_uuid))
            self.close()
            return True
        except mysql.connector.Error as err:
            print(f"Error adding friend request: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to add friend request: {err}")

    def delete_friend_request(self, user_uuid, friend_uuid):
        """Delete a friend request between two users"""
        self.connect()
        query = """
            DELETE FROM friend_requests
            WHERE (sender_id = %s AND receiver_id = %s)
            OR (sender_id = %s AND receiver_id = %s)
        """
        try:
            self.execute(query, (user_uuid, friend_uuid, friend_uuid, user_uuid))
            self.close()
            return True
        except mysql.connector.Error as err:
            print(f"Error deleting friend request: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to delete friend request: {err}")

    def get_friend_requests(self, user_uuid):
        """Get a user's friend requests"""
        self.connect()
        query = """
            SELECT uuid, userName, profilePicture, alcohol, gender
            FROM users
            WHERE uuid IN (
                SELECT sender_id FROM friend_requests WHERE receiver_id = %s
            )
        """
        try:
            self.execute(query, (user_uuid,))
            result = self.fetchall()
            self.close()
            if result is None:
                return []
            else:
                return [SimpleUser.from_tuple(user) for user in result]
        except mysql.connector.Error as err:
            print(f"Error getting friend requests: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to get friend requests: {err}")

    def check_if_pending_friend_request(self, user_uuid, friend_uuid):
        """Check if a friend request exists between two users"""
        self.connect()
        query = """
            SELECT * FROM friend_requests
            WHERE (sender_id = %s AND receiver_id = %s)
            OR (sender_id = %s AND receiver_id = %s)
        """
        try:
            self.execute(query, (user_uuid, friend_uuid, friend_uuid, user_uuid))
            result = self.fetchone()
            self.close()
            return result is not None
        except mysql.connector.Error as err:
            print(f"Error checking if friend request exists: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to check if friend request exists: {err}")

    def check_friendship(self, user_uuid, friend_uuid):
        """Check if two users are friends"""
        self.connect()
        query = """
            SELECT * FROM friends
            WHERE (user_id_1 = %s AND user_id_2 = %s)
            OR (user_id_1 = %s AND user_id_2 = %s)
        """
        try:
            self.execute(query, (user_uuid, friend_uuid, friend_uuid, user_uuid))
            result = self.fetchone()
            self.close()
            return result is not None
        except mysql.connector.Error as err:
            print(f"Error checking if friendship exists: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to check if friendship exists: {err}")