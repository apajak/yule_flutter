from datetime import datetime

from yule_modules.user.token_manager import IdentificationToken, MailToken
from yule_modules.database.manager import Database
from yule_modules.user.user_manager import User
from yule_modules.user.token_manager import IdentificationToken, MailToken
import mysql.connector

class DatabaseInsertionError(Exception):
    """Custom exception for database insertion errors"""
    def __init__(self, message="Database insertion failed"):
        self.message = message
        super().__init__(self.message)


class TokenDatabase(Database):
    def __init__(self, host, user, password, database):
        super().__init__(host, user, password, database)

    def _insert_token(self, token_object, token_type) -> int:
        """ Insert identification_token """
        self.connect()
        query = f"""
                    INSERT INTO {token_type} (user_uuid, token)
                    VALUES (%s,%s)
        """
        try :
            self.execute(query, (token_object.user_uuid,token_object.token))
            identification_id = self.get_last_row_id()
            return identification_id
        except mysql.connector.Error as err:
            print(f"Error inserting user: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to insert user: {err}")

    def _update_token(self, token_object, token_type) -> bool:
        """ Update identification_token """
        self.connect()

        query = f"""
                    UPDATE {token_type}
                    SET token = %s, timestamp = CURRENT_TIMESTAMP
                    WHERE user_uuid = %s
        """
        try :
            self.execute(query, (token_object.token,token_object.user_uuid))
            self.close()
            return True
        except mysql.connector.Error as err:
            print(f"Error updating user: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to update user: {err}")

    def _get_token(self, user_uuid, token_type) -> tuple:
        """ Get identification_token """
        self.connect()
        query = f"""
                    SELECT *
                    FROM {token_type}
                    WHERE user_uuid = %s
        """
        try :
            self.execute(query, (user_uuid,))
            token = self.fetchone()
            self.close()
            return token
        except mysql.connector.Error as err:
            print(f"Error updating user: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to update user: {err}")

    def _delete_token(self, user_uuid, token_type) -> bool:
        """ Delete identification_token """
        self.connect()
        query = f"""
                    DELETE FROM {token_type}
                    WHERE user_uuid = %s
        """
        try :
            self.execute(query, (user_uuid,))
            self.close()
            return True
        except mysql.connector.Error as err:
            print(f"Error updating user: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to update user: {err}")

    def _get_user_by_token(self, token, token_type) -> tuple:
        """ Get user by identification_token """
        self.connect()
        query = f"""
                    SELECT *
                    FROM users
                    WHERE uuid = (SELECT user_uuid FROM {token_type} WHERE token = %s)
        """
        try :
            self.execute(query, (token,))
            user = self.fetchone()
            self.close()
            return user
        except mysql.connector.Error as err:
            print(f"Error updating user: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to update user: {err}")

    def _check_token(self, token, token_type) -> tuple:
        """ Check if identification_token exists """
        self.connect()
        query = f"""
                    SELECT *
                    FROM {token_type}
                    WHERE token = %s
        """
        try :
            self.execute(query, (token,))
            token = self.fetchone()
            self.close()
            return token
        except mysql.connector.Error as err:
            print(f"Error updating user: {err}")
            self.close()
            raise DatabaseInsertionError(f"Failed to update user: {err}")

    def insert_identification_token(self, token_object) -> int:
        """ Insert identification_token """
        # check if user already has a token
        if self.get_identification_token(token_object.user_uuid):
            return self.update_identification_token(token_object)
        else:
            return self._insert_token(token_object, "identification_tokens")
    def insert_mail_token(self, token_object) -> int:
        """ Insert mail_token """
        return self._insert_token(token_object, "mail_tokens")

    def update_identification_token(self, token_object) -> bool:
        """ Update identification_token """
        return self._update_token(token_object, "identification_tokens")

    def reset_identification_token(self, token_object) -> bool:
        """ Reset identification_token """
        # get user
        user = self.get_user_by_identification_token(token_object.token)
        # if user is already connected, reset token
        if user.is_connect == 1:
            token_object.set_token()
            self.update_identification_token(token_object)
            return True
        return False

    def update_mail_token(self, token_object) -> bool:
        """ Update mail_token """
        return self._update_token(token_object, "mail_tokens")

    def get_identification_token(self, user_uuid) -> IdentificationToken:
        """ Get identification_token """
        return IdentificationToken.from_tuple(self._get_token(user_uuid, "identification_tokens"))

    def get_mail_token(self, user_uuid) -> MailToken:
        """ Get mail_token """
        return MailToken.from_tuple(self._get_token(user_uuid, "mail_tokens"))

    def delete_identification_token(self, user_uuid) -> bool:
        """ Delete identification_token """
        return self._delete_token(user_uuid, "identification_tokens")

    def delete_mail_token(self, user_uuid) -> bool:
        """ Delete mail_token """
        return self._delete_token(user_uuid, "mail_tokens")

    def get_user_by_identification_token(self, token) -> User:
        """ Get user by identification_token """
        return User.from_tuple(self._get_user_by_token(token, "identification_tokens"))

    def get_user_by_mail_token(self, token) -> User:
        """ Get user by mail_token """
        return User.from_tuple(self._get_user_by_token(token, "mail_tokens"))

    def check_identification_token(self, token) -> bool:
        """ Check if identification_token exists """
        token = IdentificationToken.from_tuple(self._check_token(token, "identification_tokens"))
        if token:
            date_delta = datetime.now() - token.timestamp
            # if token is older than 1 hour delete it
            if date_delta.seconds > 3600:
                if self.reset_identification_token(token):
                    return True
                else:
                    return False
            else:
                return True
        return False

    def check_mail_token(self, token):
        """ Check if mail_token exists """
        token = MailToken.from_tuple(self._check_token(token, "mail_tokens"))
        delta_date = datetime.now() - token.timestamp
        # if token is older than 15 minutes delete it
        if delta_date.seconds > 900:
            self.delete_mail_token(token.user_uuid)
            return False
        else:
            return True