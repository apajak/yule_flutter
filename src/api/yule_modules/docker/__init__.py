import string
from datetime import datetime
import docker
import random

class DockerManager:
    def __init__(self,database):
        self.client = docker.from_env()
        self.database = database


    def generate_unique_identifier(self, length=8):
        """Generate a unique identifier for a game."""
        letters = string.ascii_letters + string.digits
        return ''.join(random.choice(letters) for i in range(length))
    def find_available_port(self, start=3000, end=3999):
        """Find an available port within a range."""
        used_ports = self._get_used_ports()
        available_port = None
        while not available_port:
            port = random.randint(start, end)
            if port not in used_ports:
                available_port = port
        return available_port

    def _get_used_ports(self):
        """Get a list of ports currently used by running containers."""
        used_ports = []
        containers = self.client.containers.list()
        for container in containers:
            for port in container.ports.values():
                if port and port[0]['HostPort']:
                    used_ports.append(int(port[0]['HostPort']))
        return used_ports

    def create_socketio_container(self, image_name, user_uuid, external_port=None):
        """Create and start a Socket.IO server container."""
        if not external_port:
            external_port = self.find_available_port()
        container = self.client.containers.run(
            image=image_name,
            ports={'3000/tcp': external_port},
            detach=True
        )
        identifier = self.generate_unique_identifier()
        url = f"ws://10.0.2.2:{external_port}"
        self.database.insert_container(container.id, external_port, url,user_uuid, "running", identifier)
        print(f"Container {container.id} created successfully. URL: {url}")
        return url, identifier

    def remove_container(self, container_id):
        """Stop and remove a container by its ID."""
        try:
            container = self.client.containers.get(container_id)
            # set status to stopped
            self.database.update_container_status(container_id, "stopped")
            container.stop()
            container.remove()
            self.database.delete_container(container_id)
            print(f"Container {container_id} removed successfully.")
            return True
        except docker.errors.NotFound:
            print(f"Container {container_id} not found.")
            return False
        except Exception as e:
            print(f"Error removing container {container_id}: {e}")
            return False

class DockerData:
    def __init__(self, container_id, port, url, user_uuid, status, identifier, created_at=None, updated_at=None):
        self.container_id = container_id
        self.port = port
        self.url = url
        self.user_uuid = user_uuid
        self.status = status
        self.identifier = identifier
        self.created_at = created_at if created_at else datetime.now()
        self.updated_at = updated_at if updated_at else datetime.now()

    def __repr__(self):
        return (f"DockerData(container_id={self.container_id}, port={self.port}, url='{self.url}', "
                f"user_uuid='{self.user_uuid}', status='{self.status}', identifier='{self.identifier}', "
                f"created_at={self.created_at}, updated_at={self.updated_at})")

    def fromTuple(tuple):
        return DockerData(tuple[0],tuple[1],tuple[2],tuple[3],tuple[4],tuple[5],tuple[6],tuple[7])

    def toTuple(self):
        return (self.container_id,self.port,self.url,self.user_uuid,self.status,self.identifier,self.created_at,self.updated_at)

