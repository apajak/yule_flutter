import re
from yule_modules.user.user_manager import UserCredentials
class LoginFormVerifier:
    def __init__(self,form_data: UserCredentials):
        self.form_data = form_data

    def check_indentifier(self) -> (bool, list):
        """Checks if the identifier is valid"""
        failed_tests = []
        if self.form_data.identifier is None or not 4 <= len(self.form_data.identifier) <= 25:
            failed_tests.append("Username length requirement")

        if not re.match(r"^[a-zA-Z0-9_-]+$", self.form_data.identifier):
            failed_tests.append("Username Allowed characters")

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests

    def check_password(self) -> (bool, list):
        """ check if password is valid """
        failed_tests = []
        if self.form_data.password is None or not 8 <= len(self.form_data.password) <= 30:
            failed_tests.append("Password Length requirement")

        # check if password contains at least one uppercase
        if not re.search(r"[A-Z]", self.form_data.password):
            failed_tests.append("Password uppercase requirement")

        # check if password contains at least one lowercase
        if not re.search(r"[a-z]", self.form_data.password):
            failed_tests.append("Password lowercase requirement")

        # check if password contains at least one digit
        if not re.search(r"\d", self.form_data.password):
            failed_tests.append("Password digit requirement")

        # check if password contains at least one special character
        if not re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~"+r'"]', self.form_data.password):
            failed_tests.append("Password special character requirement")

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests


    def check_form(self) -> (bool, list):
        """ Check all form fields """
        failed_tests = []
        # check username
        is_valid, failed_tests = self.check_indentifier()
        if not is_valid:
            failed_tests.append("Username requirement")

        #### DISABLED FOR TESTING PURPOSES ####

        # # check password
        # is_valid, failed_tests = self.check_password()
        # if not is_valid:
        #     failed_tests.append("Password requirement")

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests