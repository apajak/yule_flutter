from yule_modules.user.user_manager import UserUpdatePassword
import re

class UpdatePasswordVerifier:
    def __init__(self, form_data: UserUpdatePassword):
        self.form_data = form_data

    def check_password(self) -> (bool, list):
        """ check if password is valid """
        failed_tests = []
        if self.form_data.new_password is None or not 8 <= len(self.form_data.new_password) <= 30:
            failed_tests.append("Password Length requirement")

        # check if password contains at least one uppercase
        if not re.search(r"[A-Z]", self.form_data.new_password):
            failed_tests.append("Password uppercase requirement")

        # check if password contains at least one lowercase
        if not re.search(r"[a-z]", self.form_data.new_password):
            failed_tests.append("Password lowercase requirement")

        # check if password contains at least one digit
        if not re.search(r"\d", self.form_data.new_password):
            failed_tests.append("Password digit requirement")

        # check if password contains at least one special character
        if not re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~" + r'"]', self.form_data.new_password):
            failed_tests.append("Password special character requirement")

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests
