from yule_modules.user.user_manager import UserCreate
import re
import json
from yule_modules.database.user import UserDatabase
import os
from dotenv import load_dotenv
from datetime import date

load_dotenv(".env")
db_config = {
    "host": os.getenv("DATABASE_HOST"),
    "user": os.getenv("DATABASE_USER"),
    "password": os.getenv("DATABASE_PASSWORD"),
    "database": os.getenv("DATABASE_NAME"),
}
user_db = UserDatabase(**db_config)
gender_list = ["female", "male", "ND"]
sexual_orientation_list = ["heterosexual", "homosexual", "ND"]
couple_state_list = ["single", "couple", "open_relationship" "ND"]
alcohol_list = ["never", "occasionally", "regularly", "ND"]
reserved_user_name_list = json.load(open("./yule_modules/security/reserved_user_name.json", "r"))
profanity_en = json.load(open("./yule_modules/security/profanity_list_en.json", "r"))
profanity_fr = json.load(open("./yule_modules/security/profanity_list_fr.json", "r"))
profanity_es = json.load(open("./yule_modules/security/profanity_list_es.json", "r"))
system_banned_words = json.load(open("./yule_modules/security/system_banned_list.json", "r"))
forbidden_words = profanity_es + profanity_fr + profanity_en + system_banned_words

unlock_reserved = "-elds-hoss-thrust"


class RegisterFormVerifier:
    def __init__(self, form_data: UserCreate):
        self.form_data = form_data

    @staticmethod
    def is_reserved_username(user_name: str) -> bool:
        """ Check if user_name is reserved """
        if user_name in reserved_user_name_list:
            return True
        return False

    @staticmethod
    def contain_unlock_reserved(user_name: str) -> bool:
        """ Check if user_name contain unlock reserved """
        if unlock_reserved in user_name:
            return True
        return False

    @staticmethod
    def remove_unlock_reserved(user_name: str) -> str:
        """ Remove unlock reserved """
        return user_name.replace(unlock_reserved, "")

    @staticmethod
    def is_username_in_use(user_name: str) -> bool:
        """ Check if user_name is in use """
        return user_db.user_name_exist(user_name)

    @staticmethod
    def contains_forbidden_words(user_name: str) -> bool:
        """ Check if user_name contains forbidden words """
        for word in forbidden_words:
            if word.lower() in user_name.lower() and len(word) > 3:
                print(word)
                return True
        return False

    def check_user_name(self) -> (bool, list):
        """ Check if user_name is valid """
        failed_tests = []

        if self.form_data.user_name is None or not 4 <= len(self.form_data.user_name) <= 25:
            failed_tests.append("Username length requirement")

        if not re.match(r"^[a-zA-Z0-9_-]+$", self.form_data.user_name):
            failed_tests.append("Username Allowed characters")

        if self.is_reserved_username(self.form_data.user_name):
            failed_tests.append("Username Reserved username")

        if self.contain_unlock_reserved(self.form_data.user_name):
            self.form_data.user_name = self.remove_unlock_reserved(self.form_data.user_name)

        if self.is_username_in_use(self.form_data.user_name):
            failed_tests.append("Username in use")

        if self.contains_forbidden_words(self.form_data.user_name):
            failed_tests.append("Username Forbidden words")

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests

    def check_user_first_name(self) -> (bool, list):
        """ check if first_name is valid """
        failed_tests = []
        if self.form_data.first_name is None or not 4 <= len(self.form_data.first_name) <= 25:
            failed_tests.append("First name length requirement")

        if not re.match(r"^[a-zA-Z0-9_-]+$", self.form_data.first_name):
            failed_tests.append("First name Allowed characters")

        if self.contains_forbidden_words(self.form_data.first_name):
            failed_tests.append("First name Forbidden words")

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests

    def check_user_last_name(self) -> (bool, list):
        """ check if last_name is valid """
        failed_tests = []
        if self.form_data.last_name is None or not 4 <= len(self.form_data.last_name) <= 25:
            failed_tests.append("Last name length requirement")

        if not re.match(r"^[a-zA-Z0-9_-]+$", self.form_data.last_name):
            failed_tests.append("Last name allowed characters")

        if self.contains_forbidden_words(self.form_data.last_name):
            failed_tests.append("Last name forbidden words")

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests

    def check_user_email(self) -> (bool, list):
        """ check if email is valid """
        failed_tests = []
        if self.form_data.email is None or not 6 <= len(self.form_data.email) <= 25:
            failed_tests.append("Email length requirement")

        if not re.match(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", self.form_data.email):
            failed_tests.append("Email bad format")

        if self.contains_forbidden_words(self.form_data.email):
            failed_tests.append("Email forbidden words")

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests

    def check_user_birth_date(self) -> (bool, list):
        """ check if birth date is valid """
        failed_tests = []
        if self.form_data.birth_date is None:
            failed_tests.append("Birthdate length requirement")

        # check if birth date is in the future
        if self.form_data.birth_date > date.today():
            failed_tests.append("Birthdate in the future")

        # check if user age is between 18 and 60
        if not 18 <= (date.today() - self.form_data.birth_date).days // 365 <= 60:
            failed_tests.append("Birthdate age requirement")

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests

    def check_gender(self) -> (bool, list):
        """ check if gender is valid """
        failed_tests = []
        # check if is well in gender_list
        in_list = False
        for gender in gender_list:
            if gender == self.form_data.gender:
                in_list = True
                break
        if not in_list:
            failed_tests.append("Unknown gender")

        is_valid = len(failed_tests) == 0
        return is_valid, failed_tests

    def check_sexual_orientation(self) -> (bool, list):
        """ Check if sexual_orientation is valid """
        failed_tests = []
        # check if is well in sexual_orientation_list
        in_list = False
        for sexual_orientation in sexual_orientation_list:
            if sexual_orientation == self.form_data.sexual_orientation:
                in_list = True
                break
        if not in_list:
            failed_tests.append("Unknown sexual orientation")

        is_valid = len(failed_tests) == 0
        return is_valid, failed_tests

    def check_couple_state(self) -> (bool, list):
        """ Check if couple_state is valid """
        failed_tests = []
        # check if is well in couple_state_list
        in_list = False
        for couple_state in couple_state_list:
            if couple_state == self.form_data.couple_state:
                in_list = True
                break
        if not in_list:
            failed_tests.append("Unknown couple state")

        is_valid = len(failed_tests) == 0
        return is_valid, failed_tests

    def check_alcohol(self) -> (bool, list):
        """ Check if alcohol is valid """
        failed_tests = []
        # check if is well in alcohol_list
        in_list = False
        for alcohol in alcohol_list:
            if alcohol == self.form_data.alcohol:
                in_list = True
                break
        if not in_list:
            failed_tests.append("Unknown alcohol")

        is_valid = len(failed_tests) == 0
        return is_valid, failed_tests

    def check_password(self) -> (bool, list):
        """ check if password is valid """
        failed_tests = []
        if self.form_data.password is None or not 8 <= len(self.form_data.password) <= 30:
            failed_tests.append("Password Length requirement")

        # check if password contains at least one uppercase
        if not re.search(r"[A-Z]", self.form_data.password):
            failed_tests.append("Password uppercase requirement")

        # check if password contains at least one lowercase
        if not re.search(r"[a-z]", self.form_data.password):
            failed_tests.append("Password lowercase requirement")

        # check if password contains at least one digit
        if not re.search(r"\d", self.form_data.password):
            failed_tests.append("Password digit requirement")

        # check if password contains at least one special character
        if not re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~" + r'"]', self.form_data.password):
            failed_tests.append("Password special character requirement")

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests

    def check_form(self) -> (bool, list):
        """ check all form """
        failed_tests = []
        # check user_name
        is_valid, failed_tests_user_name = self.check_user_name()
        if not is_valid:
            failed_tests.append(failed_tests_user_name)

        # check first_name
        is_valid, failed_tests_first_name = self.check_user_first_name()
        if not is_valid:
            failed_tests.append(failed_tests_first_name)

        # check last_name
        is_valid, failed_tests_last_name = self.check_user_last_name()
        if not is_valid:
            failed_tests.append(failed_tests_last_name)

        # check email
        is_valid, failed_tests_email = self.check_user_email()
        if not is_valid:
            failed_tests.append(failed_tests_email)

        # check birth_date
        is_valid, failed_tests_birth_date = self.check_user_birth_date()
        if not is_valid:
            failed_tests.append(failed_tests_birth_date)

        # check gender
        is_valid, failed_tests_gender = self.check_gender()
        if not is_valid:
            failed_tests.append(failed_tests_gender)

        # check sexual_orientation
        is_valid, failed_tests_sexual_orientation = self.check_sexual_orientation()
        if not is_valid:
            failed_tests.append(failed_tests_sexual_orientation)

        # check couple_state
        is_valid, failed_tests_couple_state = self.check_couple_state()
        if not is_valid:
            failed_tests.append(failed_tests_couple_state)

        # check alcohol
        is_valid, failed_tests_alcohol = self.check_alcohol()
        if not is_valid:
            failed_tests.append(failed_tests_alcohol)

        # check password
        is_valid, failed_tests_password = self.check_password()
        if not is_valid:
            failed_tests.append(failed_tests_password)

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests
