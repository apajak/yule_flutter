from yule_modules.user.user_manager import UserUpdate
import re
import json
from yule_modules.database.user import UserDatabase
import os
from dotenv import load_dotenv
from datetime import date

load_dotenv(".env")
db_config = {
    "host": os.getenv("DATABASE_HOST"),
    "user": os.getenv("DATABASE_USER"),
    "password": os.getenv("DATABASE_PASSWORD"),
    "database": os.getenv("DATABASE_NAME"),
}
user_db = UserDatabase(**db_config)
gender_list = ["female", "male", "ND"]
sexual_orientation_list = ["heterosexual", "homosexual", "ND"]
couple_state_list = ["single", "couple", "open_relationship", "ND"]
alcohol_list = ["never", "occasionally", "regularly", "ND"]
reserved_user_name_list = json.load(open("./yule_modules/security/reserved_user_name.json", "r"))
profanity_en = json.load(open("./yule_modules/security/profanity_list_en.json", "r"))
profanity_fr = json.load(open("./yule_modules/security/profanity_list_fr.json", "r"))
profanity_es = json.load(open("./yule_modules/security/profanity_list_es.json", "r"))
system_banned_words = json.load(open("./yule_modules/security/system_banned_list.json", "r"))
forbidden_words = profanity_es + profanity_fr + profanity_en + system_banned_words

unlock_reserved = "-elds-hoss-thrust"


class ProfileUpdateVerifier:
    def __init__(self, form_data: UserUpdate, user):
        self.form_data = form_data
        self.user = user

    @staticmethod
    def is_reserved_username(user_name: str) -> bool:
        """ Check if user_name is reserved """
        if user_name in reserved_user_name_list:
            return True
        return False

    @staticmethod
    def contain_unlock_reserved(user_name: str) -> bool:
        """ Check if user_name contain unlock reserved """
        if unlock_reserved in user_name:
            return True
        return False

    @staticmethod
    def remove_unlock_reserved(user_name: str) -> str:
        """ Remove unlock reserved """
        return user_name.replace(unlock_reserved, "")

    @staticmethod
    def is_username_in_use(user_name: str, base: str) -> bool:
        """ Check if user_name is in use """
        if user_name == base:
            return False
        return user_db.user_name_exist(user_name)

    def is_mail_in_use(self, email: str) -> bool:
        """ Check if email is in use """
        if email == self.user.email:
            return False
        return user_db.user_mail_exist(email)

    @staticmethod
    def contains_forbidden_words(user_name: str) -> bool:
        """ Check if user_name contains forbidden words """
        for word in forbidden_words:
            if word.lower() in user_name.lower() and len(word) > 3:
                print(word)
                return True
        return False

    def check_user_name(self) -> (bool, list):
        """ Check if user_name is valid """
        failed_tests = []

        if self.form_data.user_name is None or not 4 <= len(self.form_data.user_name) <= 25:
            failed_tests.append("Username length requirement")

        if not re.match(r"^[a-zA-Z0-9_-]+$", self.form_data.user_name):
            failed_tests.append("Username Allowed characters")

        if self.is_reserved_username(self.form_data.user_name):
            failed_tests.append("Username Reserved username")

        if self.contain_unlock_reserved(self.form_data.user_name):
            self.form_data.user_name = self.remove_unlock_reserved(self.form_data.user_name)

        if self.is_username_in_use(self.form_data.user_name, self.user.user_name):
            failed_tests.append("Username in use")

        if self.contains_forbidden_words(self.form_data.user_name):
            failed_tests.append("Username Forbidden words")

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests

    def check_user_email(self) -> (bool, list):
        """ check if email is valid """
        failed_tests = []
        if self.form_data.email is None or not 6 <= len(self.form_data.email) <= 25:
            failed_tests.append("Email length requirement")

        if not re.match(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", self.form_data.email):
            failed_tests.append("Email bad format")

        if self.contains_forbidden_words(self.form_data.email):
            failed_tests.append("Email forbidden words")

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests

    def check_sexual_orientation(self) -> (bool, list):
        """ Check if sexual_orientation is valid """
        failed_tests = []
        # check if is well in sexual_orientation_list
        in_list = False
        for sexual_orientation in sexual_orientation_list:
            if sexual_orientation == self.form_data.sexual_orientation:
                in_list = True
                break
        if not in_list:
            failed_tests.append("Unknown sexual orientation")

        is_valid = len(failed_tests) == 0
        return is_valid, failed_tests

    def check_couple_state(self) -> (bool, list):
        """ Check if couple_state is valid """
        failed_tests = []
        # check if is well in couple_state_list
        in_list = False
        for couple_state in couple_state_list:
            if couple_state == self.form_data.couple_state:
                in_list = True
                break
        if not in_list:
            failed_tests.append("Unknown couple state")

        is_valid = len(failed_tests) == 0
        return is_valid, failed_tests

    def check_alcohol(self) -> (bool, list):
        """ Check if alcohol is valid """
        failed_tests = []
        # check if is well in alcohol_list
        in_list = False
        for alcohol in alcohol_list:
            if alcohol == self.form_data.alcohol:
                in_list = True
                break
        if not in_list:
            failed_tests.append("Unknown alcohol")

        is_valid = len(failed_tests) == 0
        return is_valid, failed_tests

    def check_profile_update(self) -> (bool, list):
        failed_tests = []

        is_valid, failed_tests_user_name = self.check_user_name()
        if not is_valid:
            failed_tests.append(failed_tests_user_name)

        is_valid, failed_tests_email = self.check_user_email()
        if not is_valid:
            failed_tests.append(failed_tests_email)

        is_valid, failed_tests_sexual_orientation = self.check_sexual_orientation()
        if not is_valid:
            failed_tests.append(failed_tests_sexual_orientation)

        is_valid, failed_tests_couple_state = self.check_couple_state()
        if not is_valid:
            failed_tests.append(failed_tests_couple_state)

        is_valid, failed_tests_alcohol = self.check_alcohol()
        if not is_valid:
            failed_tests.append(failed_tests_alcohol)

        is_valid = len(failed_tests) == 0

        return is_valid, failed_tests
