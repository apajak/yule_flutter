import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:yule_flutter/models/user.dart';
import 'dart:io';
import '../../models/flutter_secure_storage.dart';

class ApiService {
  final String baseUrl;
  final storage = UserDataStorage();

  ApiService() : baseUrl = 'http://10.0.2.2:8000';

  // test server connection
  Future<Map<String, dynamic>> ping() async {
    final response = await http.get(Uri.parse('$baseUrl/ping'));

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to ping the server');
    }
  }

  // test database connection
  Future<Map<String, dynamic>> databasePing() async {
    final response = await http.get(Uri.parse('$baseUrl/database_ping'));

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to database ping the server');
    }
  }

  // Register new user
  Future<Map<String, dynamic>> register(UserCreate userCreate) async {
    final response = await http.post(
      Uri.parse('$baseUrl/register'),
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(userCreate.toJson()),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to register user on the server');
    }
  }

  // Update user profile
  Future<Map<String, dynamic>> updateProfile(UserUpdate userUpdate) async {
    print(userUpdate.toJson());
    final response = await http.post(
      Uri.parse('$baseUrl/update_profile'),
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(userUpdate.toJson()),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to update profile on the server');
    }
  }

  // Update user password
  Future<Map<String, dynamic>> updatePassword(
      UserUpdatePassword userPasswordUpdate) async {
    print(userPasswordUpdate.toJson());
    final response = await http.post(
      Uri.parse('$baseUrl/update_password'),
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(userPasswordUpdate.toJson()),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to update password on the server');
    }
  }

  // Login user
  Future<Map<String, dynamic>> login(UserCredentials userCredentials) async {
    final response = await http.post(
      Uri.parse('$baseUrl/login'),
      headers: {
        'Content-Type': 'application/json',
      },
      body: json
          .encode(userCredentials.toJson()), // Convert UserCredentials to JSON
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to login user on the server');
    }
  }

  // Upload profile picture
  Future<Map<String, dynamic>> uploadProfilePicture(
      File imageFile, int userId) async {
    final uploadUrl =
        Uri.parse('$baseUrl/upload/profile-picture?user_id=$userId');

    try {
      var request = http.MultipartRequest('POST', uploadUrl);
      var multipartFile =
          await http.MultipartFile.fromPath('file', imageFile.path);

      request.files.add(multipartFile);

      var response = await request.send();

      if (response.statusCode == 200) {
        var responseJson = await response.stream.bytesToString();
        return json.decode(responseJson);
      } else {
        throw Exception('Failed to upload profile picture');
      }
    } catch (e) {
      throw Exception('Failed to upload profile picture: $e');
    }
  }

  // get all users that are not friends of the user (DEV ONLY)
  Future<Map<String, dynamic>> getUsersNotFriends() async {
    final userToken = await storage.getIdentificationToken();
    final Uri uri =
        Uri.parse('$baseUrl/get_users_not_friends?user_token=$userToken');

    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to retrieve users not friends');
    }
  }

  // Get friends of the user
  Future<Map<String, dynamic>> getFriends() async {
    final userToken = await storage.getIdentificationToken();
    final Uri uri =
        Uri.parse('$baseUrl/get_user_friends?user_token=$userToken');

    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
      },
    );
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to retrieve friends');
    }
  }

  // Search new friends by username
  Future<Map<String, dynamic>> searchUsers(String usernameSlice) async {
    final userToken = await storage.getIdentificationToken();
    final Uri uri = Uri.parse(
        '$baseUrl/search_users?user_token=$userToken&username_slice=$usernameSlice');

    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to search for users');
    }
  }

  // remove friend
  Future<Map<String, dynamic>> removeFriend(String friendId) async {
    final userToken = await storage.getIdentificationToken();
    final url = '$baseUrl/remove_friend';

    final headers = {
      'Authorization': 'Bearer $userToken',
      'Content-Type': 'application/json',
    };

    final body = {
      'user_token': userToken,
      'user_uuid': friendId,
    };

    try {
      final response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        // Successful response from the server
        final responseData = jsonDecode(response.body);
        return responseData;
      } else {
        // Handle API errors, e.g., server returned an error status code
        return {
          'value': false,
          'error': 'Failed to remove friend',
        };
      }
    } catch (e) {
      // Handle network errors or exceptions
      return {
        'value': false,
        'error': 'Network error: $e',
      };
    }
  }

  // add friend
  Future<Map<String, dynamic>> sendFriendRequest(String friendUuid) async {
    final userToken = await storage.getIdentificationToken();
    final url = '$baseUrl/send_friend_request';
    final headers = {
      'Content-Type': 'application/json',
    };

    final body = {
      'user_token': userToken,
      'user_uuid': friendUuid,
    };

    try {
      final response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: jsonEncode(body),
      );
      print(response.body);
      if (response.statusCode == 200) {
        // Successful response from the server
        final responseData = jsonDecode(response.body);
        return responseData;
      } else {
        // Handle API errors, e.g., server returned an error status code
        return {
          'value': false,
          'error': 'Failed to send friend request',
        };
      }
    } catch (e) {
      // Handle network errors or exceptions
      return {
        'value': false,
        'error': 'Network error: $e',
      };
    }
  }

  Future<List<SimpleUser>> getFriendRequests() async {
    final userToken = await storage.getIdentificationToken();
    final response = await http.get(
      Uri.parse('$baseUrl/get_friend_requests?user_token=$userToken'),
      headers: {'Content-Type': 'application/json'},
    );

    if (response.statusCode == 200) {
      final responseData = json.decode(response.body);
      print(responseData);

      if (responseData['value']) {
        List<SimpleUser> requests = (responseData['friend_requests'] as List)
            .map((request) => SimpleUser.fromJson(request))
            .toList();
        return requests;
      } else {
        throw Exception(responseData['message']);
      }
    } else {
      throw Exception('Failed to retrieve friend requests');
    }
  }

  Future<bool> acceptFriendRequest(String friendUuid) async {
    final userToken = await storage.getIdentificationToken();
    final response = await http.post(
      Uri.parse('$baseUrl/accept_friend_request'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'user_token': userToken,
        'user_uuid': friendUuid,
      }),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body)['value'];
    } else {
      throw Exception('Failed to accept friend request');
    }
  }

  Future<bool> declineFriendRequest(String friendUuid) async {
    final userToken = await storage.getIdentificationToken();
    final response = await http.post(
      Uri.parse('$baseUrl/decline_friend_request'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({
        'user_token': userToken,
        'user_uuid': friendUuid,
      }),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body)['value'];
    } else {
      throw Exception('Failed to decline friend request');
    }
  }

  Future<Map<String, dynamic>> createGame() async {
    final userToken = await storage.getIdentificationToken();
    final response = await http.post(
      Uri.parse('$baseUrl/create-game'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({'user_token': userToken}),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to create game');
    }
  }

  Future<Map<String, dynamic>> deleteGame(String identifier) async {
    final userToken = await storage.getIdentificationToken();
    final response = await http.post(
      Uri.parse('$baseUrl/delete-game'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({'user_token': userToken, 'identifier': identifier}),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to delete game');
    }
  }

  // Register new fcm token
  Future<Map<String, dynamic>> sendFcmToken(String fcmToken, userToken) async {
    final url = '$baseUrl/send_fcm_token';

    print(fcmToken);
    print(userToken);

    final headers = {
      'Content-Type': 'application/json',
    };

    final body = {
      'user_token': userToken,
      'fcm_token': fcmToken,
    };

    try {
      final response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        // Successful response from the server
        final responseData = jsonDecode(response.body);
        return responseData;
      } else {
        // Handle API errors, e.g., server returned an error status code
        return {
          'value': false,
          'error': 'Failed to send FCM token',
        };
      }
    } catch (e) {
      // Handle network errors or exceptions
      return {
        'value': false,
        'error': 'Network error: $e',
      };
    }
  }

  Future<Map<String, dynamic>> sendGameInvite(
      String identifier, String friendUuid) async {
    final url = '$baseUrl/invite-friend';
    final userToken = await storage.getIdentificationToken();

    print('Inviting friend to game: $identifier');
    print('Friend UUID: $friendUuid');

    final headers = {
      'Content-Type': 'application/json',
    };

    final body = {
      'user_token': userToken,
      'identifier': identifier,
      'friend_uuid': friendUuid,
    };

    try {
      final response = await http.post(
        Uri.parse(url),
        headers: headers,
        body: jsonEncode(body),
      );

      if (response.statusCode == 200) {
        // Successful response from the server
        final responseData = jsonDecode(response.body);
        return responseData;
      } else {
        // Handle API errors, e.g., server returned an error status code
        return {
          'value': false,
          'error': 'Failed to invite friend',
        };
      }
    } catch (e) {
      // Handle network errors or exceptions
      return {
        'value': false,
        'error': 'Network error: $e',
      };
    }
  }

  Future<Map<String, dynamic>> getGameServerUrl(String gameIdentifier) async {
    print('Getting game server URL for game: $gameIdentifier');
    final userToken = await storage.getIdentificationToken();
    final Uri uri = Uri.parse(
        '$baseUrl/get-game?user_token=$userToken&identifier=$gameIdentifier');
    final response = await http.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to search for users');
    }
  }
}
