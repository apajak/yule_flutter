import 'package:socket_io_client/socket_io_client.dart' as IO;
import '../models/flutter_secure_storage.dart';
import '../models/user.dart';

class SocketIoService {
  static final SocketIoService _instance = SocketIoService._internal();
  SocketIoService._internal();

  factory SocketIoService() {
    return _instance;
  }
  late IO.Socket socket;
  Player? player;

  // Event callbacks
  Function? onGameStarted;
  Function? onGameStateUpdate;
  Function? onPlayerListUpdate;

  Function? onSelectPlayerBroadcast;
  Function? onSelectPlayerEvent;
  Function? onCountSipsBroadcast;
  Function? onCountSips;
  Function? onServantsBroadcast;
  Function? onValidateServantsBroadcast;
  Function? onServants;
  Function? onCustomRuleBroadcast;
  Function? onCustomRule;
  Function? onDrawCardBroadcast;
  Function? onNewTurnBroadcast;
  Function? onEndGameBroadcast;
  Function? broadcastFirstTurn;

  // Event handler fields
  Function? onConnectHandler;
  Function? onDisconnectHandler;

  //game event listeners
  Function? onDrinkBroadcast;
  Function? onTeamDrinkBroadcast;
  Function? onDrink;
  Function? onDrinkServant;
  Function? onQueenBroadcast;
  Function? onKingBroadcast;
  Function? onThumb_master_broadcast;
  Function? onThumb_master;
  Function? onThumb_master_drink_broadcast;
  Function? onThumb_master_drink;
  Function? onShifumi_broadcast;
  Function? onShifumi;
  Function? onShifumi_preturn_event_master;
  Function? onShifumi_turn_event_master;
  Function? onShifumi_preturn_event_player;
  Function? onShifumi_turn_event_player;
  Function? onShifumi_turn_event_broadcast;
  Function? onShifumi_result_broadcast;
  Function? onShifumi_result_inter_broadcast;
  Function? onShifumi_drink;


  void createSocketConnection(String url) {
    socket = IO.io(url, <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
    });

    // Internal handling of connect and disconnect
    socket.onConnect((_) {
      if (onConnectHandler != null) {
        onConnectHandler!();
      }
    });

    socket.onDisconnect((_) {
      if (onDisconnectHandler != null) {
        onDisconnectHandler!();
      }
    });
  }

  void setupListeners() {
    // Listening for periodic events
    socket.on('periodic_event', (data) {
      print('Periodic event received: $data');
    });

    // Listen for game start event
    socket.on('game_started', (data) {
      if (onGameStarted != null) {
        onGameStarted!(data);
      }
    });

    // Listen for game state updates
    socket.on('game_state_update', (data) {
      if (onGameStateUpdate != null) {
        onGameStateUpdate!(data);
      }
    });

    // Listen for player list updates
    socket.on('player_list_update', (data) {
      if (onPlayerListUpdate != null) {
        onPlayerListUpdate!(data);
      }
    });

    socket.on('select_player_broadcast', (data) => onSelectPlayerBroadcast?.call(data));
    socket.on('select_player', (data) => onSelectPlayerEvent?.call(data));
    socket.on('count_sips_broadcast', (data) => onCountSipsBroadcast?.call(data));
    socket.on('count_sips', (data) => onCountSips?.call(data));
    socket.on('servants_broadcast', (data) => onServantsBroadcast?.call(data));
    socket.on('validate_servants_broadcast', (data) => onValidateServantsBroadcast?.call(data));
    socket.on('servants', (data) => onServants?.call(data));
    socket.on('custom_rule_broadcast', (data) => onCustomRuleBroadcast?.call(data));
    socket.on('custom_rule', (data) => onCustomRule?.call(data));
    socket.on('draw_card', (data) => onDrawCardBroadcast?.call(data));
    socket.on('new_turn', (data) => onNewTurnBroadcast?.call(data));
    socket.on('end_game', (data) => onEndGameBroadcast?.call(data));
    socket.on('drink_broadcast', (data) => onDrinkBroadcast?.call(data));
    socket.on('team_drink_broadcast', (data) => onTeamDrinkBroadcast?.call(data));
    socket.on('drink', (data) => onDrink?.call(data));
    socket.on('drink_servant', (data) => onDrinkServant?.call(data));
    socket.on('broadcast_first_turn', (data) => broadcastFirstTurn?.call(data));
    socket.on('queen_broadcast', (data) => onQueenBroadcast?.call(data));
    socket.on('king_broadcast', (data) => onKingBroadcast?.call(data));
    socket.on('thumb_master_broadcast', (data) => onThumb_master_broadcast?.call(data));
    socket.on('thumb_master', (data) => onThumb_master?.call(data));
    socket.on('thumb_master_drink_broadcast', (data) => onThumb_master_drink_broadcast?.call(data));
    socket.on('thumb_master_drink', (data) => onThumb_master_drink?.call(data));
    socket.on('shifumi_broadcast', (data) => onShifumi_broadcast?.call(data));
    socket.on('shifumi', (data) => onShifumi?.call(data));
    socket.on('shifumi_preturn_event_master', (data) => onShifumi_preturn_event_master?.call(data));
    socket.on('shifumi_turn_event_master', (data) => onShifumi_turn_event_master?.call(data));
    socket.on('shifumi_preturn_event_player', (data) => onShifumi_preturn_event_player?.call(data));
    socket.on('shifumi_turn_event_player', (data) => onShifumi_turn_event_player?.call(data));
    socket.on('shifumi_turn_event_broadcast', (data) => onShifumi_turn_event_broadcast?.call(data));
    socket.on('shifumi_result_broadcast', (data) => onShifumi_result_broadcast?.call(data));
    socket.on('shifumi_result_inter_broadcast', (data) => onShifumi_result_inter_broadcast?.call(data));
    socket.on('shifumi_drink', (data) => onShifumi_drink?.call(data));
  }

  Future<Player> _loadUserData() async {
    // Fetch the user data from secure storage
    final storage = UserDataStorage();
    final User? user = await storage.getUserData();
    if (user != null) {
      return user.toPlayer();
    } else {
      throw Exception('User data not found');
    }
  }

  void connect() async {
    if (!socket.connected) {
      socket.connect();

      // Listen for the server's request for player data
      socket.on('request_player_data', (_) async {
        Player player =
            await _loadUserData(); // Load user data and convert to Player
        socket.emit(
            'player_data', player.toMap()); // Send player data to server
      });
    }
  }

  void disconnect() {
    if (socket.connected) {
      socket.disconnect();
    }
  }

  void kickPlayer(String userUuid) {
    print("Kicking player $userUuid");
    socket.emit('kick_player', {
      'playerUuid': userUuid,
    });
  }

  void on(String event, Function(dynamic) callback) {
    socket.on(event, callback);
  }
  void getFirstTurn() {
    socket.emit('get_first_turn', []);
  }
  void requestPlayerList() {
    socket.emit('get_player_list', []); // Envoyer l'événement sans données supplémentaires
  }

  void requestGameState() {
    socket.emit('request_game_state', []); // No additional data needed
  }


  void startGame() {
    socket.emit('start_game', []); // Envoyer l'événement sans données supplémentaires
  }

  // Méthode pour valider les sips
  void validateSips(sipsValue) {
    socket.emit('validate_sips', {'sips': sipsValue});
  }

  // Méthode pour valider le nouveau thumb master
  void validateThumbMaster() {
    socket.emit('thumb_master_validate', []);
  }

  // Méthode pour l'evenement thumb master
  void thumbMasterEvent(userUuid) {
    socket.emit('thumb_master_event', {'thumbedPlayerUuid': userUuid});
  }

  // Méthode pour valider la gorgée du thumb master
  void validateThumbMasterDrink() {
    socket.emit('thumb_master_drink_validate', []);
  }

  // Méthode pour valider les servants
  void validateServants() {
    socket.emit('validate_servants', []);
  }

  // Méthode pour piocher une carte
  void drawCard() {
    socket.emit('draw_card', []);
  }

  // Méthode pour envoyer le nombre de sips
  void sendCountSips(int sips) {
    socket.emit('send_count_sips', {'sips': sips});
  }
  void sendSelectedPlayers(List<String> playerUuids, List<int> sips) {
    socket.emit('send_selected_players', {
      'selectedPlayers': playerUuids,
      'sips': sips
    });
  }

  void sendSelectedServants(String uuid) {
    print("Sending servants" + uuid);
    socket.emit('validate_servants', {
      'selectedServant': uuid,
    });
  }
  void sendGameAlcoholLevel(int level) {
    socket.emit('send_game_alcohol', {'gameAlcohol': level});
  }
  void validateShifumi() {
    socket.emit('shifumi_validate');
  }
  void validateShifumiPreturn() {
    socket.emit('shifumi_preturn_validate');
  }
  void shifumiTurnResult(int sips) {
    socket.emit('shifumi_turn_result', {'sips': sips});
  }

// Additional methods for game logic can be added here
}
