import 'package:flutter/material.dart';

class CustomSearchBar extends StatelessWidget {
  final TextEditingController controller;
  final ValueChanged<String> onChanged;
  final VoidCallback onClear;
  final VoidCallback onSearch;

  CustomSearchBar({
    required this.controller,
    required this.onChanged,
    required this.onClear,
    required this.onSearch,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      onChanged: onChanged,
      onSubmitted: (_) {
        onSearch(); // Trigger the search when the user submits the text
      },
      decoration: InputDecoration(
        hintText: 'Search...',
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Theme.of(context).highlightColor,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Theme.of(context).highlightColor,
          ),
        ),
        // Customize the text color
        labelStyle: TextStyle(color: Theme.of(context).highlightColor),
        prefixIcon: IconButton(
          icon: Icon(Icons.search),
          onPressed: onSearch,
          color: Theme.of(context).highlightColor,
        ),
        suffixIcon: IconButton(
          icon: Icon(Icons.clear),
          onPressed: onClear,
          color: Theme.of(context).highlightColor,
        ),
      ),
    );
  }
}
