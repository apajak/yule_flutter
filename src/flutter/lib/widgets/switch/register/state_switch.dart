import 'package:flutter/material.dart';

class StateSwitch extends StatefulWidget {
  final String label;
  final List<String> values;
  final ValueChanged<String> onChanged;
  final ValueNotifier<int> selectedValueNotifier;

  StateSwitch({
    required this.label,
    required this.values,
    required this.onChanged,
    required this.selectedValueNotifier,
  });

  @override
  _StateSwitchState createState() => _StateSwitchState();
}

class _StateSwitchState extends State<StateSwitch> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          widget.label,
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 8),
        Row(
          children: List.generate(
            widget.values.length,
            (index) => _buildSwitchButton(widget.values[index], index),
          ),
        ),
      ],
    );
  }

  Widget _buildSwitchButton(String value, int index) {
    return GestureDetector(
      onTap: () {
        widget.selectedValueNotifier.value = index; // Update the selected index
        widget.onChanged(value);
      },
      child: ValueListenableBuilder<int>(
        valueListenable: widget.selectedValueNotifier,
        builder: (context, selectedIndex, child) {
          return Container(
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              border: Border.all(
                color: selectedIndex == index ? Colors.blue : Colors.grey,
              ),
              borderRadius: BorderRadius.circular(4),
              color: selectedIndex == index ? Colors.blue : Colors.white,
            ),
            child: Text(
              value,
              style: TextStyle(
                fontSize: 16,
                color: selectedIndex == index ? Colors.white : Colors.black,
              ),
            ),
          );
        },
      ),
    );
  }
}
