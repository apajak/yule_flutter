import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ProfilePicture_L extends StatelessWidget {
  final String? imageUrl;
  final String defaultImagePath;

  ProfilePicture_L({
    required this.imageUrl,
    this.defaultImagePath =
        'assets/profile_pics/default_pp.png', // Ensure this path is correct
  });

  Future<bool> _isImageLoadable(String? url) async {
    if (url == null) return false; // Return false if the URL is null
    try {
      final response = await http.head(Uri.parse(url));
      return response.statusCode == 200;
    } catch (e) {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: _isImageLoadable(imageUrl),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data == true &&
            imageUrl != null) {
          // Network image
          return CircleAvatar(
            radius: 100, // Adjust the radius as per your design
            backgroundImage: NetworkImage(imageUrl!),
          );
        } else {
          // Default image
          return CircleAvatar(
            radius: 100, // Adjust the radius as per your design
            backgroundImage: AssetImage(defaultImagePath),
          );
        }
      },
    );
  }
}
