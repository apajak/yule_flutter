import 'package:flutter/material.dart';

class SmallProfilePictureWithBadge extends StatelessWidget {
  final String imageUrl;
  final double size;
  final int badgeNumber;

  SmallProfilePictureWithBadge({
    required this.imageUrl,
    this.size = 20.0,
    required this.badgeNumber,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none, // Permet au badge de déborder du cercle de l'image
      children: [
        Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(imageUrl),
            ),
          ),
        ),
        if (badgeNumber > 0) // Afficher le badge seulement si badgeNumber > 0
          Positioned(
            right: -size * 0.2, // Ajustez la position en fonction de la taille
            top: -size * 0.2, // Ajustez la position en fonction de la taille
            child: CircleAvatar(
              backgroundColor: Colors.red,
              radius: size * 0.3, // Taille du badge
              child: Text(
                badgeNumber.toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: size * 0.3, // Taille du texte dans le badge
                ),
              ),
            ),
          ),
      ],
    );
  }
}
