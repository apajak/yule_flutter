import 'package:flutter/material.dart';

class SmallProfilePictureWithBorder extends StatelessWidget {
  final String imageUrl;
  final double size;
  final double borderWidth;
  final Color borderColor;

  SmallProfilePictureWithBorder({
    required this.imageUrl,
    this.size = 20.0,
    this.borderWidth = 2.0,
    this.borderColor = Colors.orange,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size + borderWidth * 2, // Augmenter la taille pour la bordure
      height: size + borderWidth * 2, // Augmenter la taille pour la bordure
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: borderColor,
          width: borderWidth,
        ),
      ),
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            fit: BoxFit.cover,
            image: NetworkImage(imageUrl),
          ),
        ),
      ),
    );
  }
}
