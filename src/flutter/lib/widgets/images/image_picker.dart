import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:photo_view/photo_view.dart';
import 'dart:io';

class ImagePickerWidget extends StatefulWidget {
  @override
  _ImagePickerWidgetState createState() => _ImagePickerWidgetState();
}

class _ImagePickerWidgetState extends State<ImagePickerWidget> {
  File? _image;

  Future<void> _getImage(ImageSource source) async {
    final pickedFile = await ImagePicker().pickImage(source: source);

    if (pickedFile != null) {
      final croppedFile = await ImageCropper.platform.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(ratioX: 1, ratioY: 1),
        compressQuality: 100,
      );

      if (croppedFile != null) {
        setState(() {
          _image = File(croppedFile.path);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        if (_image == null) PlaceholderImage() else CircularImage(_image!),
        ElevatedButton(
          onPressed: () {
            _getImage(ImageSource.camera);
          },
          child: Text('Take a Photo'),
        ),
        ElevatedButton(
          onPressed: () {
            _getImage(ImageSource.gallery);
          },
          child: Text('Pick from Gallery'),
        ),
      ],
    );
  }
}

class CircularImage extends StatelessWidget {
  final File imageFile;

  CircularImage(this.imageFile);

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: PhotoView(
        imageProvider: FileImage(imageFile),
        backgroundDecoration: BoxDecoration(
          color: Colors.white,
        ),
      ),
    );
  }
}

class PlaceholderImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: Colors.blue, width: 2.0),
      ),
      child: Center(
        child: Icon(
          Icons.person,
          size: 100,
          color: Colors.blue,
        ),
      ),
    );
  }
}
