import 'package:flutter/material.dart';

class SmallProfilePicture extends StatelessWidget {
  final String imageUrl;
  final double size;

  SmallProfilePicture({required this.imageUrl, this.size = 20.0});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(imageUrl),
        ),
      ),
    );
  }
}
