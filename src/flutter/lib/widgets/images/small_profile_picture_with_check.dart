import 'package:flutter/material.dart';

class SmallProfilePictureWithCheck extends StatelessWidget {
  final String imageUrl;
  final double size;
  final bool isVerified;

  SmallProfilePictureWithCheck({
    required this.imageUrl,
    this.size = 20.0,
    required this.isVerified,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none, // Permet à l'icône de vérification de déborder du cercle de l'image
      children: [
        Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(imageUrl),
            ),
          ),
        ),
        if (isVerified) // Afficher l'icône de vérification seulement si isVerified est true
          Positioned(
            right: -size * 0.2, // Ajustez la position en fonction de la taille
            top: -size * 0.2, // Ajustez la position en fonction de la taille
            child: CircleAvatar(
              backgroundColor: Colors.red, // Couleur de fond du badge
              radius: size * 0.3, // Taille du badge
              child: Icon(
                Icons.check, // Icône de vérification
                color: Colors.white,
                size: size * 0.3, // Taille de l'icône
              ),
            ),
          ),
      ],
    );
  }
}
