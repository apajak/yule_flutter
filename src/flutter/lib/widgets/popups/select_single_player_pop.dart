import 'dart:math';

import 'package:flutter/material.dart';
import 'package:yule_flutter/models/user.dart';
import 'package:yule_flutter/widgets/images/small_profile_picture_with_check.dart';

class SelectSinglePlayerPop extends StatefulWidget {
  final String userName;
  final String profilePicture;
  final List<SimpleUser> suggestedPlayers;
  final List<SimpleUser> otherPlayers;
  final Function(String) onValidateSelection;

  SelectSinglePlayerPop({
    Key? key,
    required this.userName,
    required this.profilePicture,
    required this.suggestedPlayers,
    required this.otherPlayers,
    required this.onValidateSelection,
  }) : super(key: key);

  @override
  _SelectSinglePlayerPopState createState() => _SelectSinglePlayerPopState();
}

class _SelectSinglePlayerPopState extends State<SelectSinglePlayerPop> {
  String? selectedPlayerUuid;

  void _selectRandomPlayer() {
    if (widget.suggestedPlayers.isNotEmpty) {
      final randomIndex = Random().nextInt(widget.suggestedPlayers.length);
      setState(() {
        selectedPlayerUuid = widget.suggestedPlayers[randomIndex].userUuid;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Dialog(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text('${widget.userName}, choisis une personne', style: TextStyle(color: Colors.black)),
            ),
            Container(
              height: 100,
              child: _buildPlayerList(),
            ),
            IconButton(
              icon: Icon(Icons.shuffle), // Icône représentant le hasard
              onPressed: _selectRandomPlayer,
            ),
            ElevatedButton(
              child: Text('Valider'),
              onPressed: selectedPlayerUuid != null ? () {
                widget.onValidateSelection(selectedPlayerUuid!);
                Navigator.of(context).pop();
              } : null,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildPlayerList() {
    var combinedList = [...widget.suggestedPlayers, ...widget.otherPlayers];

    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: combinedList.length,
      itemBuilder: (context, index) {
        var player = combinedList[index];
        Color borderColor;

        // Déterminez la couleur de la bordure en fonction de la position du joueur et de la liste à laquelle il appartient
        if (index == 0 && widget.suggestedPlayers.contains(player)) {
          borderColor = Colors.amber; // Doré pour le premier de suggestedPlayers
        } else if (widget.suggestedPlayers.contains(player)) {
          borderColor = Colors.green; // Vert pour les autres dans suggestedPlayers
        } else {
          borderColor = Colors.transparent; // Sans couleur pour les joueurs dans otherPlayers
        }

        return GestureDetector(
          onTap: () {
            setState(() {
              selectedPlayerUuid = player.userUuid;
            });
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                CircleAvatar(
                  radius: 30,
                  backgroundColor: borderColor,
                  child: SmallProfilePictureWithCheck(
                    imageUrl: player.profilePicture ?? 'default_image_url',
                    size: 52.0,
                    isVerified: player.userUuid == selectedPlayerUuid,
                  ),
                ),
                Text(player.userName, style: TextStyle(color: Colors.black)),
              ],
            ),
          ),
        );
      },
    );
  }
}
