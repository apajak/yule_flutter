import 'package:flutter/material.dart';

class ThumbDrinkPop extends StatelessWidget {
  final String userName;
  final String gender;
  final VoidCallback onOkPressed;

  ThumbDrinkPop({
    Key? key,
    required this.userName,
    required this.gender,
    required this.onOkPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String sipsText = '';
    print(gender);
    if (gender == 'female') {
      sipsText = 'Tu es trop lente, bois une gorgée';
    } else {
      sipsText = 'Tu es trop lent, bois une gorgée';
    }



    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // Utilisation d'Image.asset pour charger l'image depuis les ressources
          Image.asset(
            'assets/images/vik.png',
            height: 100,
            width: 100,
          ),
          Text(
            userName,
            style: TextStyle(color: Colors.black), // Couleur du texte en noir
          ),
          Text(
            sipsText,
            style: TextStyle(color: Colors.black), // Couleur du texte en noir
          ),
        ],
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'OK',
            style: TextStyle(color: Colors.black), // Couleur du texte en noir pour le bouton
          ),
          onPressed: () {
            Navigator.of(context).pop();
            onOkPressed();
          },
        ),
      ],
    );
  }
}
