import 'package:flutter/material.dart';

import '../../models/user.dart';
import '../game_component/animated_timer_widget.dart';
import '../game_component/round_counter.dart';

enum ShifumiChoice { pierre, feuille, ciseau }

class ShifumiPlayer extends StatefulWidget {
  final SimpleUser master;
  final SimpleUser secondPlayer;
  final int numberOfRounds;
  final int currentRound;
  final int numberOfTurns;
  final int currentTurn;
  final Function(String, String, int) onGameResult;

  ShifumiPlayer({
    Key? key,
    required this.master,
    required this.secondPlayer,
    required this.numberOfRounds,
    required this.currentRound,
    required this.numberOfTurns,
    required this.currentTurn,
    required this.onGameResult,
  }) : super(key: key);

  @override
  _ShifumiPlayerState createState() => _ShifumiPlayerState();
}

class _ShifumiPlayerState extends State<ShifumiPlayer> {
  ShifumiChoice? selectedChoice;
  int lostRounds = 0;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Shifumi Duel', style: TextStyle(color: Colors.black)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RoundCounterDisplay(
                  totalRounds: widget.numberOfRounds,
                  currentRound: widget.currentRound,
                  iconData: Icons.people,
                  color: Colors.black
              ),
              RoundCounterDisplay(
                  totalRounds: widget.numberOfTurns,
                  currentRound: widget.currentTurn,
                  iconData: Icons.loop,
                  color: Colors.black
              ),
            ],
          ),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Text(
              'Choisissez votre coup',
              style: TextStyle(color: Colors.black),
            ),
          ),
          SizedBox(height: 20),
          AnimatedTimerWidget(
            duration: Duration(seconds: 5), // Durée du timer
            textColor: Colors.black, // Couleur du texte
            onTimerEnd: () {
              // Que faire lorsque le timer se termine
              print("Timer terminé");
            },
          ),
          SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: ShifumiChoice.values
                .map((choice) => _buildChoiceButton(choice))
                .toList(),
          ),
        ],
      ),
      actions: <Widget>[
        TextButton(
          child: Text('OK'),
          onPressed: () => _onGameFinished(),
        ),
      ],
    );
  }

  Widget _buildChoiceButton(ShifumiChoice choice) {
    IconData iconData;
    switch (choice) {
      case ShifumiChoice.pierre:
        iconData = Icons.circle;
        break;
      case ShifumiChoice.feuille:
        iconData = Icons.file_copy;
        break;
      case ShifumiChoice.ciseau:
        iconData = Icons.content_cut;
        break;
    }

    return Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.grey[300], // Arrière-plan clair pour les boutons
            borderRadius: BorderRadius.circular(30), // Coins arrondis
          ),
          padding: EdgeInsets.all(8), // Espacement autour de l'icône
          child: IconButton(
            icon: Icon(iconData, color: Colors.black),
            onPressed: () => _onChoiceSelected(choice),
          ),
        ),
        if (selectedChoice ==
            choice) // Afficher le badge si c'est le choix sélectionné
          Positioned(
            right: -5,
            top: -5,
            child: CircleAvatar(
              backgroundColor: Colors.red,
              radius: 12,
              child: Icon(
                Icons.check,
                color: Colors.white,
                size: 16,
              ),
            ),
          ),
      ],
    );
  }

  void _onChoiceSelected(ShifumiChoice choice) {
    setState(() {
      selectedChoice = choice;
    });
  }

  void _onGameFinished() {
    Navigator.of(context).pop();
    widget.onGameResult(
        widget.master.userUuid, widget.secondPlayer.userUuid, lostRounds);
  }
}
