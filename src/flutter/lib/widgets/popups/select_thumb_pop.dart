import 'package:flutter/material.dart';
import 'package:yule_flutter/models/user.dart';
import 'package:yule_flutter/widgets/images/small_profile_picture_with_check.dart';

class SelectThumbedPlayerPop extends StatefulWidget {
  final String userName;
  final String profilePicture;
  final List<SimpleUser> players;
  final Function(String) onValidateSelection;

  SelectThumbedPlayerPop({
    Key? key,
    required this.userName,
    required this.profilePicture,
    required this.players,
    required this.onValidateSelection,
  }) : super(key: key);

  @override
  _SelectThumbedPlayerPopState createState() => _SelectThumbedPlayerPopState();
}

class _SelectThumbedPlayerPopState extends State<SelectThumbedPlayerPop> {
  String? selectedPlayerUuid;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Dialog(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text('${widget.userName}, selectionne la ou le perdant', style: TextStyle(color: Colors.black)),
            ),
            Container(
              height: 120,
              child: _buildPlayerList(),
            ),
            ElevatedButton(
              child: Text('Valider'),
              onPressed: selectedPlayerUuid != null ? () {
                widget.onValidateSelection(selectedPlayerUuid!);
                Navigator.of(context).pop();
              } : null,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildPlayerList() {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: widget.players.length,
      itemBuilder: (context, index) {
        var player = widget.players[index];
        bool isSelected = player.userUuid == selectedPlayerUuid;

        return GestureDetector(
          onTap: () {
            setState(() {
              selectedPlayerUuid = player.userUuid;
            });
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0), // Ajouter un padding autour de chaque élément
            child: Column(
              children: [
                CircleAvatar(
                  radius: 30,
                  backgroundColor: isSelected ? Colors.amber : Colors.transparent,
                  child: SmallProfilePictureWithCheck(
                    imageUrl: player.profilePicture ?? 'default_image_url',
                    size: 52.0,
                    isVerified: isSelected,
                  ),
                ),
                Text(player.userName, style: TextStyle(color: Colors.black)),
              ],
            ),
          ),
        );
      },
    );
  }
}