import 'package:flutter/material.dart';

class DrinkLevelPopup extends StatefulWidget {
  final Function(int) onValidateSelection;
  final int initialLevel; // Paramètre pour le niveau initial

  DrinkLevelPopup({
    Key? key,
    required this.onValidateSelection,
    this.initialLevel = 0, // Valeur par défaut est 0
  }) : super(key: key);

  @override
  _DrinkLevelPopupState createState() => _DrinkLevelPopupState();
}

class _DrinkLevelPopupState extends State<DrinkLevelPopup> {
  late int selectedLevel;

  @override
  void initState() {
    super.initState();
    selectedLevel = widget.initialLevel; // Utiliser le niveau initial
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false, // Empêche la fermeture du popup
      child: GestureDetector(
        onTap: () {
          setState(() {
            selectedLevel = 0; // Réinitialiser les étoiles en tapant en dehors
          });
        },
        child: AlertDialog(
          title: Text("How much you want to drink?", style: TextStyle(color: Colors.black)),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: List.generate(5, (index) => buildStar(index)),
          ),
          actions: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
                widget.onValidateSelection(selectedLevel);
              },
              child: Text('Confirm'),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildStar(int index) {
    return IconButton(
      icon: Icon(
        index < selectedLevel ? Icons.star : Icons.star_border,
        color: index < selectedLevel ? Colors.yellow : Colors.grey,
      ),
      onPressed: () {
        setState(() {
          selectedLevel = index + 1; // Mettre à jour l'état pour refléter la sélection
        });
      },
    );
  }
}
