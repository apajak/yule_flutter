import 'package:flutter/material.dart';
import 'package:yule_flutter/widgets/images/profile_pic_large.dart';

class DrinkBroadcastPop extends StatelessWidget {
  final String userName;
  final String profilePicture;
  final int sips;

  DrinkBroadcastPop({
    Key? key,
    required this.userName,
    required this.profilePicture,
    required this.sips,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String sipsText = sips == -1 ? 'Finit son verre' : 'Doit boire $sips gorgée(s)';

    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ProfilePicture_L(imageUrl: profilePicture),
          Text(
            userName,
            style: TextStyle(color: Colors.black), // Texte en noir
          ),
          Text(
            sipsText,
            style: TextStyle(color: Colors.black), // Texte en noir
          ),
        ],
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'OK',
            style: TextStyle(color: Colors.black), // Bouton avec texte en noir
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ],
    );
  }
}
