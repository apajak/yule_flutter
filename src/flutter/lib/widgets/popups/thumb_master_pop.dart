import 'package:flutter/material.dart';

class ThumbMasterPop extends StatelessWidget {
  final String userName;
  final VoidCallback onOkPressed;
  ThumbMasterPop({
    Key? key,
    required this.userName,
    required this.onOkPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String sipsText = "Tu es le nouveau maître du pouce !";

    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            'assets/images/vik.png',
            height: 100,
            width: 100,
          ),
          Text(
            userName,
            style: TextStyle(color: Colors.black), // Texte en noir
          ),
          Text(
            sipsText,
            style: TextStyle(color: Colors.black), // Texte en noir
          ),
        ],
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'OK',
            style: TextStyle(color: Colors.black), // Bouton avec texte en noir
          ),
          onPressed: () {
            onOkPressed(); // Utiliser la fonction de callback fournie
            Navigator.of(context).pop(); // Fermer également le dialogue
          },
        ),
      ],
    );
  }
}
