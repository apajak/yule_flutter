import 'package:flutter/material.dart';
import 'package:yule_flutter/models/user.dart';
import '../images/profile_pic_small_badge.dart';

class SelectPlayerPop extends StatefulWidget {
  final String userName;
  final String gender;
  final String profilePicture;
  final int sips;
  final List<SimpleUser> suggestedPlayers;
  final List<SimpleUser> otherPlayers;
  final Function(List<String>, List<int>) onValidateSelection;

  SelectPlayerPop({
    Key? key,
    required this.userName,
    required this.gender,
    required this.profilePicture,
    required this.sips,
    required this.suggestedPlayers,
    required this.otherPlayers,
    required this.onValidateSelection,
  }) : super(key: key);

  @override
  _SelectPlayerPopState createState() => _SelectPlayerPopState();
}

class _SelectPlayerPopState extends State<SelectPlayerPop> {
  late int remainingSips;
  late Map<String, int> sipsAssigned;

  @override
  void initState() {
    super.initState();
    remainingSips = widget.sips;
    sipsAssigned = {};
  }

  @override
  Widget build(BuildContext context) {
    String cardTitle = '';
    String cardText = '';

    if (widget.gender == 'female') {
      if (widget.sips == 1) {
        cardTitle = 'Distribue 1 gorgée';
        cardText = '${widget.userName} à qui veux-tu distribuer cette gorgée?';
      } else {
        cardTitle = 'Distribue ${widget.sips} gorgées';
        cardText = '${widget.userName} à qui veux-tu distribuer ces gorgées?';
      }
    } else {
      if (widget.sips == 1) {
        cardTitle = 'Distribue 1 gorgée';
        cardText = '${widget.userName} à qui veux-tu distribuer cette gorgée?';
      } else {
        cardTitle = 'Distribue ${widget.sips} gorgées';
        cardText = '${widget.userName} à qui veux-tu distribuer ces gorgées?';
      }
    }

    return WillPopScope(
      onWillPop: () async => false,
      // Empêche la fermeture par le bouton de retour
      child: Dialog(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Text(cardTitle,
                      style: TextStyle(color: Colors.black, fontSize: 20)),
                  SizedBox(height: 20),
                  Text(cardText, style: TextStyle(color: Colors.black)),
                  SizedBox(height: 10),
                  Center(
                      child: Text('$remainingSips',
                          style: TextStyle(fontSize: 30, color: Colors.black))),
                  Container(
                    height: 100,
                    child: _buildPlayerList(),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: TextButton(
                      child: Text('Reset'),
                      onPressed: () {
                        setState(() {
                          sipsAssigned.clear();
                          remainingSips = widget.sips;
                        });
                      },
                    ),
                  ),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: TextButton(
                      child: Text('Valider'),
                      onPressed: remainingSips == 0
                          ? () {
                              List<String> selectedPlayerUuids = [];
                              List<int> sipsAssignedList = [];

                              sipsAssigned.forEach((userUuid, sips) {
                                if (sips > 0) {
                                  selectedPlayerUuids.add(userUuid);
                                  sipsAssignedList.add(sips);
                                }
                              });

                              if (selectedPlayerUuids.isNotEmpty) {
                                widget.onValidateSelection(
                                    selectedPlayerUuids, sipsAssignedList);
                              }
                              Navigator.of(context).pop();
                            }
                          : null, // Désactive le bouton si des gorgées restent à attribuer
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildPlayerList() {
    // Combine suggested and other players
    var combinedList = [...widget.suggestedPlayers, ...widget.otherPlayers];

    return Container(
      height: 100, // Adjust the height as needed
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: combinedList.length,
        itemBuilder: (context, index) {
          var player = combinedList[index];
          var sipsForPlayer = sipsAssigned[player.userUuid] ?? 0;

          Color borderColor;
          // Déterminez la couleur de la bordure en fonction de la position du joueur et de la liste à laquelle il appartient
          if (index == 0 && widget.suggestedPlayers.contains(player)) {
            borderColor =
                Colors.amber; // Doré pour le premier de suggestedPlayers
          } else if (widget.suggestedPlayers.contains(player)) {
            borderColor =
                Colors.green; // Vert pour les autres dans suggestedPlayers
          } else {
            borderColor = Colors
                .transparent; // Sans couleur pour les joueurs dans otherPlayers
          }

          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              onTap: () {
                if (remainingSips > 0) {
                  setState(() {
                    sipsAssigned.update(player.userUuid, (value) => value + 1,
                        ifAbsent: () => 1);
                    remainingSips--;
                  });
                }
              },
              onLongPress: () {
                // Attribuer toutes les gorgées restantes à ce joueur
                setState(() {
                  sipsAssigned.update(
                      player.userUuid, (value) => value + remainingSips,
                      ifAbsent: () => remainingSips);
                  remainingSips = 0;
                });
              },
                child: Column(
                  children: [
                    CircleAvatar(
                      radius: 30,
                      backgroundColor: borderColor,
                      child: SmallProfilePictureWithBadge(
                        imageUrl: player.profilePicture ?? 'default_image_url',
                        size: 52.0,
                        badgeNumber: sipsForPlayer,
                      ),
                    ),
                    Text(player.userName,
                        style: TextStyle(color: Colors.black)),
                  ],
                ),
              ),
            );
        },
      ),
    );
  }
}
