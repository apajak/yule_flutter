import 'package:flutter/material.dart';

class DrinkPop extends StatelessWidget {
  final String userName;
  final String gender;
  final int sips;
  final VoidCallback onOkPressed;

  DrinkPop({
    Key? key,
    required this.userName,
    required this.gender,
    required this.sips,
    required this.onOkPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String sipsText = '';
    print(sips);
    print(gender);
    if (sips == -1) {
      if (gender == 'female') {
        sipsText = 'Finis ton verre';
      } else {
        sipsText = 'Finis ton verre';
      }
    } else if (sips == 1) {
      if (gender == 'female') {
        sipsText = 'Bois $sips gorgée';
      } else {
        sipsText = 'Bois $sips gorgée';
      }
    } else {
      if (gender == 'female') {
        sipsText = 'Bois $sips gorgées';
      } else {
        sipsText = 'Bois $sips gorgées';
      }
    }


    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // Utilisation d'Image.asset pour charger l'image depuis les ressources
          Image.asset(
            'assets/images/vik.png',
            height: 100,
            width: 100,
          ),
          Text(
            userName,
            style: TextStyle(color: Colors.black), // Couleur du texte en noir
          ),
          Text(
            sipsText,
            style: TextStyle(color: Colors.black), // Couleur du texte en noir
          ),
        ],
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'OK',
            style: TextStyle(color: Colors.black), // Couleur du texte en noir pour le bouton
          ),
          onPressed: () {
            Navigator.of(context).pop();
            onOkPressed();
          },
        ),
      ],
    );
  }
}
