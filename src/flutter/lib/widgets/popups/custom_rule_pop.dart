import 'package:flutter/material.dart';
import 'package:yule_flutter/widgets/images/profile_pic_large.dart';

class CustomPop extends StatelessWidget {
  final String userName;
  final String profilePicture;
  final String rule;

  CustomPop({
    Key? key,
    required this.userName,
    required this.profilePicture,
    required this.rule,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String sipsText = rule;

    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
        Image.asset(
        'assets/images/vik.png',
        height: 100,
        width: 100,
      ),
          Text(
            userName,
            style: TextStyle(color: Colors.black), // Texte en noir
          ),
          Text(
            sipsText,
            style: TextStyle(color: Colors.black), // Texte en noir
          ),
        ],
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'OK',
            style: TextStyle(color: Colors.black), // Bouton avec texte en noir
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ],
    );
  }
}
