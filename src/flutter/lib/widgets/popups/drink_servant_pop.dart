import 'package:flutter/material.dart';

class DrinkServantPop extends StatelessWidget {
  final String userName;
  final int sips;
  final String masterName;
  final String masterPicture;

  DrinkServantPop({
    Key? key,
    required this.userName,
    required this.sips,
    required this.masterName,
    required this.masterPicture,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Servant Drink!', style: TextStyle(color: Colors.black)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('$masterName te partage ses gorgées', style: TextStyle(color: Colors.black)),
          SizedBox(height: 10),
          CircleAvatar(
            radius: 30,
            backgroundImage: NetworkImage(masterPicture),
          ),
          SizedBox(height: 10),
          Text('$userName, tu dois boire $sips gorgée(s)', style: TextStyle(color: Colors.black)),
        ],
      ),
      actions: [
        ElevatedButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text('OK'),
        ),
      ],
    );
  }
}
