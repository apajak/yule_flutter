import 'package:flutter/material.dart';

class ShifumiRulePop extends StatelessWidget {
  final String userName;
  final String profilePicture;
  final String gender;
  final VoidCallback onOkPressed;  // Ajout d'une fonction de callback

  ShifumiRulePop({
    Key? key,
    required this.userName,
    required this.profilePicture,
    required this.gender,
    required this.onOkPressed, // Ajout d'un paramètre pour la fonction de callback
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String text = '';
    if (gender == 'female'){
      text = userName + ', tu es partante pour un duel de SHI FU MI contre chaques joueurs ?';
    } else {
      text = userName + ', tu es partant pour un duel de SHI FU MI contre chaques joueurs ?';
    }
    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            'assets/images/vik.png', // shifumi image
            height: 100,
            width: 100,
          ),
          Text(
            'SHI FU MI',
            style: TextStyle(color: Colors.black), // Texte en noir
          ),
          Text(
            text,
            style: TextStyle(color: Colors.black), // Texte en noir
          ),
        ],
      ),
      actions: <Widget>[
        TextButton(
          child: Text(
            'OK',
            style: TextStyle(color: Colors.black), // Bouton avec texte en noir
          ),
          onPressed: () {
            onOkPressed(); // Appel de la fonction de callback
            Navigator.of(context).pop(); // Fermeture du popup
          },
        ),
      ],
    );
  }
}
