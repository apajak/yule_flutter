import 'package:flutter/material.dart';

class CardPilesWidget extends StatefulWidget {
  final double screenWidth;

  const CardPilesWidget({Key? key,
  required this.screenWidth,
  }) : super(key: key);

  @override
  CardPilesWidgetState createState() => CardPilesWidgetState();
}

class CardPilesWidgetState extends State<CardPilesWidget> with SingleTickerProviderStateMixin {
  late double cardWidth;
  late double cardHeight;
  late double slideEndPosition;
  static const Duration animationDuration = Duration(milliseconds: 800);

  late AnimationController _animationController;
  late Animation<double> _slideAnimation;
  late Animation<double> _flipAnimation;
  bool isFront = false;
  String currentCardImage = 'assets/images/cards/b1fv.png';
  List<Widget> rightPileCards = [];

  @override
  void initState() {
    super.initState();
    cardWidth = widget.screenWidth / 2 - 20;
    cardHeight = cardWidth * 1.4;
    slideEndPosition = (cardWidth) + 20; // Par exemple, déplacer la carte de la moitié de sa largeur

    _setupAnimation();
  }

  void _setupAnimation() {
    _animationController = AnimationController(
      duration: animationDuration,
      vsync: this,
    );

    _slideAnimation = Tween<double>(begin: 0.0, end: slideEndPosition).animate(_animationController);
    _flipAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(_animationController)
      ..addListener(() {
        setState(() {
          isFront = _flipAnimation.value > 0.5;
        });
        if (_flipAnimation.value == 1) {
          rightPileCards.add(_buildCardWidget(currentCardImage));
        }
      });
  }

  void setNextCard(String cardImage) {
    setState(() {
      currentCardImage = cardImage;
      if (_animationController.isCompleted) {
        _animationController.reset();
      }
      _animationController.forward();
    });
  }

  Widget _buildCardWidget(String cardImage) {
    return Positioned(
      child: SizedBox(
        width: cardWidth,
        height: cardHeight,
        child: Image.asset(cardImage, fit: BoxFit.cover),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double startPosition = 10;

    return Center(
      child: Container(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              left: startPosition,
              child: SizedBox(
                width: cardWidth,
                height: cardHeight,
                child: Image.asset('assets/images/cards/b1fv.png', fit: BoxFit.cover),
              ),
            ),
            Positioned(
              left: startPosition + slideEndPosition,
              child: Stack(
                children: rightPileCards,
              ),
            ),
            Positioned(
              left: startPosition + _slideAnimation.value,
              child: Transform(
                alignment: FractionalOffset.center,
                transform: Matrix4.identity()
                  ..setEntry(3, 2, 0.001)
                  ..rotateY(isFront ? (3.14 * (1 - _flipAnimation.value)) : (-3.14 * _flipAnimation.value)),
                child: SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: Image.asset(
                    isFront ? currentCardImage : 'assets/images/cards/b1fv.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
