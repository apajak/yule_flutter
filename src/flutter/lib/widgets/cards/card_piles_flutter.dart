import 'package:flutter/material.dart';
import 'package:playing_cards/playing_cards.dart';

class CardPilesWidgetFlutter extends StatefulWidget {
  final double screenWidth;

  const CardPilesWidgetFlutter({Key? key, required this.screenWidth}) : super(key: key);

  @override
  CardPilesWidgetFlutterState createState() => CardPilesWidgetFlutterState();
}

class CardPilesWidgetFlutterState extends State<CardPilesWidgetFlutter> with SingleTickerProviderStateMixin {
  late double cardWidth;
  late double cardHeight;
  late double slideEndPosition;
  static const Duration animationDuration = Duration(milliseconds: 800);

  late AnimationController _animationController;
  late Animation<double> _slideAnimation;
  late Animation<double> _flipAnimation;
  bool isFront = false;
  PlayingCard currentCard = PlayingCard(Suit.hearts, CardValue.ace); // Carte actuelle
  List<PlayingCard> rightPileCards = []; // Pile de cartes

  @override
  void initState() {
    super.initState();
    cardWidth = widget.screenWidth / 2 - 20;
    cardHeight = cardWidth * 1.4;
    slideEndPosition = (cardWidth) + 20; // Déplacement de la carte de la moitié de sa largeur

    _setupAnimation();
  }

  void _setupAnimation() {
    _animationController = AnimationController(duration: animationDuration, vsync: this);

    _slideAnimation = Tween<double>(begin: 0.0, end: slideEndPosition).animate(_animationController);
    _flipAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(_animationController)
      ..addListener(() {
        setState(() {
          isFront = _flipAnimation.value > 0.5;
        });
        if (_flipAnimation.value == 1) {
          rightPileCards.add(currentCard); // Ajoute la carte actuelle à la pile
        }
      });
  }

  void setNextCard(PlayingCard card) {
    setState(() {
      currentCard = card;
      if (_animationController.isCompleted) {
        _animationController.reset();
      }
      _animationController.forward();
    });
  }

  Widget _buildCardWidget(PlayingCard card) {
    return Positioned(
      child: SizedBox(
        width: cardWidth,
        height: cardHeight,
        child: PlayingCardView(card: card),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double startPosition = 10;

    return Center(
      child: Container(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              left: startPosition,
              child: SizedBox(
                width: cardWidth,
                height: cardHeight,
                child: PlayingCardView(card: PlayingCard(Suit.hearts, CardValue.ace)), // Exemple de carte
              ),
            ),
            Positioned(
              left: startPosition + slideEndPosition,
              child: Stack(
                children: rightPileCards.map((card) => _buildCardWidget(card)).toList(),
              ),
            ),
            Positioned(
              left: startPosition + _slideAnimation.value,
              child: Transform(
                alignment: FractionalOffset.center,
                transform: Matrix4.identity()
                  ..setEntry(3, 2, 0.001)
                  ..rotateY(isFront ? (3.14 * (1 - _flipAnimation.value)) : (-3.14 * _flipAnimation.value)),
                child: SizedBox(
                  width: cardWidth,
                  height: cardHeight,
                  child: PlayingCardView(card: isFront ? currentCard : PlayingCard(Suit.hearts, CardValue.ace)), // Carte à retourner
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
