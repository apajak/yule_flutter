import 'package:flutter/material.dart';

class ThumbButton extends StatelessWidget {
  final VoidCallback onPressed;

  const ThumbButton({Key? key, required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: onPressed,
      child: Text('Thumb'),
      backgroundColor: Colors.blue,
      shape: CircleBorder(),
    );
  }
}
