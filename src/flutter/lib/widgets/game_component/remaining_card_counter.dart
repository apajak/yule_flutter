import 'package:flutter/material.dart';

class RemainingCardsDisplay extends StatelessWidget {
  final int remainingCards;

  const RemainingCardsDisplay({Key? key, required this.remainingCards})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(Icons.loop, color: Colors.white), // Icône de votre choix
        SizedBox(width: 8), // Espace entre l'icône et le texte
        Text(
          '$remainingCards',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}
