import 'package:flutter/material.dart';

class CurrentPlayerDisplay extends StatelessWidget {
  final String currentPlayerName;

  const CurrentPlayerDisplay({Key? key, required this.currentPlayerName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(Icons.person, color: Colors.white), // Icône de votre choix
        SizedBox(width: 8), // Espace entre l'icône et le texte
        Text(
          '$currentPlayerName',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}