import 'dart:async';
import 'package:flutter/material.dart';

class PulsatingShifumiWidget extends StatefulWidget {
  final VoidCallback onAnimationEnd;

  PulsatingShifumiWidget({Key? key, required this.onAnimationEnd}) : super(key: key);

  @override
  _PulsatingShifumiWidgetState createState() => _PulsatingShifumiWidgetState();
}

class _PulsatingShifumiWidgetState extends State<PulsatingShifumiWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;
  Timer? _timer;
  int _currentIndex = 0;
  final List<String> _texts = ["Shi", "Fu", "Mi"];

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );
    _animation = Tween<double>(begin: 1.0, end: 1.2).animate(
      CurvedAnimation(parent: _animationController, curve: Curves.easeInOut),
    )..addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _animationController.reverse();
      }
    });
    _startTimer();
  }

  void _startTimer() {
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (_currentIndex < _texts.length - 1) {
        setState(() {
          _currentIndex++;
        });
      } else {
        _timer?.cancel();
        _animationController.dispose();
        widget.onAnimationEnd(); // Appeler le callback
      }
    });
    _animationController.repeat();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animation,
      builder: (context, child) {
        return Transform.scale(
          scale: _animation.value,
          child: Text(
            _texts[_currentIndex],
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          ),
        );
      },
    );
  }
}

