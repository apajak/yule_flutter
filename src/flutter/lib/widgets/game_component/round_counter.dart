import 'package:flutter/material.dart';

class RoundCounterDisplay extends StatelessWidget {
  final int totalRounds;
  final int currentRound;
  final Color color;// Couleur de l'icône et du texte
  final IconData iconData; // Ajout d'un paramètre pour l'icône

  const RoundCounterDisplay({
    Key? key,
    required this.totalRounds,
    required this.currentRound,
    required this.color,
    this.iconData = Icons.timelapse, // Valeur par défaut pour l'icône
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(iconData, color: color), // Utilisation de l'icône passée en paramètre
        SizedBox(width: 8), // Espace entre l'icône et le texte
        Text(
          '$currentRound / $totalRounds', // Affichage du nombre de rounds actuels et totaux
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: color),
        ),
      ],
    );
  }
}
