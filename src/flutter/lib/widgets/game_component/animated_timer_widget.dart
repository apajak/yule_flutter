import 'dart:async';
import 'package:flutter/material.dart';

class AnimatedTimerWidget extends StatefulWidget {
  final Duration duration;
  final VoidCallback onTimerEnd;
  final Color textColor;

  const AnimatedTimerWidget({
    Key? key,
    required this.duration,
    required this.onTimerEnd,
    this.textColor = Colors.red,
  }) : super(key: key);

  @override
  _AnimatedTimerWidgetState createState() => _AnimatedTimerWidgetState();
}

class _AnimatedTimerWidgetState extends State<AnimatedTimerWidget>
    with TickerProviderStateMixin {
  late AnimationController _pulseController;
  late AnimationController _progressController;
  late Animation<double> _pulseAnimation;
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    _pulseController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );

    _progressController = AnimationController(
      vsync: this,
      duration: widget.duration,
    )..forward();

    _pulseAnimation = Tween<double>(begin: 1.0, end: 1.5).animate(
      CurvedAnimation(parent: _pulseController, curve: Curves.easeInOut),
    )..addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _pulseController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        _pulseController.forward();
      }
    });


    _pulseController.forward();
    _startTimer();
  }

  void _startTimer() {
    _timer = Timer(widget.duration, () {
      _timer?.cancel();
      widget.onTimerEnd();
    });
  }

  @override
  void dispose() {
    _timer?.cancel();
    _pulseController.dispose();
    _progressController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        SizedBox(
          width: 100.0, // Ajuster la taille du cercle
          height: 100.0,
          child: AnimatedBuilder(
            animation: _progressController,
            builder: (context, child) {
              return CircularProgressIndicator(
                value: 1.0 - _progressController.value,
                strokeWidth: 8.0, // Ajuster l'épaisseur du cercle
                backgroundColor: Colors.grey,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
              );
            },
          ),
        ),
        AnimatedBuilder(
          animation: _pulseAnimation,
          builder: (context, child) {
            // Calcule le temps restant de manière descendante
            int remainingTime = widget.duration.inSeconds - (widget.duration.inSeconds * _progressController.value).round();
            return Transform.scale(
              scale: _pulseAnimation.value,
              child: Text(
                '$remainingTime', // Affiche le temps restant
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24.0,
                  color: widget.textColor,
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}