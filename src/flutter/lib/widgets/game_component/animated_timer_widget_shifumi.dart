import 'dart:async';
import 'package:flutter/material.dart';

class AnimatedTimerWidgetShifumi extends StatefulWidget {
  final VoidCallback onTimerEnd;

  const AnimatedTimerWidgetShifumi({
    Key? key,
    required this.onTimerEnd,
  }) : super(key: key);

  @override
  _AnimatedTimerWidgetShifumiState createState() => _AnimatedTimerWidgetShifumiState();
}

class _AnimatedTimerWidgetShifumiState extends State<AnimatedTimerWidgetShifumi>
    with TickerProviderStateMixin {
  late AnimationController _animationController;
  Timer? _timer;
  int _currentStep = 0; // 0: Timer, 1: Shi, 2: Fu, 3: Mi

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );

    _startSequence();
  }

  void _startSequence() {
    _timer = Timer(Duration(seconds: 3), () {
      setState(() {
        _currentStep = 1;
      });
      _animationController.forward();
    });

    _animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _animationController.reset();
        if (_currentStep < 3) {
          setState(() {
            _currentStep++;
          });
          _animationController.forward();
        } else {
          widget.onTimerEnd();
        }
      }
    });
  }

  @override
  void dispose() {
    _timer?.cancel();
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        String text;
        switch (_currentStep) {
          case 0:
            text = '';
            break;
          case 1:
            text = 'Shi';
            break;
          case 2:
            text = 'Fu';
            break;
          case 3:
            text = 'Mi';
            break;
          default:
            text = '';
        }
        double scale = 1.0 + (0.5 * _animationController.value);
        return Transform.scale(
          scale: scale,
          child: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 24.0,
              color: Colors.black,
            ),
          ),
        );
      },
    );
  }
}
