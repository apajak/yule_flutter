import 'package:flutter/material.dart';
import 'package:yule_flutter/widgets/alerts/base/animated_alert.dart';

class ErrorAnimatedAlert extends StatefulWidget {
  final String title;
  final String text;
  final IconData? iconData;
  final VoidCallback? onClose;

  ErrorAnimatedAlert(
      {required this.title, required this.text, this.onClose, this.iconData});

  @override
  _ErrorAnimatedAlertState createState() => _ErrorAnimatedAlertState();
}

class _ErrorAnimatedAlertState extends State<ErrorAnimatedAlert> {
  Color backgroundColor = Colors.red; // Default background color

  @override
  Widget build(BuildContext context) {
    return BaseAnimatedAlert(
      title: widget.title,
      text: widget.text,
      backgroundColor: backgroundColor,
      iconData: widget.iconData,
      onClose: widget.onClose,
    );
  }
}
