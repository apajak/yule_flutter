import 'dart:async';
import 'package:flutter/material.dart';

class BaseAnimatedAlert extends StatefulWidget {
  final String title;
  final String text;
  final Color backgroundColor;
  final TextStyle titleTextStyle;
  final TextStyle textTextStyle;
  final IconData? iconData; // Icon data (optional)
  final VoidCallback? onClose; // Callback for closing the alert

  BaseAnimatedAlert({
    required this.title,
    required this.text,
    this.backgroundColor = Colors.grey, // Default background color
    this.titleTextStyle = const TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 18,
      color: Colors.white, // Title text color
    ),
    this.textTextStyle = const TextStyle(
      fontSize: 16,
      color: Colors.white, // Text color
    ),
    this.iconData, // Optional icon
    this.onClose, // Callback for closing the alert
  });

  @override
  _BaseAnimatedAlertState createState() => _BaseAnimatedAlertState();
}

class _BaseAnimatedAlertState extends State<BaseAnimatedAlert> {
  bool isVisible = false;
  bool hideImmediately = false;
  Completer<void> delayedOperationCompleter =
      Completer<void>(); // Add a Completer

  @override
  void initState() {
    super.initState();
    // Show the alert with an animation when the widget is built
    WidgetsBinding.instance.addPostFrameCallback((_) {
      toggleVisibility();
    });
  }

  void toggleVisibility() {
    setState(() {
      isVisible = !isVisible;
    });

    // Set shouldShow to true when you want to show the alert again
    if (isVisible) {
      delayedOperationCompleter = Completer<void>(); // Initialize Completer
      Timer(const Duration(seconds: 3), () {
        if (!delayedOperationCompleter.isCompleted) {
          toggleVisibility();
          delayedOperationCompleter.complete(); // Complete the Completer
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: isVisible ? 1.0 : 0.0,
      duration: Duration(milliseconds: 500),
      curve: Curves.easeInOut,
      child: Stack(
        children: [
          _AnimatedAlert(
            title: widget.title,
            text: widget.text,
            backgroundColor: widget.backgroundColor,
            titleTextStyle: widget.titleTextStyle,
            textTextStyle: widget.textTextStyle,
            iconData: widget.iconData,
          ),
          if (widget.onClose != null)
            Positioned(
              top: 8,
              right: 8,
              child: IconButton(
                icon: Icon(Icons.close),
                color: Colors.white,
                onPressed: () {
                  setState(() {
                    isVisible =
                        false; // Set isVisible to false to hide the alert immediately
                  });
                  if (!delayedOperationCompleter.isCompleted) {
                    delayedOperationCompleter
                        .complete(); // Complete the Completer to cancel the delayed operation
                  }
                },
              ),
            ),
        ],
      ),
    );
  }
}

class _AnimatedAlert extends StatelessWidget {
  final String title;
  final String text;
  final Color backgroundColor;
  final TextStyle titleTextStyle;
  final TextStyle textTextStyle;
  final IconData? iconData;

  const _AnimatedAlert({
    required this.title,
    required this.text,
    required this.backgroundColor,
    required this.titleTextStyle,
    required this.textTextStyle,
    this.iconData,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius:
            BorderRadius.circular(16), // Adjust the border radius as needed
      ),
      elevation: 5, // Add shadow
      color: backgroundColor,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (iconData != null)
              Row(
                children: [
                  Icon(
                    iconData,
                    color: Colors.white, // Icon color
                    size: 24, // Icon size
                  ),
                  const SizedBox(width: 8),
                  Text(
                    title,
                    style: titleTextStyle,
                  ),
                ],
              ),
            if (iconData == null)
              Text(
                title,
                style: titleTextStyle,
              ),
            const SizedBox(height: 8),
            Text(
              text,
              style: textTextStyle,
            ),
          ],
        ),
      ),
    );
  }
}
