# Usage of Alerts
## Properties:
- title : String
- text : String
- iconData : IconData (Optional. if not provided alert have no icon.)
- onClose : Function (Optional. if not provided alert have close button.)

## 1. Import

```dart
import 'package:yule_flutter/widgets/alerts/index.dart';
```

## 2. Usage
### BaseAnimatedAlert :
``` Dart
BaseAnimatedAlert(
      title: 'Title',
      text: 'There is a text here.',
      iconData: Icons.question_mark,
      onClose: () {
        print('Alert closed.');
      },
    ),
```
### InfoAnimatedAlert :

``` Dart
InfoAnimatedAlert(
    title: "Somthing Happened",
    text: "There is a text here.",
    onClose: () {
      print('Alert closed.');
    },
    iconData: Icons.info_outline_rounded),
```

### ErrorAnimatedAlert :

``` Dart
ErrorAnimatedAlert(
    title: "Wrong Password",
    text:
        "The password you entered is incorrect. Please try again.",
    onClose: () {
      print('Alert closed.');
    },
    iconData: Icons.error_outline_rounded),
```
### SuccessAnimatedAlert :

``` Dart
SuccessAnimatedAlert(
    title: "Welcome",
    text: "You are now logged in.",
    onClose: () {
      print('Alert closed.');
    },
    iconData: Icons.check_circle_outline_rounded)
```