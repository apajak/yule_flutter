import 'package:flutter/material.dart';
import 'package:yule_flutter/widgets/alerts/base/animated_alert.dart';

class InfoAnimatedAlert extends StatefulWidget {
  final String title;
  final String text;
  final IconData? iconData; // Add the iconData parameter
  final VoidCallback? onClose;

  InfoAnimatedAlert({
    required this.title,
    required this.text,
    this.onClose,
    this.iconData, // Add the iconData parameter
  });

  @override
  _InfoAnimatedAlertState createState() => _InfoAnimatedAlertState();
}

class _InfoAnimatedAlertState extends State<InfoAnimatedAlert> {
  Color backgroundColor = Colors.blueGrey; // Default background color

  @override
  Widget build(BuildContext context) {
    return BaseAnimatedAlert(
      title: widget.title,
      text: widget.text,
      backgroundColor: backgroundColor,
      iconData: widget.iconData, // Pass the iconData to BaseAnimatedAlert
      onClose: widget.onClose,
    );
  }
}
