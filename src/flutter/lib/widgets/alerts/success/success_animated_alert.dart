import 'package:flutter/material.dart';
import 'package:yule_flutter/widgets/alerts/base/animated_alert.dart';

class SuccessAnimatedAlert extends StatefulWidget {
  final String title;
  final String text;
  final IconData? iconData; // Change Icons to IconData
  final VoidCallback? onClose;

  SuccessAnimatedAlert({
    required this.title,
    required this.text,
    this.onClose,
    this.iconData, // Add the iconData parameter
  });

  @override
  _SuccessAnimatedAlertState createState() => _SuccessAnimatedAlertState();
}

class _SuccessAnimatedAlertState extends State<SuccessAnimatedAlert> {
  Color backgroundColor = Colors.green; // Default background color

  @override
  Widget build(BuildContext context) {
    return BaseAnimatedAlert(
      title: widget.title,
      text: widget.text,
      backgroundColor: backgroundColor,
      iconData: widget.iconData,
      onClose: widget.onClose,
    );
  }
}
