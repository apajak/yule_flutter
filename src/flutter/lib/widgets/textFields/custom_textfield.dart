import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String labelText;
  final TextEditingController controller;
  final bool obscureText;

  const CustomTextField({
    super.key,
    required this.labelText,
    required this.controller,
    this.obscureText = false,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      obscureText: obscureText,
      decoration: InputDecoration(
        labelText: labelText,
        fillColor: Theme.of(context).highlightColor,
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Theme.of(context).highlightColor)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Theme.of(context).highlightColor)),
      ),
      style: TextStyle(color: Theme.of(context).highlightColor),
    );
  }
}

// Usage:
//CustomTextField(
//labelText: "Enter your name",
//controller: nameController,
//)
