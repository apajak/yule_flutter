import 'package:flutter/material.dart';

class BirthdatePickerWidget extends StatefulWidget {
  final ValueNotifier<DateTime?> selectedDateNotifier;

  BirthdatePickerWidget({required this.selectedDateNotifier});

  @override
  _BirthdatePickerWidgetState createState() => _BirthdatePickerWidgetState();
}

class _BirthdatePickerWidgetState extends State<BirthdatePickerWidget> {
  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = (await showDatePicker(
      context: context,
      initialDate: widget.selectedDateNotifier.value ?? DateTime.now(),
      firstDate: DateTime(1900), // Adjust the start date as needed
      lastDate: DateTime.now(),
    ))!;
    if (picked != widget.selectedDateNotifier.value) {
      setState(() {
        widget.selectedDateNotifier.value = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Birthdate',
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 8),
        GestureDetector(
          onTap: () => _selectDate(context),
          child: Container(
            padding: EdgeInsets.all(12),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(4),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  widget.selectedDateNotifier.value != null
                      ? "${widget.selectedDateNotifier.value!.day}/${widget.selectedDateNotifier.value!.month}/${widget.selectedDateNotifier.value!.year}"
                      : "Select your birthdate",
                  style: TextStyle(fontSize: 16),
                ),
                Icon(Icons.calendar_today),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
