import 'package:flutter/material.dart';
import 'package:yule_flutter/widgets/popups/drink_level_pop.dart';
import 'package:yule_flutter/models/user.dart';
import 'package:yule_flutter/services/socketio_service.dart';

class AlcoholLevelWidget extends StatelessWidget {
  final SimpleUser player;
  final User? currentUser;
  final SocketIoService socketIoService;

  AlcoholLevelWidget({
    Key? key,
    required this.player,
    required this.currentUser,
    required this.socketIoService,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isCurrentUser = currentUser?.userUuid == player.userUuid;

    return GestureDetector(
      onTap: isCurrentUser ? () => _showAlcoholLevelPopup(context) : null,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: List.generate(5, (index) {
          return Icon(
            index < player.gameAlcohol ? Icons.star : Icons.star_border,
            color: index < player.gameAlcohol ? Colors.yellow : Colors.grey,
          );
        }),
      ),
    );
  }

  void _showAlcoholLevelPopup(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return DrinkLevelPopup(
          initialLevel: player.gameAlcohol,
          onValidateSelection: (int level) {
            socketIoService.sendGameAlcoholLevel(level);
          },
        );
      },
    );
  }
}
