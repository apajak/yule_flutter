import 'package:flutter/material.dart';

class RightIconButtonS extends StatelessWidget {
  final IconData icon;
  final String text;
  final VoidCallback onPressed;

  RightIconButtonS(
      {super.key,
      required this.icon,
      required this.text,
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100, // Fixed width
      height: 60, // Fixed height
      margin:
          const EdgeInsets.symmetric(vertical: 10), // Margin on top and bottom
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          foregroundColor: Colors.white, backgroundColor: Colors.green, // Change the text color to white
          padding: const EdgeInsets.symmetric(
              vertical: 16, horizontal: 16), // Adjust padding for size
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              text,
              style: const TextStyle(fontSize: 20), // Adjust text size
            ),
            const SizedBox(width: 10), // Add spacing between icon and text
            Icon(icon), // Icon on the right
          ],
        ),
      ),
    );
  }
}
