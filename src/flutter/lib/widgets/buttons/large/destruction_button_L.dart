import 'package:flutter/material.dart';

class DestructionButtonL extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;

  DestructionButtonL({required this.text, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300, // Fixed width
      height: 60, // Fixed height
      margin: EdgeInsets.symmetric(vertical: 10), // Margin on top and bottom
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          foregroundColor: Colors.white, backgroundColor: Colors
              .red, // Change the text color to white
          padding: EdgeInsets.symmetric(
              vertical: 16, horizontal: 24), // Adjust padding for size
          textStyle: TextStyle(fontSize: 20), // Adjust text size
        ),
        child: Text(text),
      ),
    );
  }
}
