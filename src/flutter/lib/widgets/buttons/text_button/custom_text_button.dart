import 'package:flutter/material.dart';

class CustomTextButton extends StatelessWidget {
  final String text;
  final VoidCallback? onPressed;

  const CustomTextButton({
    super.key,
    required this.text,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      style: _buttonStyle, // Use the static button style
      child: Text(
        text,
        style: _textStyle, // Use the static text style
      ),
    );
  }

  static final ButtonStyle _buttonStyle = ButtonStyle(
    backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
    padding: MaterialStateProperty.all<EdgeInsetsGeometry>(EdgeInsets.zero),
    overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
  );

  static const TextStyle _textStyle = TextStyle(
    color: Colors.blue, // Customize text color here
    fontSize: 16, // Customize text size here
  );
}
