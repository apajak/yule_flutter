import 'package:flutter/material.dart';

class LeftIconButtonM extends StatelessWidget {
  final IconData icon;
  final String text;
  final VoidCallback onPressed;
  final String badgeText;
  final bool showBadge;

  LeftIconButtonM({
    required this.icon,
    required this.text,
    required this.onPressed,
    this.badgeText = '!',
    this.showBadge = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 60,
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Stack(
        alignment: Alignment.center,
        children: [
          ElevatedButton(
            onPressed: onPressed,
            style: ElevatedButton.styleFrom(
              foregroundColor: Colors.white, backgroundColor: Colors.green,
              padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(icon),
                const SizedBox(width: 10),
                Text(text, style: const TextStyle(fontSize: 20)),
              ],
            ),
          ),
          if (showBadge)
            Positioned(
              right: 10,
              top: 10,
              child: Container(
                padding: EdgeInsets.all(2),
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(12),
                ),
                constraints: BoxConstraints(
                  minWidth: 24,
                  minHeight: 24,
                ),
                child: Text(
                  badgeText,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
