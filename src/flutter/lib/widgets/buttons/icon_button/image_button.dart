import 'package:flutter/material.dart';

class ImageButton extends StatelessWidget {
  final Widget icon; // Custom icon, which can be an image, a flag, etc.
  final String text;
  final VoidCallback onPressed;

  ImageButton({
    required this.icon,
    required this.text,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity, // Take the full width available
      height: 60, // Fixed height
      margin:
          const EdgeInsets.symmetric(vertical: 10), // Margin on top and bottom
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          foregroundColor: Colors.white, backgroundColor: Theme.of(context).highlightColor, // Change the text color to white
          padding: const EdgeInsets.symmetric(
              vertical: 16, horizontal: 16), // Adjust padding for size
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            icon, // Icon on the left
            Text(
              text,
              style: const TextStyle(fontSize: 20), // Adjust text size
            ),
            Opacity(
                opacity: 0, child: icon), // Invisible icon to balance the row
          ],
        ),
      ),
    );
  }
}
