import 'package:flutter/material.dart';

class CircularIconButton extends StatelessWidget {
  final IconData icon;
  final VoidCallback onPressed;

  CircularIconButton({required this.icon, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80, // Fixed width for a circular button
      height: 80, // Fixed height for a circular button
      margin: EdgeInsets.symmetric(vertical: 10), // Margin on top and bottom
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          foregroundColor: Colors.white, backgroundColor: Theme.of(context).highlightColor, shape: CircleBorder(), // Change the text/icon color to white
          alignment: Alignment.center, // Center the button's content
        ),
        child: Icon(icon), // Display the icon at the center
      ),
    );
  }
}
