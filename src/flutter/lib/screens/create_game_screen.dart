import 'package:flutter/material.dart';
import 'package:yule_flutter/services/api_service.dart';
import 'package:yule_flutter/models/user.dart';
import 'dart:async';
import 'package:yule_flutter/services/socketio_service.dart';
import 'package:yule_flutter/models/flutter_secure_storage.dart';
import '../widgets/images/profile_pic_small.dart';
import '../widgets/pickers/alcohol_level_widget.dart';
// Import your SocketIoService

class CreateGameScreen extends StatefulWidget {
  @override
  _CreateGameScreenState createState() => _CreateGameScreenState();
}

class _CreateGameScreenState extends State<CreateGameScreen> {
  // late Future<Map<String, dynamic>> gameData;
  final apiService = ApiService();
  final socketIoService = SocketIoService();
  late String identifier;
  Timer? _debounce;
  TextEditingController searchController = TextEditingController();
  List<SimpleUser> friends = [];
  List<SimpleUser> searchResults = [];
  List<SimpleUser> joinedPlayers = []; // Dummy list for players who joined
  bool isQueryInProgress = false;
  FocusNode searchFocusNode = FocusNode();
  bool isMaster = true;
  // load current user
  final storage = UserDataStorage();
  User? currentUser;

  // Method to get all friends
  Future<void> _fetchFriends() async {
    try {
      final response = await apiService.getFriends();
      print("response" + response.toString());
      print(response['users']);
      if (response['value']) {
        List<SimpleUser> userList = (response['users'] as List)
            .map((userData) => SimpleUser.fromJson(userData))
            .toList();
        if (mounted) {
          setState(() {
            friends = userList;
          });
        }
      } else {
        print('Failed to fetch users');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  void dispose() {
    _debounce?.cancel();
    searchController.dispose();
    searchFocusNode.dispose(); // Dispose the focus node
    socketIoService.disconnect();
    super.dispose();
  }

  void _showSnackBar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      duration: Duration(seconds: 3),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  // Method to send a game invite
  Future<void> _sendGameInvite(String friendUuid) async {
    try {
      var response = await apiService.sendGameInvite(identifier, friendUuid);
      if (response['value']) {
        _showSnackBar('Game invitation sent successfully');
      } else {
        _showSnackBar('Failed to send friend request: ${response['error']}');
      }
    } catch (error) {
      _showSnackBar('Error sending friend request: $error');
    }
  }

  @override
  void initState() {
    super.initState();
    identifier = "y04oDOnl"; // Test server identifier
    _connectToTestServer();
    _loadCurrentUser();
    _fetchFriends(); // Just fetch the friends without setting searchResults
    searchFocusNode.addListener(() {
      if (searchFocusNode.hasFocus && searchController.text.isEmpty) {
        setState(() => searchResults = friends);
      } else if (!searchFocusNode.hasFocus) {
        setState(() => searchResults = []);
      }
    });
    print("Game data future initialized");
  }
  void _loadCurrentUser() async {
    currentUser = await storage.getUserData();
    if (currentUser != null) {
      // Now you have the current user data, you can use it in your UI
      setState(() {}); // Call setState to update the UI
    }
  }

  // TEST SERVER CODE
  void _connectToTestServer() {
    // Initialize and connect to the test server
    socketIoService.createSocketConnection('ws://10.0.2.2:3000');
    socketIoService.connect();
    // Setup listener for player list updates
    socketIoService.on('player_list_update', (data) {
      print("Player list update received: $data");
      // Update the player list based on the data received from the server
      setState(() {
        joinedPlayers = List<SimpleUser>.from(
            data.map((item) => SimpleUser.fromJson(item)));
      });
    });
  }

  //----------------

  Future<bool> _onWillPop() async {
    // Call the API to delete the game
/*    try {
      await apiService.deleteGame(identifier);
      print("Game deleted successfully");
    } catch (e) {
      print("Failed to delete game: $e");
    }*/
    return true; // Return true to allow the pop operation
  }

  void _searchFriends(String query) {
    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      setState(() {
        if (query.isEmpty && searchFocusNode.hasFocus) {
          searchResults = friends;
        } else {
          searchResults = friends
              .where((friend) =>
                  friend.userName.toLowerCase().contains(query.toLowerCase()))
              .toList();
        }
      });
    });
  }

  void _kickPlayer(String userUuid) {
    socketIoService.kickPlayer(userUuid);
  }

  void _startGame() {
    socketIoService.startGame();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Create Game")),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text("Game Identifier: $identifier"),
            SizedBox(height: 20),
            TextField(
              controller: searchController,
              focusNode: searchFocusNode,
              onChanged: _searchFriends,
              decoration: InputDecoration(
                labelText: "Search friends to invite",
                suffixIcon: Icon(Icons.search),
              ),
            ),
            Expanded(
              child: searchResults.isNotEmpty
                  ? ListView.builder(
                      itemCount: searchResults.length,
                      itemBuilder: (context, index) {
                        final friend = searchResults[index];
                        return Card(
                          color: Theme.of(context).highlightColor,
                          child: ListTile(
                            title: Text(friend.userName),
                            leading: SmallProfilePicture(
                              imageUrl: friend.profilePicture!,
                              size: 40.0,
                            ),
                            trailing: ElevatedButton(
                              onPressed: () => _sendGameInvite(friend.userUuid),
                              child: Text('Invite'),
                            ),
                          ),
                        );
                      },
                    )
                  : Container(), // Display an empty container when no results
            ),
            Expanded(
              child: ListView.builder(
                itemCount: joinedPlayers.length,
                itemBuilder: (context, index) {
                  final player = joinedPlayers[index];
                  bool isCurrentUser = currentUser?.userUuid == player.userUuid;

                  return Card(
                    color: Theme.of(context).highlightColor,
                    child: ListTile(
                      title: Text(player.userName),
                      leading: SmallProfilePicture(
                        imageUrl: player.profilePicture ?? '', // Gérer les cas de null
                        size: 40.0,
                      ),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          AlcoholLevelWidget(
                            player: player,
                            currentUser: currentUser,
                            socketIoService: socketIoService,
                          ),
                          SizedBox(width: 8),
                          // Bouton Kick si l'utilisateur est le maître du jeu et que le joueur n'est pas l'utilisateur actuel
                          if (isMaster && !isCurrentUser)
                            ElevatedButton(
                              onPressed: () => _kickPlayer(player.userUuid),
                              child: Text('Kick'),
                            ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
              ElevatedButton(
              onPressed: () {
                // redirect to /barbu
                _startGame();
                Navigator.of(context).pushNamed('/barbu');
              },
              child: Text('Start Game'),
            ),
          ],
        ),
      ),
    );
  }
}
