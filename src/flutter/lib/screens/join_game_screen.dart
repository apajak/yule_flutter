import 'package:flutter/material.dart';
import 'package:yule_flutter/models/flutter_secure_storage.dart';
import 'package:yule_flutter/widgets/buttons/base/custom_button.dart';
import '../../models/user.dart';
import '../widgets/images/profile_pic_large.dart';
import '../widgets/textFields/custom_textfield.dart';
import 'package:yule_flutter/services/api_service.dart';

import 'lobby_screen.dart';

class JoinGameScreen extends StatefulWidget {
  final String? gameIdentifier;

  JoinGameScreen({Key? key, this.gameIdentifier}) : super(key: key);

  @override
  _JoinGameScreenState createState() => _JoinGameScreenState();
}

class _JoinGameScreenState extends State<JoinGameScreen> {
  late String identifier;
  User? user;
  String? imageUrl;
  final apiService = ApiService();
  final TextEditingController identifierController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadUserData();
    identifier = widget.gameIdentifier ?? '';
    identifierController.text = "y04oDOnl";
        //identifier; // Set the identifier in the text controller
  }

  Future<bool> _onWillPop() async {
    // Logic to call the API to leave the game can be added here
    return true; // Return true to allow the pop operation
  }

  void _loadUserData() async {
    // Fetch the user data from secure storage
    final storage = UserDataStorage();
    user = await storage.getUserData();
    if (user != null) {
      // Update the UI with the user's data
      setState(() {
        imageUrl = user!.profilePicture;
      });
    }
  }

  void _joinGame() async {
    // Retrieve the game identifier from the text field
    String gameIdentifier = identifierController.text;

    // Call the API to get the game server URL
    var response = await apiService.getGameServerUrl(gameIdentifier);
    if (response['value'] == true) {
      String gameServerUrl = response['url'];
      print("Game server URL: $gameServerUrl");

      // Navigate to the LobbyScreen with the identifier and server URL
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => LobbyScreen(
          gameIdentifier: gameIdentifier,
          gameServerUrl: gameServerUrl,
        ),
      ));
    } else {
      // Handle the error scenario, maybe show an alert dialog with the error message
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Error'),
          content: Text(response['error']),
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('OK'),
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Join Game"),
        ),
        body: Padding(
          padding: EdgeInsets.all(50.0),
          child: Column(
            children: [
              ProfilePicture_L(
                imageUrl: imageUrl,
              ),
              SizedBox(height: 30.0),
              Text("JOIN GAME"),
              SizedBox(height: 30.0),
              CustomTextField(
                labelText: "Enter game identifier",
                controller: identifierController,
              ),
              SizedBox(height: 30.0),
              CustomButton(
                text: "JOIN GAME",
                onPressed:
                    _joinGame, // Call the _joinGame method when the button is pressed
              ),
              // Other UI elements and logic related to joining the game
            ],
          ),
        ),
      ),
    );
  }
}
