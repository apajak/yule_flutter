import 'package:flutter/material.dart';

// Import your ImagePickerWidget

class WidgetTestScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Widget Test Screen'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Welcome to your widget testing screen!',
              style: TextStyle(fontSize: 18),
            ),
            // Add your new widgets for testing here
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
