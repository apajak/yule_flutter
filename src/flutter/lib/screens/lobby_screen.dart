import 'package:flutter/material.dart';
import 'package:yule_flutter/models/user.dart';
import 'package:yule_flutter/services/socketio_service.dart';
import 'package:yule_flutter/widgets/pickers/alcohol_level_widget.dart';
import '../models/flutter_secure_storage.dart';
import '../widgets/images/profile_pic_small.dart';
// Import your ProfilePicSmall widget


class LobbyScreen extends StatefulWidget {
  final String gameIdentifier;
  final String gameServerUrl;

  LobbyScreen(
      {Key? key, required this.gameIdentifier, required this.gameServerUrl})
      : super(key: key);

  @override
  _LobbyScreenState createState() => _LobbyScreenState();
}
// y04oDOnl
class _LobbyScreenState extends State<LobbyScreen> {
  final storage = UserDataStorage();
  final socketIoService = SocketIoService(); // Récupération de l'instance singleton
  User? currentUser;
  List<SimpleUser> joinedPlayers = []; // List for players who joined
  bool isConnected = false;

  void setupGameStartListener() {
    socketIoService.on('game_started', (data) {
      Navigator.of(context).pushNamed('/barbu');
    });
  }

  @override
  void initState() {
    super.initState();
    initializeSocketConnection();
    setupGameStartListener();
    loadCurrentUser(); // Appel de la méthode pour charger l'utilisateur actuel
  }

  void loadCurrentUser() async {
    currentUser = await storage.getUserData();
    if (mounted) {
      setState(() {});
    }
  }

  void initializeSocketConnection() {
    socketIoService.createSocketConnection(widget.gameServerUrl);
    socketIoService.connect();

    socketIoService.on('player_list_update', (data) {
      print('player_list_update: $data');
      setState(() {
        joinedPlayers = List<SimpleUser>.from(
            data.map((item) => SimpleUser.fromJson(item)));
        isConnected = true;
      });
    });

    // Add listener for 'you_are_kicked' event
    socketIoService.on('you_are_kicked', (data) {
      Navigator.of(context).pushReplacementNamed('/home'); // Redirect to home
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              'Kicked',
              style: TextStyle(color: Colors.black), // Set the text color to black
            ),
            content: Text(
              'You have been kicked from the game.',
              style: TextStyle(color: Colors.black), // Set the text color to black
            ),
            actions: [
              TextButton(
                child: Text(
                  'OK',
                  style: TextStyle(color: Colors.black), // Optionally set button text color to black
                ),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          );

        },
      );
    });
  }


  @override
  void dispose() {
    socketIoService.disconnect();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Game Lobby: ${widget.gameIdentifier}')),
      body: isConnected
          ? Column(
        children: [
          // some space between the top and the list
          SizedBox(height: 20),
          Text('Players joined:'),
          SizedBox(height: 20),
          ListView.builder(
            shrinkWrap: true,
            itemCount: joinedPlayers.length,
            itemBuilder: (context, index) {
              final player = joinedPlayers[index];
              return Card(
                color: Theme.of(context).highlightColor,
                child: ListTile(
                  title: Text(player.userName),
                  leading: SmallProfilePicture(
                    imageUrl: player.profilePicture!,
                    size: 40.0,
                  ),
                  trailing: AlcoholLevelWidget(
                    player: player,
                    currentUser: currentUser,
                    socketIoService: socketIoService,
                  ),
                ),
              );
            },
          ),

        ],
      )
          : Center(child: CircularProgressIndicator()),
    );
  }
}
