import 'dart:async';
import 'package:flutter/material.dart';
import 'package:yule_flutter/widgets/cards/card_piles.dart';
import 'package:yule_flutter/widgets/images/profile_pic_small.dart';
import 'package:yule_flutter/models/user.dart'; // Modèle utilisateur
import 'package:yule_flutter/services/socketio_service.dart';
import '../../models/flutter_secure_storage.dart';
import '../../widgets/game_component/current_player_name_indicator.dart';
import '../../widgets/game_component/game_message_widget.dart';
import '../../widgets/game_component/remaining_card_counter.dart';
import '../../widgets/game_component/thumb_master_button.dart';
import '../../widgets/images/profile_pic_bordered.dart';
import '../../widgets/popups/custom_rule_pop.dart';
import '../../widgets/popups/drink_pop.dart';
import '../../widgets/popups/drink_servant_pop.dart';
import '../../widgets/popups/select_player_pop.dart';
import 'package:collection/collection.dart';
import '../../widgets/popups/select_single_player_pop.dart';
import '../../widgets/popups/select_thumb_pop.dart';
import '../../widgets/popups/shifumi_pop.dart';
import '../../widgets/popups/thumb_master_drink_pop.dart';
import '../../widgets/popups/thumb_master_pop.dart';
import 'barbu_screen_shifumi.dart';

class BarbuScreen extends StatefulWidget {
  const BarbuScreen({Key? key}) : super(key: key);

  @override
  _BarbuScreenState createState() => _BarbuScreenState();
}


class _BarbuScreenState extends State<BarbuScreen> {
  List<String> cardImages = [];
  List<SimpleUser> players = []; // Liste des utilisateurs
  final socketIoService = SocketIoService(); // Récupération de l'instance singleton
  final GlobalKey<CardPilesWidgetState> cardPilesKey = GlobalKey();
  late User? user;
  int currentPlayerIndex = 0;
  String gameMessage = "";
  String currentPlayerName = '';
  String currentCard = '';
  String currentPlayerId = '';
  String currentCategory = '';
  String currentRule = '';
  String gameState = '';
  bool isThumbMaster = false;
  int remainingCards = 0;
  var isMyTurn = false;

  @override
  void initState() {
    super.initState();
    _loadUserData();
    generateCardImages();
    fetchUsers();
    setupGameEventListeners();
    socketIoService.getFirstTurn();
  }
  void _loadUserData() async {
    // Fetch the user data from secure storage
    final storage = UserDataStorage();
    var userData = await storage.getUserData();
    setState(() {
      user = userData;
    });
  }

  void updateGameMessage(String newMessage) {
    setState(() {
      gameMessage = newMessage;
    });
  }

  void setupGameEventListeners() {

    // Add listener for 'game_state_update' event
    socketIoService.onGameStateUpdate = (data) {
      print("Game state updated: $data");
      if (mounted) {
        setState(() {
          currentPlayerIndex = data['current_turn'];
          players = List<SimpleUser>.from(
              data['players'].map((item) => SimpleUser.fromJson(item)));
          currentPlayerName = players[currentPlayerIndex].userName;
          currentCard = data['current_card'] ?? '';
          print("Current card: $currentCard");
          currentCategory = data['current_category'] ?? '';
          currentPlayerId = players[currentPlayerIndex].userUuid;
          print("Current player id: $currentPlayerId");
          currentRule = data['current_rule'] ?? '';
          gameState = data['game_state'];
          if (user != null) {
            isMyTurn = currentPlayerId == user!.userUuid;
          }
          remainingCards = data['remaining_cards'];
        });
      }
    };

    socketIoService.broadcastFirstTurn = (data) {
      print("Broadcast first turn: $data");
      var playerUuid = data['current_player_uuid'];
      var playerName = data['current_player_name'];
      setState(() {
        currentPlayerName = playerName;
        currentPlayerId = playerUuid;
        isMyTurn = currentPlayerId == user!.userUuid;
      });
      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      if (isMyTurn){
        if (player.gender == 'female'){
          updateGameMessage("C'est à toi de débuter la partie !");
        } else {
          updateGameMessage("C'est à toi de débuter la partie !");
        }
      } else {
        if (player.gender == 'female'){
          updateGameMessage("C'est à " + player.userName + " de débuter la partie !");
        } else {
          updateGameMessage("C'est à " + player.userName + " de débuter la partie !");
        }
      }
    };

    // Add listener for 'select_player_broadcast' event
    socketIoService.onSelectPlayerBroadcast = (data) {
      print("Select Player Broadcast: $data");
      var playerUuid = data['playerUuid'];
      var sips = data['sips'];
      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      if (player.gender == 'female'){
        updateGameMessage(player.userName + " distribue " + sips.toString() + " gorgées");
      } else {
        updateGameMessage(player.userName + " distribue " + sips.toString() + " gorgées");
      }
    };

    // Add listener for 'select_player' event
    socketIoService.onSelectPlayerEvent = (data) {
      print("Select Player Event: $data");
      var playerUuid = data['playerUuid'] as String;
      var sips = data['sips'] as int;
      print("data: $data");

      SimpleUser? player = players.firstWhereOrNull((p) => p.userUuid == playerUuid);
      if (player == null) {
        print("Player not found");
        return;
      }
      List<String> suggestedPlayersUuids = [];
      List<String> otherPlayersUuids = [];
      List<SimpleUser>suggestedPlayersList = [];
      List<SimpleUser>otherPlayersList = [];

      // get uuids of suggested players
      if (data['suggestedPlayers'] != null) {
        suggestedPlayersUuids = List<String>.from(data['suggestedPlayers']);
      }
      // get uuids of other players
      if (data['otherPlayers'] != null) {
        otherPlayersUuids = List<String>.from(data['otherPlayers']);
      }

      // get the player from the uuid from players list
      suggestedPlayersList = players.where((p) => suggestedPlayersUuids.contains(p.userUuid)).toList();
      otherPlayersList = players.where((p) => otherPlayersUuids.contains(p.userUuid)).toList();

      if (suggestedPlayersList.isEmpty && otherPlayersList.isEmpty) {
        print("Aucun joueur disponible pour la sélection.");
        return;
      }

      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return SelectPlayerPop(
            userName: player.userName,
            gender: player.gender,
            profilePicture: player.profilePicture ?? 'default_image_url',
            sips: sips,
            suggestedPlayers: suggestedPlayersList,
            otherPlayers: otherPlayersList,
            onValidateSelection: (List<String> uuids, List<int> sips) {
              socketIoService.sendSelectedPlayers(uuids, sips);
            },
          );
        },
      );
    };


    // Add listener for 'count_sips_broadcast' event
    socketIoService.onCountSipsBroadcast = (data) {
      print("Count Sips Broadcast: $data");
    };

    // Add listener for 'count_sips' event
    socketIoService.onCountSips = (data) {
      print("Count Sips: $data");
    };

    // Add listener for 'servants_broadcast' event
    socketIoService.onServantsBroadcast = (data) {
      print("Servants Broadcast: $data");
      var playerUuid = data['masterUuid'];
      SimpleUser? player = players.firstWhereOrNull((p) => p.userUuid == playerUuid);
      print("Player: " + player!.userName + "select a servant");
      updateGameMessage(player.userName + " select a servant");
    };

    // Add listener for 'validate_servants_broadcast' event
    socketIoService.onValidateServantsBroadcast = (data) {
      print("Validate Servants Broadcast: $data");
      var masterUuid = data['masterUuid'];
      SimpleUser? master = players.firstWhereOrNull((p) => p.userUuid == masterUuid);
      var servantsUuids = data['servantUuid'];
      SimpleUser? servant = players.firstWhereOrNull((p) => p.userUuid == servantsUuids);
      print("Master: " + master!.userName);
      print("Servant: " + servant!.userName);
      updateGameMessage(master.userName + " select " + servant.userName);
    };

    // Add listener for 'servants' event
    socketIoService.onServants = (data) {
      print("Servants: $data");
      var playerUuid = data['masterUuid'] as String;
      SimpleUser? player = players.firstWhereOrNull((p) => p.userUuid == playerUuid);

      List<String> suggestedPlayersUuids = [];
      List<String> otherPlayersUuids = [];
      List<SimpleUser>suggestedPlayersList = [];
      List<SimpleUser>otherPlayersList = [];

      // get uuids of suggested players
      if (data['suggestedPlayers'] != null) {
        suggestedPlayersUuids = List<String>.from(data['suggestedPlayers']);
      }
      // get uuids of other players
      if (data['otherPlayers'] != null) {
        otherPlayersUuids = List<String>.from(data['otherPlayers']);
      }

      // get the player from the uuid from players list
      suggestedPlayersList = players.where((p) => suggestedPlayersUuids.contains(p.userUuid)).toList();
      otherPlayersList = players.where((p) => otherPlayersUuids.contains(p.userUuid)).toList();

      if (suggestedPlayersList.isEmpty && otherPlayersList.isEmpty) {
        print("Aucun joueur disponible pour la sélection.");
        return;
      }
      if (player != null){
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return SelectSinglePlayerPop(
              userName: player.userName,
              profilePicture: player.profilePicture ?? 'default_image_url',
              suggestedPlayers: suggestedPlayersList,
              otherPlayers: otherPlayersList,
              onValidateSelection: (String uuid) {
                socketIoService.sendSelectedServants(uuid);
              },
            );
          },
        );
      }
    };

    // Add listener for 'custom_rule_broadcast' event
    socketIoService.onCustomRuleBroadcast = (data) {
      print("Custom Rule Broadcast: $data");
      var playerUuid = data['playerUuid'];
      var rule = data['rule'];
      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      updateGameMessage(player.userName + ", " + rule);
    };

    // Add listener for 'custom_rule' event
    socketIoService.onCustomRule = (data) {
      print("Custom Rule: $data");
      var playerUuid = data['playerUuid'];
      var rule = data['rule'];
      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CustomPop(
              userName: player.userName,
              profilePicture: player.profilePicture ?? 'default_image_url',
              rule: rule,
            );
          });
    };

    // Add listener for 'draw_card_broadcast' event
    socketIoService.onDrawCardBroadcast = (data) {
      print("Draw Card Broadcast: $data");
      triggerAnimation(data['current_card']);
      // update card remaining
      setState(() {
        remainingCards = data['remaining_cards'];
      });
    };

    // Add listener for 'drink_broadcast' event
    socketIoService.onDrinkBroadcast = (data) {
      print("Drink Broadcast: $data");
      // Trouver le joueur concerné
      var playerUuid = data['playerUuid'];
      var sips = data['sips'];
      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      if (sips == 1){
        if (player.gender == 'female'){
          updateGameMessage(player.userName + " boit " + sips.toString() + " gorgée");
        } else {
          updateGameMessage(player.userName + " boit " + sips.toString() + " gorgée");
        }
      }
      if (sips == -1){ // finir son verre
        if (player.gender == 'female'){
          updateGameMessage(player.userName + " est heureuse de finir son verre");
        } else {
          updateGameMessage(player.userName + " est heureux de finir son verre");
        }
      } else {
        if (player.gender == 'female'){
          updateGameMessage(player.userName + " boit " + sips.toString() + " gorgées");
        } else {
          updateGameMessage(player.userName + " boit " + sips.toString() + " gorgées");
        }
      }
    };

    // Add listener for 'team_drink_broadcast' event
    socketIoService.onTeamDrinkBroadcast = (data) {
      print("Team Drink Broadcast: $data");
      List<dynamic> playerUuids = data['playersUuid'];
      int sips = data['sips'];
      List<String> messages = [];
      for (int i = 0; i < playerUuids.length; i++) {
        SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuids[i]);
        messages.add(player.userName + " boit " + sips.toString() + " gorgées");
      }
      String newMessage = messages.join('\n');
      updateGameMessage(newMessage);
    };

    // Add listener for 'drink' event
    socketIoService.onDrink = (data) {
      print("Drink Event: $data");
      // Supposons que data contienne 'playerUuid' et 'sips'
      var playerUuid = data['playerUuid'];
      var sips = data['sips'];
      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return DrinkPop(
            userName: player.userName,
            gender: player.gender,
            sips: sips,
            onOkPressed: () {
              socketIoService.validateSips(sips);
            }
          );
        },
      );
    };

    // Add listener for 'queen_broadcast' event
    socketIoService.onQueenBroadcast = (data) {
      print("Queen Broadcast: $data");
      var playerUuid = data['queenUuid'];
      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      updateGameMessage("Toutes les dames boivent. Merci " + player.userName + " !");
    };

    // Add listener for 'king_broadcast' event
    socketIoService.onKingBroadcast = (data) {
      print("King Broadcast: $data");
      var playerUuid = data['kingUuid'];
      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      updateGameMessage("Tous les rois boivent !  Merci " + player.userName + " !");
    };

    // Add listener for 'thumb_master_broadcast' event
    socketIoService.onThumb_master_broadcast = (data) {
      print("Thumb Master Broadcast: $data");
      var playerUuid = data['thumbMasterUuid'];
      isThumbMaster = false;
      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      updateGameMessage(player.userName + " est le Thumb Master !");
    };

// Add listener for 'thumb_master' event
    socketIoService.onThumb_master = (data) {
      print("Thumb Master: $data");
      var playerUuid = data['thumbMasterUuid'];
      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      // Retarder l'affichage du dialogue
      Future.delayed(Duration(seconds: 2), () {
        setState(() {
          isThumbMaster = true;
          print("isThumbMaster is now: $isThumbMaster");
        });// Attendre 2 secondes
        if (mounted) { // Vérifier si le widget est toujours présent dans l'arbre des widgets
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return ThumbMasterPop(
                  userName: player.userName,
                  onOkPressed: () {
                    print("Thumb Master validated");
                    socketIoService.validateThumbMaster();
                  }
              );
            },
          );
        }
      });
    };

    // Add listener for 'thumb_master_drink_broadcast' event
    socketIoService.onThumb_master_drink_broadcast = (data) {
      print("Thumb Master Drink Broadcast: $data");
      var playerUuid = data['looserUuid'];
      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      updateGameMessage(player.userName + " a perdu au maitre du pouce et boit 1 gorgée");
    };

    // Add listener for 'thumb_master_drink' event
    socketIoService.onThumb_master_drink = (data) {
      print("Thumb Master Drink: $data");
      if (user != null) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ThumbDrinkPop(
              userName: user!.userName,
              gender: user!.gender,
              onOkPressed: () {
                socketIoService.validateThumbMasterDrink();
              },
            );
          },
        );
      }
    };

    socketIoService.onDrinkServant = (data) {
      print("Drink Servant Event: $data");
      // Supposons que data contienne 'playerUuid' et 'sips'
      var playerUuid = data['playerUuid'];
      var sips = data['sips'];
      var masterUuid = data['masterUuid'];

      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      SimpleUser? master = players.firstWhere((p) => p.userUuid == masterUuid);

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return DrinkServantPop(
            userName: player.userName,
            sips: sips,
            masterName: master.userName,
            masterPicture: master.profilePicture ?? 'default_image_url',
          );
        },
      );
    };
    //shifumi-------------------------------------------------------------------
    socketIoService.onShifumi_broadcast = (data) {
      print("Shifumi Broadcast: $data");
      var shifumi_master_uuid = data['shifumiMasterUuid'];
      SimpleUser? shifumi_master = players.firstWhere((p) => p.userUuid == shifumi_master_uuid);
      if (shifumi_master.gender == 'female') {
        updateGameMessage(shifumi_master.userName + " est la reine du shifumi !");
      } else {
        updateGameMessage(shifumi_master.userName + " est le maître du shifumi !");
      }
    };
    socketIoService.onShifumi = (data) {
      print("Shifumi: $data");
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return ShifumiRulePop(
            userName: user!.userName,
            profilePicture: user!.profilePicture ?? 'default_image_url',
            gender: user!.gender,
            onOkPressed: () {
              print("Shifumi validated");
              socketIoService.validateShifumi();
            }
          );
        },
      );
    };
    socketIoService.onShifumi_preturn_event_master = (data) {
      // return to shifumi screen as master
      print("Shifumi Preturn Event Master: $data");
      var shifumiMaster = players.firstWhere((p) => p.userUuid == data['shifumiMasterUuid']);
      var shifumiOpponent = players.firstWhere((p) => p.userUuid == data['shifumiOpponentUuid']);
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => ShifumiDuelScreen(
            shifumiMaster: shifumiMaster,
            shifumiOpponent: shifumiOpponent,
            shifumiTurns: data['shifumiTurns'],
            shifumiCurrentTurn: data['shifumiCurrentTurn'],
            shifumiSips: data['shifumiSips'],
            isMaster: true,
          ),
        ),
      );
    };
    socketIoService.onShifumi_preturn_event_player = (data) {
      // redirect to shifumi screen as player
      print("Shifumi Preturn Event Player: $data");
      var shifumiMaster = players.firstWhere((p) => p.userUuid == data['shifumiMasterUuid']);
      var shifumiOpponent = players.firstWhere((p) => p.userUuid == data['shifumiOpponentUuid']);
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => ShifumiDuelScreen(
            shifumiMaster: shifumiMaster,
            shifumiOpponent: shifumiOpponent,
            shifumiTurns: data['shifumiTurns'],
            shifumiCurrentTurn: data['shifumiCurrentTurn'],
            shifumiSips: data['shifumiSips'],
            isMaster: false,
          ),
        ),
      );
    };
    socketIoService.onShifumi_turn_event_broadcast = (data) {
      print("Shifumi New duel: $data");
    };
    socketIoService.onShifumi_result_inter_broadcast = (data) {
      print("Shifumi Result Inter Broadcast: $data");
    };
    socketIoService.onShifumi_result_broadcast = (data) {
      print("Shifumi Result Broadcast: $data");
    };
    //--------------------------------------------------------------------------

    // Add listener for 'new_turn_broadcast' event
    socketIoService.onNewTurnBroadcast = (data) {
      print("New Turn Broadcast: $data");
      var playerUuid = data['current_player_uuid'];
      var playerIndex = data['current_player_index'];
      var playerName = data['current_player_name'];
      setState(() {
        currentPlayerName = playerName;
        currentPlayerId = playerUuid;
        isMyTurn = currentPlayerId == user!.userUuid;
      });
      SimpleUser? player = players.firstWhere((p) => p.userUuid == playerUuid);
      if (user?.userUuid == player.userUuid){
        updateGameMessage("A toi de jouer !");
      } else {
        updateGameMessage("C'est à " + player.userName + " de jouer !");
      }
    };

    socketIoService.onEndGameBroadcast = (data) {
      print("End Game Broadcast: $data");
    };

    socketIoService.setupListeners();
  }

  Future<void> fetchUsers() async {
    print("Fetching users");
    socketIoService.requestPlayerList();

    Completer<void> completer = Completer<void>();
    socketIoService.on('player_list_response', (data) {
      if (!completer.isCompleted) {
        completer.complete();
      }
      setState(() {
        players = List<SimpleUser>.from(
            data.map((item) => SimpleUser.fromJson(item)));
      });

      // Request the game state after players are fetched
      socketIoService.requestGameState();
    });
    return completer.future;
  }




  void generateCardImages() {
    const List<String> suits = ['D', 'H', 'C', 'S'];
    const List<String> ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
    for (final suit in suits) {
      for (final rank in ranks) {
        cardImages.add('assets/images/cards/${rank}$suit.png');
      }
    }
  }

  void triggerAnimation(card) {
    print("Triggering animation for card $card");
    var cardName = 'assets/images/cards/${card}.png';
    cardPilesKey.currentState?.setNextCard(cardName);
  }
  void drawCard() {
    socketIoService.drawCard();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Barbu Game'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: CurrentPlayerDisplay(currentPlayerName: currentPlayerName),
                  ),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.topRight,
                    child: RemainingCardsDisplay(remainingCards: remainingCards),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 100), // Ajuster l'espacement selon les besoins
          GameMessageWidget(message: gameMessage),
          SizedBox(height: 20), // Ajuster l'espacement selon les besoins
          Expanded(
            child: GestureDetector(
              onTap: () {
                drawCard(); // Appel de la fonction lorsque la zone est touchée
              },
              child: Center(
                child: CardPilesWidget(key: cardPilesKey, screenWidth: screenWidth),
              ),
            ),
          ),
          isThumbMaster ? Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 20.0),
              child: ThumbButton(
                onPressed: () {
                  // Actions à effectuer lorsque le bouton est pressé
                  print('Thumb button pressed');
                  SimpleUser? player = players.firstWhereOrNull((p) => p.userUuid == user?.userUuid);
                  if (player != null){
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return SelectThumbedPlayerPop(
                          userName: player.userName,
                          profilePicture: player.profilePicture ?? 'default_image_url',
                          players: players,
                          onValidateSelection: (String uuid) {
                            socketIoService.thumbMasterEvent(uuid);
                          },
                        );
                      },
                    );
                  }
                },
              ),
            ),
          ) : Container(),
          SizedBox(height: 20), // Ajuster l'espacement selon les besoins
          Container(
            height: 100, // Hauteur fixe pour la liste des utilisateurs
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: players.length,
              itemBuilder: (context, index) {
                final user = players[index];
                bool isCurrentPlayer = user.userUuid == currentPlayerId;
                return Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      isCurrentPlayer
                          ? SmallProfilePictureWithBorder(
                        imageUrl: user.profilePicture!,
                        size: 50.0,
                        borderWidth: 3.0,
                        borderColor: Colors.orange,
                      )
                          : SmallProfilePicture(
                        imageUrl: user.profilePicture!,
                        size: 50.0,
                      ),
                      Text(user.userName, style: TextStyle(fontSize: 12)),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}