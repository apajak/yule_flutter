import 'package:flutter/material.dart';
import 'package:yule_flutter/models/user.dart'; // Modèle utilisateur
import 'package:yule_flutter/services/socketio_service.dart';
import '../../models/flutter_secure_storage.dart';
import '../../widgets/game_component/animated_timer_widget.dart';
import '../../widgets/game_component/round_counter.dart';
import '../../widgets/images/profile_pic_small.dart';

enum ShifumiChoice { pierre, feuille, ciseau, random}

class ShifumiDuelScreen extends StatefulWidget {
  final SimpleUser shifumiMaster;
  final SimpleUser shifumiOpponent;
  final int shifumiTurns;
  final int shifumiCurrentTurn;
  final int shifumiSips;
  final bool isMaster;

  ShifumiDuelScreen({
    Key? key,
    required this.shifumiMaster,
    required this.shifumiOpponent,
    required this.shifumiTurns,
    required this.shifumiCurrentTurn,
    required this.shifumiSips,
    required this.isMaster,
  }) : super(key: key);

  @override
  _ShifumiDuelScreenState createState() => _ShifumiDuelScreenState();
}

class _ShifumiDuelScreenState extends State<ShifumiDuelScreen> {
  final socketIoService = SocketIoService(); // Récupération de l'instance singleton
  late User? user;
  String gameMessage = "";
  bool isReady = false;
  bool isShifumiTurnActive = false;
  ShifumiChoice? selectedChoice;



  @override
  void initState() {
    super.initState();
    _loadUserData();
    setupGameEventListeners();
  }

  void _loadUserData() async {
    // Fetch the user data from secure storage
    final storage = UserDataStorage();
    var userData = await storage.getUserData();
    setState(() {
      user = userData;
    });
  }
  void updateGameMessage(String newMessage) {
    setState(() {
      gameMessage = newMessage;
    });
  }

  void setupGameEventListeners() {
    // future listener for shifumi
    socketIoService.onShifumi_turn_event_player = (data) {
      print("Shifumi turn event player");
      print(data);
      setState(() {
        isShifumiTurnActive = true;
        // Autres logiques nécessaires
      });
    };
    socketIoService.onShifumi_turn_event_master = (data) {
      print("Shifumi turn event master");
      print(data);
      setState(() {
        isShifumiTurnActive = true;
        // Autres logiques nécessaires
      });
    };
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Shifumi Duel"),
      ),
      body: isShifumiTurnActive ? _buildShifumiTurnView() : _buildInitialView(),
    );
  }

  Widget _buildShifumiTurnView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        RoundCounterDisplay(
          totalRounds: widget.shifumiTurns,
          currentRound: widget.shifumiCurrentTurn,
          iconData: Icons.people,
          color: Colors.black,
        ),
        SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Text(
            'Choisissez votre coup',
            style: TextStyle(color: Colors.black),
          ),
        ),
        SizedBox(height: 20),
        AnimatedTimerWidget(
          duration: Duration(seconds: 5),
          textColor: Colors.black,
          onTimerEnd: () {
            print("Timer terminé");
          },
        ),
        SizedBox(height: 30),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: ShifumiChoice.values
              .map((choice) => _buildChoiceButton(choice))
              .toList(),
        ),
      ],
    );
  }

  Widget _buildInitialView() {
    var text = "";
    if (widget.isMaster) {
      if ((widget.shifumiCurrentTurn == 1) && (widget.shifumiCurrentTurn != widget.shifumiTurns)) {
        if (widget.shifumiMaster.gender == 'female') {
          text = "${widget.shifumiMaster.userName}, Es-tu prête a affronté ${widget.shifumiOpponent.userName} pour le premier duel ?";
        } else {
          text = "${widget.shifumiMaster.userName}, Es-tu prêt a affronté ${widget.shifumiOpponent.userName} pour le premier duel ?";
        }
      } else if ((widget.shifumiCurrentTurn == widget.shifumiTurns) && (widget.shifumiCurrentTurn != 1)) {
        if (widget.shifumiMaster.gender == 'female') {
          text = "${widget.shifumiMaster.userName}, Es-tu prête a affronté ${widget.shifumiOpponent.userName} pour le dernier duel ?";
        } else {
          text = "${widget.shifumiMaster.userName}, Es-tu prêt a affronté ${widget.shifumiOpponent.userName} pour le dernier duel ?";
        }
      } else if ((widget.shifumiCurrentTurn == widget.shifumiTurns) && (widget.shifumiCurrentTurn == 1)) {
        if (widget.shifumiMaster.gender == 'female') {
          text = "${widget.shifumiMaster.userName}, Es-tu prête a affronté ${widget.shifumiOpponent.userName} pour le seul duel ?";
        } else {
          text = "${widget.shifumiMaster.userName}, Es-tu prêt a affronté ${widget.shifumiOpponent.userName} pour le seul duel ?";
        }
      } else {
        if (widget.shifumiMaster.gender == 'female') {
          text = "${widget.shifumiMaster.userName}, Es-tu prête a affronté ${widget.shifumiOpponent.userName} pour le duel n°${widget.shifumiCurrentTurn} ?";
        } else {
          text = "${widget.shifumiMaster.userName}, Es-tu prêt a affronté ${widget.shifumiOpponent.userName} pour le duel n°${widget.shifumiCurrentTurn} ?";
        }
      }
    } else {
      if ((widget.shifumiCurrentTurn == 1) && (widget.shifumiCurrentTurn != widget.shifumiTurns)) {
        if (widget.shifumiMaster.gender == 'female') {
          text = "${widget.shifumiOpponent.userName}, tu es la première adversaire de ${widget.shifumiMaster.userName}. Prête?";
        } else {
          text = "${widget.shifumiOpponent.userName} tu es le premier adversaire de ${widget.shifumiMaster.userName}. Prêt?";
        }
      } else if ((widget.shifumiCurrentTurn == widget.shifumiTurns) && (widget.shifumiCurrentTurn != 1)) {
        if (widget.shifumiMaster.gender == 'female') {
          text = "${widget.shifumiOpponent.userName}, tu es la dernière adversaire de ${widget.shifumiMaster.userName}. Prête?";
        } else {
          text = "${widget.shifumiOpponent.userName} tu es le dernier adversaire de ${widget.shifumiMaster.userName}. Prêt?";
        }
      } else if ((widget.shifumiCurrentTurn == widget.shifumiTurns) && (widget.shifumiCurrentTurn == 1)) {
        if (widget.shifumiMaster.gender == 'female') {
          text = "${widget.shifumiOpponent.userName}, tu es l'unique adversaire de ${widget.shifumiMaster.userName}. Prête?";
        } else {
          text = "${widget.shifumiOpponent.userName} tu es l'unique adversaire de ${widget.shifumiMaster.userName}. Prêt?";
        }
      } else {
        if (widget.shifumiMaster.gender == 'female') {
          text = "${widget.shifumiOpponent.userName}, tu es l'adversaire n°${widget.shifumiCurrentTurn} de ${widget.shifumiMaster.userName}. Prête?";
        } else {
          text = "${widget.shifumiOpponent.userName} tu es l'adversaire n°${widget.shifumiCurrentTurn} de ${widget.shifumiMaster.userName}. Prêt?";
        }
      }
    }
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: Text(
              'Shi Fu Mi Duel',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: RoundCounterDisplay(
              totalRounds: widget.shifumiTurns,
              currentRound: widget.shifumiCurrentTurn,
              iconData: Icons.timelapse,
              color: Colors.white,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SmallProfilePicture(
                imageUrl: widget.shifumiMaster.profilePicture ?? '',
                size: 100.0,
              ),
              Text(
                'VS',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              SmallProfilePicture(
                imageUrl: widget.shifumiOpponent.profilePicture ?? '',
                size: 100.0,
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Text(
              text,
              style: TextStyle(fontSize: 20),
            ),
          ),
          ElevatedButton(
            child: Text("Prêt"),
            onPressed: () {
              // Logique pour démarrer le duel ou indiquer que le joueur est prêt
              print("Shifumi Duel Start");
              socketIoService.validateShifumiPreturn();
            },
          ),
        ],
      );
  }

  Widget _buildChoiceButton(ShifumiChoice choice) {
    IconData iconData;
    switch (choice) {
      case ShifumiChoice.pierre:
        iconData = Icons.circle;
        break;
      case ShifumiChoice.feuille:
        iconData = Icons.file_copy;
        break;
      case ShifumiChoice.ciseau:
        iconData = Icons.content_cut;
        break;
      case ShifumiChoice.random:
        iconData = Icons.shuffle;
        break;
    }

    return Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.grey[300], // Arrière-plan clair pour les boutons
            borderRadius: BorderRadius.circular(30), // Coins arrondis
          ),
          padding: EdgeInsets.all(8), // Espacement autour de l'icône
          child: IconButton(
            icon: Icon(iconData, color: Colors.black),
            onPressed: () => _onChoiceSelected(choice),
          ),
        ),
        if (selectedChoice ==
            choice) // Afficher le badge si c'est le choix sélectionné
          Positioned(
            right: -5,
            top: -5,
            child: CircleAvatar(
              backgroundColor: Colors.red,
              radius: 12,
              child: Icon(
                Icons.check,
                color: Colors.white,
                size: 16,
              ),
            ),
          ),
      ],
    );
  }


  void _onChoiceSelected(ShifumiChoice choice) {
    setState(() {
      selectedChoice = choice;
    });
  }
}