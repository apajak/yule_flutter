import 'package:flutter/material.dart';
import 'package:yule_flutter/screens/test_screen/shifumi_master.dart';
import 'package:yule_flutter/screens/test_screen/test_select_single_player.dart';

// Importez vos pages de test ici
import 'package:yule_flutter/screens/test_screen/test_thumb_select_player.dart';
import 'package:yule_flutter/screens/test_screen/card_home_view.dart';
import 'package:yule_flutter/screens/test_screen/timer_test_screen.dart';

class MainTestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Main Test Page', style: TextStyle(color: Colors.white)),
      ),
      body: ListView(
        children: <Widget>[
          // Bouton pour la page de test 1
          ListTile(
            title: Text('Thumb Selector', style: TextStyle(color: Colors.white)),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => TestThumbSelectPage()));
            },
          ),
          ListTile(
            title: Text('single player Selector', style: TextStyle(color: Colors.white)),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => TestSelectSinglePlayerPopPage()));
            },
          ),
          // Bouton pour la page de test 2
          ListTile(
            title: Text('Cards', style: TextStyle(color: Colors.white)),
            onTap: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => CardHomeView()));
            },
          ),
          ListTile(
            title: Text('shifumi master', style: TextStyle(color: Colors.white)),
            onTap: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => ShifumiTestPage()));
            },
          ),
          ListTile(
            title: Text('Timer', style: TextStyle(color: Colors.white)),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => TimerDemoPage()));
            },
          )
        ],
      ),
    );
  }
}
