import 'package:flutter/material.dart';
import 'package:yule_flutter/widgets/game_component/animated_timer_widget.dart';

import '../../widgets/game_component/animated_timer_widget_shifumi.dart';
import '../../widgets/game_component/shifumi_puls.dart';

class TimerDemoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Timer Demo"),
      ),
      body: Center(
        child: PulsatingShifumiWidget(
          onAnimationEnd: () {
            print("Animation end");
          },
        ),
      ),
    );
  }
}
