import 'package:flutter/material.dart';
import 'package:yule_flutter/models/user.dart';
import '../../widgets/popups/select_single_player_pop.dart';

class TestSelectSinglePlayerPopPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Création de faux joueurs sugérés
    List<SimpleUser> mockPlayers = List.generate(6, (index) => SimpleUser(
        userUuid: 'uuid-$index',
        userName: 'Joueur $index',
        profilePicture: 'http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png',
        gameAlcohol: 3,
        gender: index % 2 == 0 ? 'male' : 'female'
    ));
    // Création de faux joueurs autres
    List<SimpleUser> mockPlayers_others = List.generate(3, (index) => SimpleUser(
        userUuid: 'uuid-1$index',
        userName: 'Joueur_other $index',
        profilePicture: 'http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png',
        gameAlcohol: 0,
        gender: index % 2 == 0 ? 'male' : 'female'
    ));

    return Scaffold(
      appBar: AppBar(
        title: Text("Test SelectSinglePlayerPop"),
      ),
      body: Center(
        child: ElevatedButton(
          child: Text("Ouvrir le Sélecteur de Joueur"),
          onPressed: () {
            showDialog(
              context: context,
              builder: (context) => SelectSinglePlayerPop(
                userName: "Testeur",
                profilePicture: "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png",
                suggestedPlayers: mockPlayers,
                otherPlayers: mockPlayers_others,
                onValidateSelection: (selectedUuid) {
                  print("Joueur sélectionné : $selectedUuid");
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
