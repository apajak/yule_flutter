import 'package:flutter/material.dart';
import 'package:yule_flutter/models/user.dart';

import '../../widgets/popups/shifumi_master.dart';


class ShifumiTestPage extends StatefulWidget {
  @override
  _ShifumiTestPageState createState() => _ShifumiTestPageState();
}

class _ShifumiTestPageState extends State<ShifumiTestPage> {
  @override
  Widget build(BuildContext context) {
// create two instances of SimpleUser
    SimpleUser master = SimpleUser(userUuid: "uuid_master",
        userName: "Master",
        profilePicture: "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png",
        gameAlcohol: 2,
        gender: "male");
    SimpleUser secondPlayer = SimpleUser(userUuid: "uuid_player2",
        userName: "Player 2",
        profilePicture: "http://10.0.2.2:8000/profile_pics/profile_picture_7d34bb7910fad63a.png",
        gameAlcohol: 3,
        gender: "female");

    return Scaffold(
      appBar: AppBar(
        title: Text("Test Shifumi Master"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              child: Text("Start Shifumi Duel"),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return ShifumiMaster(
                      master: master,
                      secondPlayer: secondPlayer,
                      numberOfRounds: 6,
                      currentRound: 1,
                      numberOfTurns: 3,
                      currentTurn: 1,
                      onGameResult: (masterUuid, secondPlayerUuid, lostRounds) {
                        print(
                            "Shifumi Duel result : $masterUuid, $secondPlayerUuid, $lostRounds");
                      },
                    );
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
// Affichez les résultats du
