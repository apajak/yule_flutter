
import 'package:flutter/material.dart';
import 'package:yule_flutter/models/user.dart';
import 'package:yule_flutter/widgets/buttons/base/custom_button.dart';
import 'package:yule_flutter/widgets/textFields/custom_textfield.dart';
import 'package:yule_flutter/widgets/pickers/datePicker/birthdate_picker.dart';
import 'package:yule_flutter/widgets/switch/register/state_switch.dart';
import 'package:yule_flutter/services/api_service.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'dart:io';
import 'dart:async';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final apiService = ApiService();
  final genderList = ["female", "male", "ND"];
  final sexualOrientationList = ["heterosexual", "homosexual", "ND"];
  final coupleStateList = ["single", "couple", "open_relationship", "ND"];
  final alcoholConsumptionList = ["never", "occasionally", "regularly", "ND"];
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();
  final TextEditingController firstNameController = TextEditingController();
  final TextEditingController lastNameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final ValueNotifier<DateTime> selectedDateNotifier = ValueNotifier<DateTime>(
      DateTime.now()); // Initialize with null or a default date
  final ValueNotifier<int> genderValueNotifier =
      ValueNotifier<int>(0); // Initialize with the default index
  final ValueNotifier<int> sexualOrientationValueNotifier =
      ValueNotifier<int>(0); // Initialize with the default index
  final ValueNotifier<int> coupleStateValueNotifier =
      ValueNotifier<int>(0); // Initialize with the default index
  final ValueNotifier<int> alcoholConsumptionValueNotifier =
      ValueNotifier<int>(0); // Initialize with the default index
  File? _image;

  Future<void> _getImage(ImageSource source) async {
    final pickedImage = await ImagePicker().pickImage(source: source);
    if (pickedImage != null) {
      File? croppedImage;
      CroppedFile? croppedFile = await ImageCropper().cropImage(
        sourcePath: pickedImage.path,
        aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1),
        compressQuality: 100,
        maxWidth: 700,
        maxHeight: 700,
        compressFormat: ImageCompressFormat.jpg,
        cropStyle: CropStyle.circle,
      );
      if (croppedFile != null) {
        croppedImage = File(croppedFile.path);
      }
      if (croppedImage != null) {
        setState(() {
          _image = croppedImage;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.register),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              GestureDetector(
                onTap: () async {
                  showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) {
                      return SafeArea(
                        child: Wrap(
                          children: <Widget>[
                            ListTile(
                              leading: const Icon(Icons.photo_library),
                              title:
                                  Text(AppLocalizations.of(context)!.gallery),
                              onTap: () {
                                _getImage(ImageSource.gallery); // Open gallery
                                Navigator.pop(context);
                              },
                            ),
                            ListTile(
                              leading: const Icon(Icons.photo_camera),
                              title: Text(
                                  AppLocalizations.of(context)!.takeAPicture),
                              onTap: () {
                                _getImage(ImageSource.camera); // Open camera
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                      );
                    },
                  );
                },
                child: CircleAvatar(
                  radius: 100, // Adjust the radius as needed
                  backgroundImage: _image != null ? FileImage(_image!) : null,
                  child: _image == null
                      ? const Icon(
                          Icons.person,
                          size: 100, // Adjust the size of the icon
                          color: Colors.white,
                        )
                      : null,
                ),
              ),
              CustomTextField(
                labelText: AppLocalizations.of(context)!.username,
                controller: usernameController,
              ),
              CustomTextField(
                labelText: AppLocalizations.of(context)!.firstName,
                controller: firstNameController,
              ),
              CustomTextField(
                labelText: AppLocalizations.of(context)!.lastName,
                controller: lastNameController,
              ),
              CustomTextField(
                labelText: AppLocalizations.of(context)!.emailAddress,
                controller: emailController,
              ),
              BirthdatePickerWidget(selectedDateNotifier: selectedDateNotifier),
              StateSwitch(
                label: AppLocalizations.of(context)!.gender,
                values: genderList,
                onChanged: (value) {
                  print(value);
                },
                selectedValueNotifier: genderValueNotifier,
              ),
              StateSwitch(
                label: AppLocalizations.of(context)!.sexualOrientation,
                values: sexualOrientationList,
                onChanged: (value) {
                  print(value);
                },
                selectedValueNotifier: sexualOrientationValueNotifier,
              ),
              StateSwitch(
                label: AppLocalizations.of(context)!.coupleState,
                values: coupleStateList,
                onChanged: (value) {
                  print(value);
                },
                selectedValueNotifier: coupleStateValueNotifier,
              ),
              StateSwitch(
                label: AppLocalizations.of(context)!.alcoholConsumption,
                values: alcoholConsumptionList,
                onChanged: (value) {
                  print(value);
                },
                selectedValueNotifier: alcoholConsumptionValueNotifier,
              ),
              const SizedBox(height: 20),
              CustomTextField(
                labelText: AppLocalizations.of(context)!.password,
                controller: passwordController,
                obscureText: true,
              ),
              const SizedBox(height: 20),
              CustomTextField(
                labelText: AppLocalizations.of(context)!.confirmPassword,
                controller: confirmPasswordController,
                obscureText: true,
              ),
              const SizedBox(height: 20),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(16.0),
        child: CustomButton(
          text: AppLocalizations.of(context)!.register,
          onPressed: () async {
            // Create a User object from the form fields
            UserCreate user = UserCreate(
                userName: usernameController.text,
                firstName: firstNameController.text,
                lastName: lastNameController.text,
                email: emailController.text,
                birthDate: selectedDateNotifier.value,
                gender: genderList[genderValueNotifier.value],
                sexualOrientation:
                    sexualOrientationList[sexualOrientationValueNotifier.value],
                coupleState: coupleStateList[coupleStateValueNotifier.value],
                alcohol: alcoholConsumptionList[
                    alcoholConsumptionValueNotifier.value],
                password: passwordController.text);
            // Call the register API
            final registerResponse = await apiService.register(user);
            // Parse the JSON response into a User object
            String message = registerResponse['message'];
            print(message);
            bool value = registerResponse['value'];
            if (value == false) {
              // TODO: Show the error message in alert & resolve type error
              print(registerResponse['failed_tests'][0][0]);
              // Show an error alert
            } else {
              print(message);
              // upload image
              if (_image != null) {
                final userId = registerResponse['user_id'];
                final uploadResponse =
                    await apiService.uploadProfilePicture(_image!, userId);
                print(uploadResponse);
                user.profilePicture = uploadResponse['url'];

                // redirect to login page
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => RegisterScreen(),
                  ),
                );
              }
              // Show a success alert
            }
          },
        ),
      ),
    );
  }
}
