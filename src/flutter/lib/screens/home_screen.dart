import 'package:flutter/material.dart';
import 'package:yule_flutter/screens/create_game_screen.dart';
import 'package:yule_flutter/screens/settings/settings_main_screen.dart';
import 'package:yule_flutter/screens/test_screen/main_test.dart';
import 'package:yule_flutter/widgets/buttons/large/validation_button_L.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:yule_flutter/services/api_service.dart';
import '../widgets/buttons/icon_button/badge_icon_button.dart';
import 'join_game_screen.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final apiService = ApiService();
  bool hasPendingRequests = false;

  @override
  void initState() {
    super.initState();
    _checkForPendingRequests();
  }

  void _checkForPendingRequests() async {
    try {
      final requests = await apiService.getFriendRequests();
      if (mounted) {
        setState(() {
          hasPendingRequests = requests.isNotEmpty;
        });
      }
    } catch (e) {
      print("Error fetching friend requests: $e");
    }
  }

  void _refreshBadgeStatus() {
    _checkForPendingRequests();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.home),
        actions: <Widget>[
          BadgeIconButton(
            icon: Icons.settings,
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) =>
                      MainSettingsScreen(onBadgeUpdate: _refreshBadgeStatus),
                ),
              );
            },
            showBadge: hasPendingRequests,
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const SizedBox(height: 20),
            Image.asset(
              'assets/images/logo_yule_letters.png',
              width: 300, // Set the width of the logo
              height: 300, // Set the height of the logo
            ),
            ValidationButtonL(
              text: AppLocalizations.of(context)!.createGame,
              onPressed: () {
                // Navigate to the second screen using a named route.
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => CreateGameScreen(),
                  ),
                );
              },
            ),
            ValidationButtonL(
              text: AppLocalizations.of(context)!.joinGame,
              onPressed: () {
                // Navigate to the second screen using a named route.
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => JoinGameScreen(),
                  ),
                );
              },
            ),
            ValidationButtonL(
              text: "test",
              onPressed: () {
                // Navigate to the second screen using a named route.
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => MainTestPage(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
