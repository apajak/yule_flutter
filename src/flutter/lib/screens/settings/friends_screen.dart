import 'package:flutter/material.dart';
import 'package:yule_flutter/models/user.dart'; // Make sure to import the SimpleUser class
import 'package:yule_flutter/services/api_service.dart';
import 'dart:async';
import '../../widgets/images/profile_pic_small.dart';
import '../../widgets/searchBar/search_bar.dart'; // Import your API service

class FriendsScreen extends StatefulWidget {
  final Function onFriendRequestHandled;
  // callback function to update the badge icon in the home screen
  FriendsScreen({required this.onFriendRequestHandled});

  @override
  _FriendsScreenState createState() => _FriendsScreenState();
}

class _FriendsScreenState extends State<FriendsScreen> {
  final apiService = ApiService();
  Timer? _debounce;
  List<SimpleUser> friends = [];
  List<SimpleUser> searchResults = [];
  List<UserWithStatus> combinedList = [];
  bool isQueryInProgress = false;

  final searchController = TextEditingController();

  List<SimpleUser> pendingFriendRequests = [];

  @override
  void initState() {
    super.initState();
    _fetchUsers();
    _fetchPendingFriendRequests();
  }

  @override
  void dispose() {
    _debounce?.cancel();
    searchController.dispose();
    super.dispose();
  }

  void _fetchPendingFriendRequests() async {
    try {
      var requests = await apiService.getFriendRequests();
      setState(() {
        pendingFriendRequests = requests;
        print(pendingFriendRequests);
      });
    } catch (e) {
      _showSnackBar("Error fetching friend requests: $e");
    }
  }

  void _mergeLists() {
    if (searchController.text.isNotEmpty) {
      // Merge friends with search results only when there is a search query
      combinedList = [
        ...friends
            .map((user) => UserWithStatus(user: user, isFriend: true))
            .toList(),
        ...searchResults
            .map((user) => UserWithStatus(user: user, isFriend: false))
            .toList(),
      ];
    } else {
      // When search bar is empty, only show friends
      combinedList = friends
          .map((user) => UserWithStatus(user: user, isFriend: true))
          .toList();
    }
    setState(() {});
  }

  void _showSnackBar(String message) {
    final snackBar = SnackBar(
      content: Text(message),
      duration: Duration(seconds: 3),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  // Method to send a friend request
  Future<void> _sendFriendRequest(String friendUuid) async {
    try {
      var response = await apiService.sendFriendRequest(friendUuid);
      if (response['value']) {
        _showSnackBar('Friend request sent successfully');
      } else {
        _showSnackBar('Failed to send friend request: ${response['error']}');
      }
    } catch (error) {
      _showSnackBar('Error sending friend request: $error');
    }
  }

  Future<void> _acceptFriendRequest(String friendUuid) async {
    try {
      bool result = await apiService.acceptFriendRequest(friendUuid);
      if (result) {
        _showSnackBar('Friend request accepted');
        _fetchPendingFriendRequests(); // Refresh the list
        widget.onFriendRequestHandled(); // Notify the parent widget
      } else {
        _showSnackBar('Failed to accept friend request');
      }
    } catch (e) {
      _showSnackBar('Error: $e');
    }
  }

  Future<void> _declineFriendRequest(String friendUuid) async {
    try {
      bool result = await apiService.declineFriendRequest(friendUuid);
      if (result) {
        _showSnackBar('Friend request declined');
        _fetchPendingFriendRequests(); // Refresh the list
        widget.onFriendRequestHandled(); // Notify the parent widget
      } else {
        _showSnackBar('Failed to decline friend request');
      }
    } catch (e) {
      _showSnackBar('Error: $e');
    }
  }

  // Method to remove a friend
  Future<void> _removeFriend(SimpleUser friend) async {
    try {
      final response = await apiService.removeFriend(friend.userUuid);
      if (response['value']) {
        // API call was successful, so update your local data (remove the friend)
        setState(() {
          friends.removeWhere((f) => f.userUuid == friend.userUuid);
          _mergeLists(); // Rebuild combinedList to reflect the change
        });
        _showSnackBar('Friend removed successfully');
      } else {
        // Handle the case where the API call to remove the friend fails
        _showSnackBar('Failed to remove friend');
      }
    } catch (e) {
      // Handle errors here
      _showSnackBar('Error: $e');
    }
  }

  // Method to get all friends
  Future<void> _fetchUsers() async {
    try {
      final response = await apiService.getFriends();
      if (response['value']) {
        List<SimpleUser> userList = (response['users'] as List)
            .map((userData) => SimpleUser.fromJson(userData))
            .toList();
        setState(() {
          friends = userList;
          _mergeLists(); // Update combinedList after fetching friends
        });
      } else {
        print('Failed to fetch users');
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  // Method to search for users

  void _searchUsers(String query) {
    if (_debounce?.isActive ?? false) {
      _debounce!.cancel();
    }

    _debounce = Timer(const Duration(milliseconds: 500), () async {
      if (query.isNotEmpty) {
        setState(() {
          isQueryInProgress = true;
        });
        try {
          final response = await apiService.searchUsers(query);
          if (response['value']) {
            List<SimpleUser> userList = (response['users'] as List)
                .map((userData) => SimpleUser.fromJson(userData))
                .toList();

            // Filter the users into friends and non-friends
            var friendsResults = userList
                .where((user) =>
                    friends.any((friend) => friend.userUuid == user.userUuid))
                .toList();
            var nonFriendsResults = userList
                .where((user) =>
                    !friends.any((friend) => friend.userUuid == user.userUuid))
                .toList();

            setState(() {
              searchResults = [...friendsResults, ...nonFriendsResults];
              combinedList = searchResults
                  .map((user) => UserWithStatus(
                      user: user,
                      isFriend: friends
                          .any((friend) => friend.userUuid == user.userUuid)))
                  .toList();
              isQueryInProgress = false;
            });
          } else {
            print('Failed to search for users');
            setState(() {
              isQueryInProgress = false;
            });
          }
        } catch (e) {
          print('Error: $e');
          setState(() {
            isQueryInProgress = false;
          });
        }
      } else {
        // If search query is empty, reset searchResults and update combinedList
        setState(() {
          searchResults = [];
          _mergeLists();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Friends'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              _fetchUsers(); // Reload friend list
              _fetchPendingFriendRequests(); // Reload friend requests
            },
            tooltip: 'Reload Friend List and Pending Friend Requests',
          ),
        ],
      ),
      body: Column(
        children: [
          if (pendingFriendRequests.isNotEmpty)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Pending Friend Requests',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: pendingFriendRequests.length,
                    itemBuilder: (context, index) {
                      final request = pendingFriendRequests[index];
                      return Card(
                        color: Theme.of(context).highlightColor,
                        child: ListTile(
                          title: Text(request.userName),
                          leading: SmallProfilePicture(
                            imageUrl: request.profilePicture!,
                            size: 40.0,
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              ElevatedButton(
                                onPressed: () {
                                  // Accept friend request
                                  _acceptFriendRequest(request.userUuid);
                                  // Reload friend list
                                  _fetchUsers();
                                  // Reload friend requests
                                  _fetchPendingFriendRequests();
                                },
                                child: Text('Accept'),
                              ),
                              SizedBox(width: 8),
                              ElevatedButton(
                                onPressed: () =>
                                    _declineFriendRequest(request.userUuid),
                                child: Text('Decline'),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),

          // Search Bar
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: CustomSearchBar(
              controller: searchController,
              onChanged: _searchUsers,
              onClear: () {
                searchController.clear();
                _fetchUsers();
              },
              onSearch: () {
                _searchUsers(searchController.text);
              },
            ),
          ),

          // Combined Friends and Search Results
          Expanded(
            child: isQueryInProgress
                ? Center(child: CircularProgressIndicator())
                : ListView.builder(
                    itemCount: combinedList.length,
                    itemBuilder: (context, index) {
                      final userWithStatus = combinedList[index];
                      return Card(
                        margin:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        color: userWithStatus.isFriend
                            ? Colors.greenAccent
                            : Theme.of(context)
                                .highlightColor, // Change color based on condition
                        child: ListTile(
                          title: Text(userWithStatus.user.userName),
                          leading: SmallProfilePicture(
                              imageUrl: userWithStatus.user.profilePicture!,
                              size: 40.0),
                          trailing: userWithStatus.isFriend
                              ? ElevatedButton(
                                  onPressed: () {
                                    _removeFriend(userWithStatus.user);
                                  },
                                  child: Text('Remove'),
                                )
                              : ElevatedButton(
                                  onPressed: () {
                                    _sendFriendRequest(
                                        userWithStatus.user.userUuid);
                                  },
                                  child: Text('Add Friend'),
                                ),
                        ),
                      );
                    },
                  ),
          ),
        ],
      ),
    );
  }
}
