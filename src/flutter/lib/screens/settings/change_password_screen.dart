import 'package:flutter/material.dart';
import 'package:yule_flutter/screens/settings/settings_main_screen.dart';
import 'package:yule_flutter/widgets/buttons/large/validation_button_L.dart';
import '../../models/flutter_secure_storage.dart';
import '../../models/user.dart';
import '../../services/api_service.dart';
import '../../widgets/textFields/custom_textfield.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({super.key});

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final apiService = ApiService();
  final storage = UserDataStorage();
  final TextEditingController oldPasswordController = TextEditingController();
  final TextEditingController newPasswordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Change Password'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ListView(
          children: <Widget>[
            CustomTextField(
              controller: oldPasswordController,
              obscureText: true,
              labelText: AppLocalizations.of(context)!.oldPassword,
            ),
            CustomTextField(
              controller: newPasswordController,
              obscureText: true,
              labelText: AppLocalizations.of(context)!.newPassword,
            ),
            CustomTextField(
              controller: confirmPasswordController,
              obscureText: true,
              labelText: AppLocalizations.of(context)!.confirmPassword,
            ),
            ValidationButtonL(
              text: AppLocalizations.of(context)!.changePassword,
              onPressed: () async {
                // check if the new password and confirm password are the same
                if (newPasswordController.text ==
                    confirmPasswordController.text) {
                  final userUpdatePassword = UserUpdatePassword(
                      password: oldPasswordController.text,
                      newPassword: newPasswordController.text,
                      identificationToken:
                          await storage.getIdentificationToken());
                  final response =
                      await apiService.updatePassword(userUpdatePassword);
                  print(response);
                } else {
                  print(
                      'The new password and confirm password are not the same');
                }
                // redirect to settings page
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => MainSettingsScreen(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
