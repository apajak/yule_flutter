import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:yule_flutter/widgets/buttons/icon_button/image_button.dart';
import '../../main.dart';

class SelectLanguageScreen extends StatefulWidget {
  const SelectLanguageScreen({Key? key}) : super(key: key);

  @override
  _SelectLanguageScreenState createState() => _SelectLanguageScreenState();
}

class _SelectLanguageScreenState extends State<SelectLanguageScreen> {
  void _changeLanguage(Locale locale) {
    final newNavigatorKey = GlobalKey<NavigatorState>();

    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => MyApp(
          locale: locale,
          navigatorKey: newNavigatorKey,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.selectLanguage),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ListView(
          children: <Widget>[
            ImageButton(
              icon: Image.asset('icons/flags/png/gb.png',
                  package: 'country_icons', height: 24),
              text: AppLocalizations.of(context)!.en,
              onPressed: () {
                _changeLanguage(const Locale('en', ''));
              },
            ),
            ImageButton(
              icon: Image.asset('icons/flags/png/fr.png',
                  package: 'country_icons', height: 24),
              text: AppLocalizations.of(context)!.fr,
              onPressed: () {
                _changeLanguage(const Locale('fr', ''));
              },
            ),
            ImageButton(
              icon: Image.asset('icons/flags/png/es.png',
                  package: 'country_icons', height: 24),
              text: AppLocalizations.of(context)!.es,
              onPressed: () {
                _changeLanguage(const Locale('es', ''));
              },
            ),
            ImageButton(
              icon: Image.asset('icons/flags/png/it.png',
                  package: 'country_icons', height: 24),
              text: AppLocalizations.of(context)!.it,
              onPressed: () {
                _changeLanguage(const Locale('it', ''));
              },
            ),
            ImageButton(
              icon: Image.asset('icons/flags/png/pt.png',
                  package: 'country_icons', height: 24),
              text: AppLocalizations.of(context)!.pt,
              onPressed: () {
                _changeLanguage(const Locale('pt', ''));
              },
            ),
            ImageButton(
              icon: Image.asset('icons/flags/png/de.png',
                  package: 'country_icons', height: 24),
              text: AppLocalizations.of(context)!.de,
              onPressed: () {
                _changeLanguage(const Locale('de', ''));
              },
            ),
          ],
        ),
      ),
    );
  }
}
