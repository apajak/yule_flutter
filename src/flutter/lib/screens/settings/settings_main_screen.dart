import 'package:flutter/material.dart';
import 'package:yule_flutter/models/flutter_secure_storage.dart';
import 'package:yule_flutter/screens/settings/edit_profile.dart';
import 'package:yule_flutter/screens/settings/select_language.dart';
import '../../models/user.dart';
import '../../widgets/buttons/medium/left_icon_button_M.dart';
import '../../widgets/buttons/medium/validation_button_M.dart';
import '../../widgets/images/profile_pic_large.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'friends_screen.dart';
import 'package:yule_flutter/services/api_service.dart';

class MainSettingsScreen extends StatefulWidget {
  // callback for updating the badge status in the home screen
  final Function()? onBadgeUpdate;
  MainSettingsScreen({this.onBadgeUpdate});

  @override
  _MainSettingsScreenState createState() => _MainSettingsScreenState();
}

class _MainSettingsScreenState extends State<MainSettingsScreen> {
  User? user;
  String? imageUrl;
  final apiService = ApiService();
  bool hasPendingRequests = false;

  @override
  void initState() {
    super.initState();
    _loadUserData();
    _checkForPendingRequests();
  }

  void _checkForPendingRequests() async {
    try {
      final requests = await ApiService().getFriendRequests();
      setState(() {
        hasPendingRequests = requests.isNotEmpty;
      });
    } catch (e) {
      // Handle the error
      print("Error fetching friend requests: $e");
    }
  }

  void _loadUserData() async {
    // Fetch the user data from secure storage
    final storage = UserDataStorage();
    user = await storage.getUserData();
    if (user != null) {
      // Update the UI with the user's data
      setState(() {
        imageUrl = user!.profilePicture;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.settings),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ListView(
          children: <Widget>[
            // User profile picture
            ProfilePicture_L(
              imageUrl: imageUrl,
            ),
            ValidationButtonM(
              text: AppLocalizations.of(context)!.editProfile,
              onPressed: () {
                // Navigate to the second screen using a named route.
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const EditProfileScreen(),
                  ),
                );
              },
            ),
            // add space between buttons
            const SizedBox(height: 60),
            LeftIconButtonM(
              icon: Icons.people,
              text: AppLocalizations.of(context)!.friends,
              onPressed: () {
                // Navigate to the second screen using a named route.
                Navigator.of(
                        context) // Navigate to the second screen using a named route.
                    .push(
                  MaterialPageRoute(
                    builder: (context) => FriendsScreen(
                      onFriendRequestHandled: () {
                        _checkForPendingRequests();
                        widget.onBadgeUpdate!();
                      },
                    ),
                  ),
                );
              },
              showBadge:
                  hasPendingRequests, // Based on your logic to show badge
              badgeText: '!', // Optional: Customize badge text
            ),
            LeftIconButtonM(
              icon: Icons.flag,
              text: AppLocalizations.of(context)!.languages,
              onPressed: () {
                // Navigate to the second screen using a named route.
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const SelectLanguageScreen(),
                  ),
                );
              },
            ),
            LeftIconButtonM(
              icon: Icons.volume_up,
              text: AppLocalizations.of(context)!.sounds,
              onPressed: () {
                // Navigate to the second screen using a named route.
              },
            ),
            LeftIconButtonM(
              icon: Icons.warning_amber,
              text: AppLocalizations.of(context)!.sip_limiting,
              onPressed: () {
                // Navigate to the second screen using a named route.
              },
            ),
            // List of settings options
            // Add more settings options as needed
          ],
        ),
      ),
    );
  }
}
