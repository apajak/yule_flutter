import 'package:flutter/material.dart';
import 'dart:io';
import '../../services/api_service.dart';
import '../../widgets/buttons/medium/validation_button_M.dart';
import '../../widgets/images/profile_pic_large.dart';
import 'package:yule_flutter/models/flutter_secure_storage.dart';
import 'package:yule_flutter/models/user.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import '../../widgets/switch/register/state_switch.dart';
import '../../widgets/textFields/custom_textfield.dart';
import 'change_password_screen.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({super.key});

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  // Properties for editing the user's profile here
  final apiService = ApiService();
  final genderList = ["female", "male", "ND"];
  final sexualOrientationList = ["heterosexual", "homosexual", "ND"];
  final coupleStateList = ["single", "couple", "open_relationship", "ND"];
  final alcoholConsumptionList = ["never", "occasionally", "regularly", "ND"];
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final ValueNotifier<int> sexualOrientationValueNotifier =
      ValueNotifier<int>(0); // Initialize with the default index
  final ValueNotifier<int> coupleStateValueNotifier =
      ValueNotifier<int>(0); // Initialize with the default index
  final ValueNotifier<int> alcoholConsumptionValueNotifier =
      ValueNotifier<int>(0); // Initialize with the default index
  File? _image;
  User? user;
  String? userToken;
  @override
  void initState() {
    super.initState();
    // Retrieve the user data when the screen is initialized
    _loadUserData();
  }

  void _loadUserData() async {
    // Fetch the user data from secure storage
    final storage = UserDataStorage();
    user = await storage.getUserData();
    userToken = await storage.getIdentificationToken();
    if (user != null) {
      // Update the UI with the user's data
      setState(() {
        usernameController.text = user!.userName;
        emailController.text = user!.email;
        // set sexual orientation switch
        final sexualOrientationIndex =
            sexualOrientationList.indexOf(user!.sexualOrientation);
        if (sexualOrientationIndex != -1) {
          sexualOrientationValueNotifier.value = sexualOrientationIndex;
        }
        // set couple state switch
        final coupleStateIndex = coupleStateList.indexOf(user!.coupleState);
        if (coupleStateIndex != -1) {
          coupleStateValueNotifier.value = coupleStateIndex;
        }
        // set alcohol consumption switch
        final alcoholConsumptionIndex =
            alcoholConsumptionList.indexOf(user!.alcohol);
        if (alcoholConsumptionIndex != -1) {
          alcoholConsumptionValueNotifier.value = alcoholConsumptionIndex;
        }
        // Set other text fields and UI components with user data
      });
    }
  }

  Future<void> _getImage(ImageSource source) async {
    final pickedImage = await ImagePicker().pickImage(source: source);
    if (pickedImage != null) {
      File? croppedImage;
      CroppedFile? croppedFile = await ImageCropper().cropImage(
        sourcePath: pickedImage.path,
        aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1),
        compressQuality: 100,
        maxWidth: 700,
        maxHeight: 700,
        compressFormat: ImageCompressFormat.jpg,
        cropStyle: CropStyle.circle,
      );
      if (croppedFile != null) {
        croppedImage = File(croppedFile.path);
      }
      if (croppedImage != null) {
        setState(() {
          _image = croppedImage;
        });
      }
    }
  }

  ImageProvider _getImageProvider() {
    if (_image != null) {
      return FileImage(_image!);
    } else if (user?.profilePicture != null) {
      return NetworkImage(user!.profilePicture!);
    } else {
      return AssetImage('lib/assets/default_pp.png');
    }
  }

  Widget? _buildAvatarChild() {
    if (_image == null) {
      if (user?.profilePicture != null) {
        return ProfilePicture_L(imageUrl: user!.profilePicture!);
      } else {
        return const Icon(
          Icons.person,
          size: 100,
          color: Colors.white,
        );
      }
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Profile'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              // Your widgets for editing the user's profile go here
              GestureDetector(
                onTap: () async {
                  showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) {
                      return SafeArea(
                        child: Wrap(
                          children: <Widget>[
                            ListTile(
                              leading: Icon(Icons.photo_library),
                              title:
                                  Text(AppLocalizations.of(context)!.gallery),
                              onTap: () {
                                _getImage(ImageSource.gallery); // Open gallery
                                Navigator.pop(context);
                              },
                            ),
                            ListTile(
                              leading: Icon(Icons.photo_camera),
                              title: Text(
                                  AppLocalizations.of(context)!.takeAPicture),
                              onTap: () {
                                _getImage(ImageSource.camera); // Open camera
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                      );
                    },
                  );
                },
                child: CircleAvatar(
                  radius: 100,
                  backgroundImage: _getImageProvider(),
                  child: _buildAvatarChild(),
                ),
              ),
              // some space
              const SizedBox(height: 20),
              // Example: TextFields for editing the user's profile
              CustomTextField(
                labelText: AppLocalizations.of(context)!.username,
                controller: usernameController,
              ),
              // some space
              const SizedBox(height: 20),
              CustomTextField(
                labelText: AppLocalizations.of(context)!.emailAddress,
                controller: emailController,
              ),
              StateSwitch(
                label: AppLocalizations.of(context)!.sexualOrientation,
                values: sexualOrientationList,
                onChanged: (value) {
                  print(value);
                },
                selectedValueNotifier: sexualOrientationValueNotifier,
              ),
              StateSwitch(
                label: AppLocalizations.of(context)!.coupleState,
                values: coupleStateList,
                onChanged: (value) {
                  print(value);
                },
                selectedValueNotifier: coupleStateValueNotifier,
              ),
              StateSwitch(
                label: AppLocalizations.of(context)!.alcoholConsumption,
                values: alcoholConsumptionList,
                onChanged: (value) {
                  print(value);
                },
                selectedValueNotifier: alcoholConsumptionValueNotifier,
              ), // Example: Button for saving changes
              // some space
              ValidationButtonM(
                text: AppLocalizations.of(context)!.changePassword,
                onPressed: () {
                  // Navigate to the ChangePasswordScreen screen using a named route.
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ChangePasswordScreen(),
                    ),
                  );
                },
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () async {
                  // Handle saving changes to the user's profile
                  final userUpdated = UserUpdate(
                      userName: usernameController.text,
                      identificationToken: userToken,
                      email: emailController.text,
                      sexualOrientation: sexualOrientationList[
                          sexualOrientationValueNotifier.value],
                      coupleState:
                          coupleStateList[coupleStateValueNotifier.value],
                      alcohol: alcoholConsumptionList[
                          alcoholConsumptionValueNotifier.value]);
                  final updateResponse =
                      await apiService.updateProfile(userUpdated);
                  print(updateResponse);
                  // Update the user data in secure storage
                  // if _image is not null, upload the image to the server
                },
                child: Text(AppLocalizations.of(context)!.saveChanges),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
