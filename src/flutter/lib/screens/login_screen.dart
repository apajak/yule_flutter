import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:yule_flutter/screens/home_screen.dart';
import 'package:yule_flutter/widgets/buttons/base/custom_button.dart';
import 'package:yule_flutter/widgets/textFields/custom_textfield.dart';
import 'package:yule_flutter/widgets/buttons/text_button/custom_text_button.dart';
import 'package:yule_flutter/screens/register_screen.dart';
import 'package:yule_flutter/services/api_service.dart';
import 'package:yule_flutter/models/user.dart';
import '../models/flutter_secure_storage.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool stayLoggedIn = false;
  final apiService = ApiService();
  final storage = UserDataStorage();

  @override
  void initState() {
    super.initState();
    _loadCredentials();
  }

  Future<void> _loadCredentials() async {
    var credentials = await storage.getLoginCredentials();
    if (credentials != null) {
      usernameController.text = credentials['username']!;
      passwordController.text = credentials['password']!;
      setState(() {
        stayLoggedIn = true;
      });
    }
  }

  Future<void> _handleLogin() async {
    final username = usernameController.text;
    final password = passwordController.text;

    // Call the login API
    UserCredentials userCredentials = UserCredentials(
      identifier: username,
      password: password,
    );
    final loginResponse = await apiService.login(userCredentials);

    bool value = loginResponse['value'];
    if (!value) {
      print(loginResponse);
      return;
    } else {
      User user = User.fromJson(loginResponse['user']);
      String token = loginResponse['token'];
      print("token: $token");
      user.setIdentificationToken(token);

      // Retrieve FCM token
      String? fcmToken = await _getFcmToken();
      if (fcmToken != null) {
        // Send FCM token to your backend
        print("fcmToken: $fcmToken");
        await apiService.sendFcmToken(fcmToken, token);
      }

      // save user data and token to secure storage
      final storage = UserDataStorage();
      storage.storeUserData(user.toMap());
      storage.storeIdentificationToken(token);

      // Save or delete credentials based on the checkbox
      if (stayLoggedIn) {
        await storage.storeLoginCredentials(username, password);
      } else {
        await storage.deleteLoginCredentials();
      }

      // Navigate to the home screen
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => HomePage()),
      );
    }
  }

  Future<String?> _getFcmToken() async {
    return await FirebaseMessaging.instance.getToken();
  }

  void _showDisclaimerDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Theme(
          data: ThemeData(
            // Customize the dialog theme here
            dialogBackgroundColor: Color(0xFF001524), // Dialog background color
            textTheme: TextTheme(
              bodyLarge: TextStyle(color: Colors.white), // Content text style
              bodyMedium:
                  TextStyle(color: Colors.white), // Additional text style
            ),
            textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(
                foregroundColor: Color(0xFFFF7D00), // Text button color
              ),
            ),
          ),
          child: AlertDialog(
            title: Text("Stay Logged In",
                style: TextStyle(color: Colors.white)), // Title text style
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(
                      "Your credentials will be encrypted and stored locally on your device. But keep in mind if someone else has access to your device, they will be able to access your account.",
                      style: TextStyle(color: Colors.white)),
                  // Add more Text widgets for longer disclaimers
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
        title: Text(AppLocalizations.of(context)!.login),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/images/logo_yule.png',
                width: 300,
                height: 300,
              ),
              CustomTextField(
                labelText: AppLocalizations.of(context)!.username,
                controller: usernameController,
              ),
              const SizedBox(height: 20),
              CustomTextField(
                labelText: AppLocalizations.of(context)!.password,
                controller: passwordController,
                obscureText: true,
              ),
              const SizedBox(height: 5),
              Row(
                children: [
                  Checkbox(
                    value: stayLoggedIn,
                    onChanged: (bool? value) {
                      setState(() {
                        stayLoggedIn = value!;
                      });
                    },
                  ),
                  Text("Stay Logged In"),
                  IconButton(
                    icon: Icon(Icons.info_outline),
                    onPressed: () {
                      _showDisclaimerDialog(context);
                    },
                  ),
                ],
              ),
              CustomButton(
                text: AppLocalizations.of(context)!.login,
                onPressed: _handleLogin,
              ),
              const SizedBox(height: 10),
              CustomTextButton(
                text: AppLocalizations.of(context)!.notRegisteredYet,
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => RegisterScreen()),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
