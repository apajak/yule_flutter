import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:yule_flutter/screens/games/barbu_screen.dart';
import 'package:yule_flutter/screens/home_screen.dart';
import 'package:yule_flutter/screens/join_game_screen.dart';
import 'package:yule_flutter/screens/login_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:yule_flutter/screens/settings/friends_screen.dart';
import 'firebase_options.dart';
import 'package:yule_flutter/models/notification_handler.dart';

void main() async {
  final navigatorKey = GlobalKey<NavigatorState>();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  NotificationHandler.initNotifications(navigatorKey);
  runApp(MyApp(navigatorKey: navigatorKey));
}

class MyApp extends StatelessWidget {
  final Locale? locale;
  final GlobalKey<NavigatorState> navigatorKey;

  const MyApp({Key? key, this.locale, required this.navigatorKey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Yule',
      theme: ThemeData(
        appBarTheme: const AppBarTheme(backgroundColor: Color(0xFFFF7D00)),
        scaffoldBackgroundColor: const Color(0xFF001524),
        primaryColor: const Color(0xFF001524),
        highlightColor: const Color(0xFFFF7D00),
        inputDecorationTheme: const InputDecorationTheme(
          labelStyle:
              TextStyle(color: Colors.white), // Set label text color to white
          hintStyle:
              TextStyle(color: Colors.white), // Set hint text color to white
        ),
        textTheme: const TextTheme(
          // Set all text styles to use white color
          displayLarge: TextStyle(color: Colors.white), // for AppBar title
          displayMedium: TextStyle(color: Colors.white), // for buttons
          displaySmall: TextStyle(color: Colors.white), // for small text
          titleLarge: TextStyle(color: Colors.white), // for large text
          titleMedium: TextStyle(color: Colors.white), // for other text
          titleSmall: TextStyle(color: Colors.white), // for other text
          bodyLarge: TextStyle(color: Colors.white), // for other text
          bodyMedium: TextStyle(color: Colors.white), // for other text
          bodySmall: TextStyle(color: Colors.white), // for other text
        ),
      ),
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', ''),
        Locale('es', ''),
        Locale('fr', ''),
        Locale('it', ''),
        Locale('pt', ''),
        Locale('de', ''),
      ],
      routes: {
        '/login': (context) => LoginPage(),
        '/friendScreen': (context) =>
            FriendsScreen(onFriendRequestHandled: () {}),
        '/joinGame': (context) {
          // Retrieve the game identifier from the route arguments
          final routeArgs =
              ModalRoute.of(context)?.settings.arguments as String?;
          return JoinGameScreen(gameIdentifier: routeArgs);
        },
        '/home': (context) => HomePage() ,
        '/barbu': (context) => BarbuScreen() ,
      },
      initialRoute: '/login',
      navigatorKey: navigatorKey,
      locale: locale,
    );
  }
}
