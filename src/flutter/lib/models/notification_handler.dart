import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationHandler {
  static final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static void initNotifications(GlobalKey<NavigatorState> navigatorKey) async {
    // Set up local notifications
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettings =
        InitializationSettings(android: initializationSettingsAndroid);
    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: (String? payload) {
        _handleNotificationTap(payload, navigatorKey);
      },
    );

    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    // Handle foreground notifications
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      _handleForegroundNotification(message, navigatorKey);
    });

    // Handle background and terminated state notifications
    _handleBackgroundAndTerminatedNotifications(navigatorKey);
  }

  static Future<void> _firebaseMessagingBackgroundHandler(
      RemoteMessage message) async {
    if (message.notification != null) {
      _showNotification(message.notification!, message.data);
    }
  }

  static void _handleForegroundNotification(
      RemoteMessage message, GlobalKey<NavigatorState> navigatorKey) {
    RemoteNotification? notification = message.notification;
    AndroidNotification? android = message.notification?.android;
    if (notification != null && android != null) {
      _showNotification(notification, message.data);
    }
  }

  static void _showNotification(
      RemoteNotification notification, Map<String, dynamic> data) {
    flutterLocalNotificationsPlugin.show(
      notification.hashCode,
      notification.title,
      notification.body,
      NotificationDetails(
        android: AndroidNotificationDetails(
          'channel_id',
          'channel_name',
          channelDescription: 'channel_description',
          importance: Importance.max,
          priority: Priority.high,
        ),
      ),
      payload: data['route'],
    );
  }

  static void _handleBackgroundAndTerminatedNotifications(
      GlobalKey<NavigatorState> navigatorKey) {
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage? message) {
      if (message != null) {
        _handleNotificationTap(message.data['route'], navigatorKey);
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      _handleNotificationTap(message.data['route'], navigatorKey);
    });
  }

  static void _handleNotificationTap(
      String? payload, GlobalKey<NavigatorState> navigatorKey) {
    if (payload != null && payload.isNotEmpty) {
      // Extract additional data like game_id
      var uri = Uri.parse(payload);
      var gameIdentifier = uri.queryParameters['game_id'];

      // Navigate to the specific screen with additional data
      navigatorKey.currentState?.pushNamed(
        uri.path, // This is the route, like '/joinGame'
        arguments: gameIdentifier,
      );
    }
  }
}
