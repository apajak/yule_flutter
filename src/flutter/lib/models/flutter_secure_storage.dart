import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:yule_flutter/models/user.dart';

class UserDataStorage {
  final _storage = FlutterSecureStorage();

  Future<void> storeUserData(Map<String, dynamic> userData) async {
    // Convert the user data to a JSON string
    final userDataJson = jsonEncode(userData);

    // Store the user data as a JSON string in the secure storage
    await _storage.write(key: 'userData', value: userDataJson);
  }

  Future<User?> getUserData() async {
    var userData = await _storage.read(key: 'userData');
    print("user data: $userData");
    if (userData != null) {
      try {
        Map<String, dynamic> userDataMap = json.decode(userData);
        User user = User.fromStorage(userDataMap);
        return user;
      } catch (e) {
        print("Error parsing JSON: $e");
      }
    }
    return null; // Return null in case of error or missing data
  }

  Future<void> deleteUserData() async {
    await _storage.delete(key: 'userData');
  }

  Future<void> storeIdentificationToken(String identificationToken) async {
    await _storage.write(
        key: 'identificationToken', value: identificationToken);
  }

  Future<void> storeMailToken(String mailToken) async {
    await _storage.write(key: 'mailToken', value: mailToken);
  }

  Future<String?> getIdentificationToken() async {
    return await _storage.read(key: 'identificationToken');
  }

  Future<String?> getMailToken() async {
    return await _storage.read(key: 'mailToken');
  }

  Future<void> deleteIdentificationToken() async {
    await _storage.delete(key: 'identificationToken');
  }

  Future<void> deleteMailToken() async {
    await _storage.delete(key: 'mailToken');
  }

  Future<void> deleteAll() async {
    await _storage.deleteAll();
  }

  Future<void> storeLoginCredentials(String username, String password) async {
    await _storage.write(key: 'loginUsername', value: username);
    await _storage.write(key: 'loginPassword', value: password);
  }

  Future<Map<String, String>?> getLoginCredentials() async {
    String? username = await _storage.read(key: 'loginUsername');
    String? password = await _storage.read(key: 'loginPassword');
    if (username != null && password != null) {
      return {'username': username, 'password': password};
    }
    return null;
  }

  Future<void> deleteLoginCredentials() async {
    await _storage.delete(key: 'loginUsername');
    await _storage.delete(key: 'loginPassword');
  }
}
