import 'dart:convert';
import 'package:intl/intl.dart';

class User {
  final int? userId;
  final String userUuid;
  String userName;
  String firstName;
  String lastName;
  String email;
  DateTime birthDate;
  String gender;
  String sexualOrientation;
  String coupleState;
  String alcohol;
  int totalSips;
  int isConnect;
  String? identificationToken;
  String? mailToken;
  String? profilePicture;

  User({
    this.userId,
    required this.userUuid,
    required this.userName,
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.birthDate,
    required this.gender,
    required this.sexualOrientation,
    required this.coupleState,
    required this.alcohol,
    required this.totalSips,
    required this.isConnect,
    this.identificationToken,
    this.mailToken,
    this.profilePicture,
  });

  Map<String, dynamic> toMap() {
    return {
      'user_id': userId,
      'user_uuid': userUuid,
      'user_name': userName,
      'first_name': firstName,
      'last_name': lastName,
      'email': email,
      'birth_date': birthDate != null ? birthDate.toIso8601String() : null,
      'gender': gender,
      'sexual_orientation': sexualOrientation,
      'couple_state': coupleState,
      'alcohol': alcohol,
      'total_sips': totalSips,
      'is_connect': isConnect,
      'profile_picture': profilePicture,
    };
  }

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      userId: json['user_id'] as int?,
      userUuid: json['user_uuid'] as String,
      userName: json['user_name'] as String,
      firstName: json['first_name'] as String,
      lastName: json['last_name'] as String,
      email: json['email'] as String,
      birthDate: DateTime.parse(json['birth_date'] as String),
      gender: json['gender'] as String,
      sexualOrientation: json['sexual_orientation'] as String,
      coupleState: json['couple_state'] as String,
      alcohol: json['alcohol'] as String,
      totalSips: json['total_sips'] as int,
      isConnect: json['is_connect'] as int,
      identificationToken: json['identification_token'] as String?,
      mailToken: json['mail_token'] as String?,
      profilePicture: json['profile_picture'] as String?,
    );
  }

  factory User.fromStorage(Map<String, dynamic> json) {
    return User(
      userId: json['user_id'] as int?,
      userUuid: json['user_uuid'] as String,
      userName: json['user_name'] as String,
      firstName: json['first_name'] as String,
      lastName: json['last_name'] as String,
      email: json['email'] as String,
      birthDate: DateTime.parse(json['birth_date'] as String),
      gender: json['gender'] as String,
      sexualOrientation: json['sexual_orientation'] as String,
      coupleState: json['couple_state'] as String,
      alcohol: json['alcohol'] as String,
      totalSips: json['total_sips'] as int,
      isConnect: json['is_connect'] as int,
      profilePicture: json['profile_picture'] as String?,
    );
  }
  String? getProfilePicture() {
    return profilePicture;
  }

  String? getIdentificationToken() {
    return identificationToken;
  }

  void setIdentificationToken(String? newToken) {
    identificationToken = newToken;
  }

  String? getMailToken() {
    return mailToken;
  }

  void setMailToken(String? newToken) {
    mailToken = newToken;
  }

  Player toPlayer() {
    return Player(
      userUuid: this.userUuid,
      userName: this.userName,
      gender: this.gender,
      sexualOrientation: this.sexualOrientation,
      coupleState: this.coupleState,
      alcohol: this.alcohol,
      totalSips: this.totalSips,
      profilePicture: this.profilePicture,
    );
  }
}

class UserCreate {
  final String userName;
  final String firstName;
  final String lastName;
  final String email;
  final DateTime birthDate;
  final String gender;
  final String sexualOrientation;
  final String coupleState;
  final String alcohol;
  final String password;

  UserCreate({
    required this.userName,
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.birthDate,
    required this.gender,
    required this.sexualOrientation,
    required this.coupleState,
    required this.alcohol,
    required this.password,
  });

  set profilePicture(profilePicture) {
    this.profilePicture = profilePicture;
  }

  Map<String, dynamic> toJson() {
    return {
      'user_name': userName,
      'first_name': firstName,
      'last_name': lastName,
      'email': email,
      'birth_date': DateFormat('yyyy-MM-dd').format(birthDate),
      'gender': gender,
      'sexual_orientation': sexualOrientation,
      'couple_state': coupleState,
      'alcohol': alcohol,
      'password': password,
    };
  }
}

class UserCredentials {
  final String identifier;
  final String password;

  UserCredentials({
    required this.identifier,
    required this.password,
  });

  Map<String, dynamic> toJson() {
    return {
      'identifier': identifier,
      'password': password,
    };
  }
}

class UserUpdate {
  final String userName;
  final String email;
  final String sexualOrientation;
  final String coupleState;
  final String alcohol;
  final String? identificationToken;

  UserUpdate({
    required this.userName,
    required this.email,
    required this.sexualOrientation,
    required this.coupleState,
    required this.alcohol,
    required this.identificationToken,
  });

  Map<String, dynamic> toJson() {
    return {
      'user_name': userName,
      'email': email,
      'sexual_orientation': sexualOrientation,
      'couple_state': coupleState,
      'alcohol': alcohol,
      'identification_token': identificationToken,
    };
  }
}

class UserUpdatePassword {
  final String password;
  final String newPassword;
  final String? identificationToken;

  UserUpdatePassword({
    required this.password,
    required this.newPassword,
    required this.identificationToken,
  });

  Map<String, dynamic> toJson() {
    return {
      'password': password,
      'new_password': newPassword,
      'identification_token': identificationToken,
    };
  }
}

class SimpleUser {
  final String userUuid;
  String userName;
  String? profilePicture;
  int gameAlcohol;
  String gender;

  SimpleUser({
    required this.userUuid,
    required this.userName,
    this.profilePicture,
    required this.gameAlcohol,
    required this.gender
  });

  Map<String, dynamic> toMap() {
    return {
      'user_uuid': userUuid,
      'user_name': userName,
      'profile_picture': profilePicture,
      'game_alcohol': gameAlcohol,
      'gender': gender
    };
  }

  factory SimpleUser.fromJson(Map<String, dynamic> json) {
    print(json['game_alcohol']);
    return SimpleUser(
        userUuid: json['user_uuid'] as String,
        userName: json['user_name'] as String,
        profilePicture: json['profile_picture'] as String?,
        gameAlcohol: (json['game_alcohol'] as int?) ?? 3, // Gérer le cas où game_alcohol est null
        gender: json['gender'] as String
    );
  }

}

class Player {
  final String userUuid;
  final String userName;
  final String gender;
  final String sexualOrientation;
  final String coupleState;
  final String alcohol;
  final int totalSips;
  final String? profilePicture;

  Player({
    required this.userUuid,
    required this.userName,
    required this.gender,
    required this.sexualOrientation,
    required this.coupleState,
    required this.alcohol,
    required this.totalSips,
    this.profilePicture,
  });

  Map<String, dynamic> toMap() {
    return {
      'user_uuid': userUuid,
      'user_name': userName,
      'gender': gender,
      'sexual_orientation': sexualOrientation,
      'couple_state': coupleState,
      'alcohol': alcohol,
      'total_sips': totalSips,
      'profile_picture': profilePicture,
    };
  }

  factory Player.fromJson(Map<String, dynamic> json) {
    return Player(
      userUuid: json['user_uuid'] as String,
      userName: json['user_name'] as String,
      gender: json['gender'] as String,
      sexualOrientation: json['sexual_orientation'] as String,
      coupleState: json['couple_state'] as String,
      alcohol: json['alcohol'] as String,
      totalSips: json['total_sips'] as int,
      profilePicture: json['profile_picture'] as String?,
    );
  }

  String toJson() => json.encode(toMap());

  @override
  String toString() {
    return 'Player(userUuid: $userUuid, userName: $userName, gender: $gender, sexualOrientation: $sexualOrientation, coupleState: $coupleState, alcohol: $alcohol, totalSips: $totalSips, profilePicture: $profilePicture)';
  }
}

class UserWithStatus {
  final SimpleUser user;
  final bool isFriend;

  UserWithStatus({required this.user, required this.isFriend});
}
